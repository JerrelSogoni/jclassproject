package test_bed;

import java.io.File;
import javafx.collections.FXCollections;
import javafx.embed.swing.JFXPanel;
import javafx.scene.control.CheckBox;
import jc.data.ClassAttributes;
import jc.data.ClassMethods;
import jc.data.DataManager;
import jc.data.JClassClassBox;
import jc.data.JClassInterfaceBox;
import jc.file.FileManager;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jerrel
 */
public class TestSave {
    
    DataManager threadExample;

    public TestSave() throws Exception {
        threadExample = new DataManager(null);
        threadExample.setBoxes(FXCollections.observableArrayList());
   //     JClassClassBox ThreadExample = new JClassClassBox();
  //      ThreadExample.setLocationAndSize(0, 0, 10, 10);
   //     ThreadExample.getClassName().setText("ThreadExample");
  //      ThreadExample.setPackageName("Dummy");
        //Thread example Attributes
    //    ThreadExample.addParent("Application");
        ClassAttributes thread = new ClassAttributes();
        thread.setAttributeAccess("public");
        thread.setAttributeName("START_TEXT");
        thread.setIsStatic(true);
        thread.setAttributeType("String");
        ClassAttributes thread2 = new ClassAttributes();
        thread2.setAttributeAccess("public");
        thread2.setAttributeName("PAUSE_TEXT");
        thread2.setIsStatic(true);
        thread2.setAttributeType("String");
        ClassAttributes thread3 = new ClassAttributes();
        thread3.setAttributeAccess("private");
        thread3.setAttributeName("window");
        thread3.setAttributeType("String");
        ClassAttributes thread4 = new ClassAttributes();
        thread4.setAttributeAccess("private");
        thread4.setAttributeName("appPane");
        thread4.setAttributeType("FlowPane");
        ClassAttributes thread5 = new ClassAttributes();
        thread5.setAttributeAccess("private");
        thread5.setAttributeName("startButton");
        thread5.setAttributeType("Button");
        ClassAttributes thread6 = new ClassAttributes();
        thread6.setAttributeAccess("private");
        thread6.setAttributeName("startButton");
        thread6.setAttributeType("Button");
        ClassAttributes thread7 = new ClassAttributes();
        thread7.setAttributeAccess("private");
        thread7.setAttributeName("scrollPane");
        thread7.setAttributeType("ScrollPane");
        ClassAttributes thread8 = new ClassAttributes();
        thread8.setAttributeAccess("private");
        thread8.setAttributeName("dateThread");
        thread8.setAttributeType("Thread");
        ClassAttributes thread9 = new ClassAttributes();
        thread9.setAttributeAccess("private");
        thread9.setAttributeName("dateTask");
        thread9.setAttributeType("Task");
        ClassAttributes thread10 = new ClassAttributes();
        thread10.setAttributeAccess("private");
        thread10.setAttributeName("counterThread");
        thread10.setAttributeType("Thread");
        ClassAttributes thread11 = new ClassAttributes();
        thread11.setAttributeAccess("private");
        thread11.setAttributeName("counterTask");
        thread11.setAttributeType("Task");
        ClassAttributes thread12 = new ClassAttributes();
        thread12.setAttributeAccess("private");
        thread12.setAttributeName("work");
        thread12.setAttributeType("boolean");
//        ThreadExample.addAttribute(thread);
    //    ThreadExample.addAttribute(thread2);
    ////    ThreadExample.addAttribute(thread3);
   //     ThreadExample.addAttribute(thread4);
   //     ThreadExample.addAttribute(thread5);
  ///      ThreadExample.addAttribute(thread6);
   //     ThreadExample.addAttribute(thread7);
 //       ThreadExample.addAttribute(thread8);
//        ThreadExample.addAttribute(thread9);
//        ThreadExample.addAttribute(thread10);
//        ThreadExample.addAttribute(thread11);
//        ThreadExample.addAttribute(thread12);
        
        
        
        
        
        // Methods
        
        ClassMethods m1 = new ClassMethods();
        m1.setMethodAccess("public");
        m1.setMethodReturn("void");
    //    m1.setArg("Stage");
        m1.setMethodName("start");
        m1.setIsException(true);
        ClassMethods m2 = new ClassMethods();
        m2.setMethodAccess("public");
        m2.setMethodReturn("void");
        m2.setMethodName("startWork");
        ClassMethods m3 = new ClassMethods();
        m3.setMethodAccess("public");
        m3.setMethodReturn("void");
        m3.setMethodName("pauseWork");
        ClassMethods m4 = new ClassMethods();
        m4.setMethodAccess("public");
        m4.setMethodReturn("boolean");
        m4.setMethodName("doWork");
        ClassMethods m5 = new ClassMethods();
        m5.setMethodAccess("public");
        m5.setMethodReturn("void");
        m5.setMethodName("appendText");
      //  m5.setArg("String");
        ClassMethods m6 = new ClassMethods();
        m6.setMethodAccess("public");
        m6.setMethodReturn("void");
        m6.setMethodName("sleep");
      //  m6.setArg("int");
        ClassMethods m7 = new ClassMethods();
        m7.setMethodAccess("public");
        m7.setMethodReturn("void");
        m7.setMethodName("initLayout");
     
        ClassMethods m8 = new ClassMethods();
        m8.setMethodAccess("public");
        m8.setMethodReturn("void");
        m8.setMethodName("initHandlers");
        ClassMethods m9 = new ClassMethods();
        m9.setMethodAccess("public");
        m9.setMethodReturn("void");
        m9.setMethodName("initWindow");
        ClassMethods m10 = new ClassMethods();
        m10.setMethodAccess("public");
        m10.setMethodReturn("void");
        m10.setMethodName("initWindow");
      //  m10.setArg("Stage");
        ClassMethods m11 = new ClassMethods();
        m11.setMethodAccess("public");
        m11.setMethodReturn("void");
        m11.setMethodName("startWork");
        ClassMethods m12 = new ClassMethods();
        m12.setMethodAccess("public");
        m12.setMethodReturn("void");
        m12.setMethodName("initThreads");
        ClassMethods m13 = new ClassMethods();
        m13.setMethodAccess("public");
        m13.setMethodReturn("void");
        m13.setMethodName("startWork");
        m13.setIsStatic(true);
       // m13.setArg("String[]");
//        
//        ThreadExample.addMethods(m1);
//        ThreadExample.addMethods(m2);
//        ThreadExample.addMethods(m3);
//        ThreadExample.addMethods(m4);
//        ThreadExample.addMethods(m5);
//        ThreadExample.addMethods(m6);
//        ThreadExample.addMethods(m7);
//        ThreadExample.addMethods(m8);
//        ThreadExample.addMethods(m9);
//        ThreadExample.addMethods(m10);
//        ThreadExample.addMethods(m11);
//        ThreadExample.addMethods(m12);
//        ThreadExample.addMethods(m13);
//        
        //Start Handler
        
//      //  JClassClassBox StartHandler = new JClassClassBox();
//        StartHandler.setLocationAndSize(200, 200, 30, 30);
//
//        StartHandler.getClassName().setText("StartHandler");
//        StartHandler.setPackageName("Dummy");
//        StartHandler.addParent("EventHandler");
//        ClassAttributes a1 = new ClassAttributes();
//        a1.setAttributeAccess("private");
//        a1.setAttributeType("ThreadExample");
//        a1.setAttributeName("app");
        ClassMethods s1 = new ClassMethods();
        s1.setMethodName("StartHandler");
        s1.setMethodAccess("public");
       // s1.setArg("ThreadExample");
        ClassMethods s2 = new ClassMethods();
        s2.setMethodAccess("public");
        s2.setMethodReturn("void");
        s2.setMethodName("handle");
       // s2.setArg("Event");
        
//        StartHandler.addAttribute(a1);
//        StartHandler.addMethods(s1);
//        StartHandler.addMethods(s2);
//        
        // Pause Handler
        
//        JClassClassBox PauseHandler = new JClassClassBox();
//        PauseHandler.setLocationAndSize(300, 0, 40, 40);
//        PauseHandler.getClassName().setText("PauseHandler");
//        PauseHandler.setPackageName("Dummy");
//        StartHandler.addParent("PauseHandler");
        ClassAttributes p1 = new ClassAttributes();
        p1.setAttributeAccess("private");
        p1.setAttributeType("ThreadExample");
        p1.setAttributeName("app");
        ClassMethods q1 = new ClassMethods();
        q1.setMethodAccess("public");
        q1.setMethodName("PauseHandler");
      //  q1.setArg("ThreadExample");
        ClassMethods y2 = new ClassMethods();
        y2.setMethodReturn("void");
        y2.setMethodAccess("public");
        y2.setMethodName("handle");
//      //  y2.setArg("Event");
//        
//        PauseHandler.addAttribute(p1);
//        PauseHandler.addMethods(q1);
//        PauseHandler.addMethods(y2);
//        // Date Task
//        
//        JClassClassBox DateTask = new JClassClassBox();
//        DateTask.setLocationAndSize(0, 400, 60, 60);
//        DateTask.getClassName().setText("DateTask");
//        DateTask.setPackageName("Dummy");
//        DateTask.addParent("Task<Void>");
        ClassAttributes d1 = new ClassAttributes();
        d1.setAttributeAccess("private");
        d1.setAttributeType("ThreadExample");
        d1.setAttributeName("app");
        ClassAttributes d2 = new ClassAttributes();
        d2.setAttributeAccess("private");
        d2.setAttributeType("Date");
        d2.setAttributeName("now");
        ClassMethods d3 = new ClassMethods();
      //  d3.setArg("ThreadExample");
        d3.setMethodAccess("public");
        d3.setMethodName("DateTask");
        ClassMethods d4 = new ClassMethods();
        d4.setMethodName("call");
        d4.setIsException(true);
        d4.setMethodAccess("protected");
//        la1.setAttributeType("ThreadExample");
       d4.setMethodReturn("Void");
//        
//        DateTask.addAttribute(d1);
//        DateTask.addAttribute(d2);
//        DateTask.addMethods(d3);
//        DateTask.addMethods(d4);
//        
//        //COunter Example
//        JClassClassBox CounterTask = new JClassClassBox();
//        CounterTask.setLocationAndSize(250, 250, 100, 100);
//        CounterTask.setLayoutX(150);
//        CounterTask.setLayoutY(150);
//        CounterTask.getClassName().setText("CounterTask");
//        CounterTask.setPackageName("Dummy");
//        DateTask.addParent("Task<Void>");
//        ClassAttributes la1 = new ClassAttributes();
//        la1.setAttributeAccess("private");
//         la1.setAttributeName("app");
        ClassAttributes la2 = new ClassAttributes();
        la2.setAttributeAccess("private");
        la2.setAttributeType("int");
        la2.setAttributeName("counter");
        ClassMethods la3 = new ClassMethods();
      //  la3.setArg("ThreadExample");
        la3.setMethodAccess("public");
        la3.setMethodName("CounterTask");
        ClassMethods la4 = new ClassMethods();
        la4.setMethodName("call");
//        la4.setIsException(true);
//        la4.setMethodAccess("protected");
//        la4.setMethodReturn("Void");
//        
//        CounterTask.addAttribute(la1);
//        CounterTask.addAttribute(la2);
//        CounterTask.addMethods(la3);
//        CounterTask.addMethods(la4);
//        
//        //EventHandler
//        JClassInterfaceBox EventHandler = new JClassInterfaceBox();
//        EventHandler.setLocationAndSize(700, 200, 250, 250);
//        EventHandler.getClassName().setText("EventHandler");
//        EventHandler.setPackageName("javafx.event");
//        
//        // Task<Void>
//        
//        JClassInterfaceBox Task = new JClassInterfaceBox();
//        Task.setLocationAndSize(0, 100, 40, 40);
//        Task.getClassName().setText("Task<Void>");
//        EventHandler.setPackageName("javafx.concurrent");
        
        // Application
        
//        JClassClassBox abs = new JClassClassBox();
//        abs.setLocationAndSize(300, 0, 40, 40);
//        abs.setPackageName("javafx.application");
//        abs.getClassName().setText("Application");
//        abs.putAbstractLabel();
//        
// 
//        
//        threadExample.getBoxes().addAll(ThreadExample,StartHandler, PauseHandler, DateTask, CounterTask,EventHandler, Task
//                                        , abs);
//        //Lines
//        threadExample.makeLine(abs, ThreadExample.getName(), "EXTENDS");
//        threadExample.makeLine(EventHandler, StartHandler.getName(), "IMPLEMENTS");
//        threadExample.setGrid(new CheckBox());
//        threadExample.getGrid().setSelected(false);
//        threadExample.setSnap(new CheckBox());
//        threadExample.getSnap().setSelected(false);
//        
//    
    }
    public static void main(String[] args) throws Exception{
        JFXPanel fxPanel = new JFXPanel();
        TestSave test = new TestSave();
        FileManager fileManager = new FileManager();
	String currentDirectory;
	File file = new File("");
	currentDirectory = file.getAbsolutePath() + "/src/test_bed/DesignSaveTest.";
        fileManager.saveData(test.threadExample, currentDirectory);
        
        
    }

}
