package test_bed;

import java.io.File;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Node;
import jc.data.ClassAttributes;
import jc.data.ClassMethods;
import jc.data.DataManager;
import jc.data.InterfaceAttributes;
import jc.data.InterfaceMethods;
import jc.data.JClassBoxes;
import jc.data.JClassClassBox;
import jc.data.JClassInterfaceBox;
import jc.data.JLine;
import jc.file.FileManager;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jerrel
 */
public class TestLoad {
    DataManager data;
    public TestLoad() throws Exception{
        data = new DataManager(null);
        data.setBoxes(FXCollections.observableArrayList());
        
    }
    public static void main(String[] args) throws Exception{
        JFXPanel fxPanel = new JFXPanel();
        TestLoad test = new TestLoad();
        FileManager fileManager = new FileManager();
	String currentDirectory;
	File file = new File("");
	currentDirectory = file.getAbsolutePath() + "/src/test_bed/DesignSaveTest.json";
        fileManager.loadData(test.data, currentDirectory);
        ObservableList<Node> data = test.data.getBoxes();
        for(Node  node: data){
            if(node instanceof JClassClassBox){
                JClassClassBox boxes = (JClassClassBox)node;
                String type = boxes.getBoxType();
                double x= boxes.getX();
                double y = boxes.getY();
                double width = boxes.getWidth();
                double height = boxes.getHeight();
                System.out.println("Type:" + type );
                System.out.println("x:" + x);
                System.out.println("y:" + y);
                System.out.println("width:" + width);
                System.out.println("height:" + height);
                ArrayList<String> parents = boxes.getBoxParent();
                for(String parent: parents){
                    System.out.println("Parent:" + parent);
                }  
                ObservableList<ClassAttributes> attributeData = boxes.getClassAttributes();
                if(attributeData != null){
                for(ClassAttributes aps : attributeData){
                        String attributeName = aps.getAttributeName();
                        String attributeType = aps.getAttributeType();
                        boolean isFinal = aps.getIsFinal();
                        boolean isStatic = aps.getIsStatic();
                        System.out.println("Attribute Name:" + attributeName );
                        System.out.println("Attribute Type:" + attributeType);
                        System.out.println("isFinal?:" + isFinal);
                        System.out.println("isStatic?:" + isStatic);
                        
                }
                }
               ObservableList<ClassMethods> methodData = boxes.getClassMethods();
               if(methodData != null){
                for(ClassMethods aps : methodData){
                    String methodName = aps.getMethodName();
                    String methodReturn = aps.getMethodReturn();
                    String methodAccess = aps.getMethodAccess();
                    boolean isStatic = aps.getIsStatic();
                    boolean isE = aps.getIsException();
                    boolean isAbs = aps.getIsAbstract();
                    System.out.println("Method Name:" + methodName );
                    System.out.println("Method Return:" + methodReturn);
                    System.out.println("isStatic?:" + isStatic);
                    System.out.println("isAbstract?:" + isAbs);
                    System.out.println("isException?:" + isE);
                    ArrayList<String> arugments = aps.getArguments();
                    for(String a : arugments){
                        int i = 1;
                        System.out.println("Arg" + i + ":" + a);
              
                    }     
                } 
               }
            }
            else if(node instanceof JClassInterfaceBox){
                JClassInterfaceBox boxes = (JClassInterfaceBox)node;
                String type = boxes.getBoxType();
                double x= boxes.getX();
                double y = boxes.getY();
                double width = boxes.getWidth();
                double height = boxes.getHeight();
                String parent = boxes.getBoxParent();
                System.out.println("Type:" + type );
                System.out.println("x:" + x);
                System.out.println("y:" + y);
                System.out.println("width:" + width);
                System.out.println("height:" + height);
                System.out.println("Parent:" + parent);
                ObservableList<InterfaceAttributes> attributeData = boxes.getInterfaceAttributes();
                if(attributeData != null){
               for(InterfaceAttributes aps : attributeData){
                       String attributeName = aps.getAttributeName();
                        String attributeType = aps.getAttributeType();
                        System.out.println("Attribute Name:" + attributeName );
                        System.out.println("Attribute Type:" + attributeType);


                        
                    }
                }
                ObservableList<InterfaceMethods> methodData = boxes.getInterfaceMethods();
                if(methodData != null){
                    
                for(InterfaceMethods aps : methodData){
                    String methodName = aps.getMethodName();
                    String methodReturn = aps.getMethodReturn();
                    String methodAccess = aps.getMethodAccess();
                    boolean isE = aps.getIsException();
                    System.out.println("Method Name:" + methodName );
                    System.out.println("Method Return:" + methodReturn);
                    System.out.println("isException?:" + isE);
                    ArrayList<String> arugments = aps.getArguments();
                    for(String a : arugments){
                        int i = 1;
                        System.out.println("Arg" + i + ":" + a);
              
                    }   
                }
            }
                
            }
        }
        ObservableList<Node> lines = test.data.getBoxes();
        int i = 0;
        for(Node node: lines){
                if(node instanceof JLine){
                JLine line = (JLine)node;
                
                
                System.out.println("LINE" + i++);
                System.out.println("TYPE:" + line.getType());
                System.out.println("STARTX:" + line.getStartX() );
                System.out.println("STARTY:" + line.getStartY());
                System.out.println("ENDX:" + line.getEndX());
                System.out.println("ENDY:" + line.getEndY());
                System.out.println("PARENT:" + line.getJParent().getName());
                System.out.println("CHILDREN:" + line.getJChildren().getName());
                }
                
        }

        
    }
    
    
}
