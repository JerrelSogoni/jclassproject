package jc.gui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBase;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.util.Callback;
import static jc.PropertyType.SELECTION_TOOL_ICON;
import static jc.PropertyType.SELECTION_TOOL_TOOLTIP;
import static jc.PropertyType.RESIZE_ICON;
import static jc.PropertyType.RESIZE_TOOLTIP;
import static jc.PropertyType.SNAPSHOT_ICON;
import static jc.PropertyType.SNAPSHOT_TOOLTIP;
import static jc.PropertyType.CODE_ICON;
import static jc.PropertyType.Code_TOOLTIP;
import static jc.PropertyType.ADD_CLASS_ICON;
import static jc.PropertyType.ADD_CLASS_TOOLTIP;
import static jc.PropertyType.ADD_INTERFACE_ICON;
import static jc.PropertyType.ADD_INTERFACE_TOOLTIP;
import static jc.PropertyType.REMOVE_ICON;
import static jc.PropertyType.REMOVE_TOOLTIP;
import static jc.PropertyType.UNDO_ICON;
import static jc.PropertyType.UNDO_TOOLTIP;
import static jc.PropertyType.REDO_ICON;
import static jc.PropertyType.REDO_TOOLTIP;
import static jc.PropertyType.ZOOM_IN_ICON;
import static jc.PropertyType.ZOOM_IN_TOOLTIP;
import static jc.PropertyType.ZOOM_OUT_ICON;
import static jc.PropertyType.ZOOM_OUT_TOOLTIP;
import jc.controller.CanvasController; 
import jc.controller.JClassEditController;
import jc.data.DataManager;
import jc.data.JClassBoxes;
import static jc.data.JClassBoxes.CLASS;
import static jc.data.JClassBoxes.INTERFACE;
import jc.data.JClassClassBox;
import jc.data.JClassInterfaceBox;
import jc.data.JClassMakerState;
import jc.data.States;
import saf.ui.AppYesNoCancelDialogSingleton;
import saf.ui.AppMessageDialogSingleton;
import saf.ui.AppGUI;
import saf.AppTemplate;
import saf.components.AppWorkspaceComponent;
import static saf.settings.AppStartupConstants.FILE_PROTOCOL;
import static saf.settings.AppStartupConstants.PATH_IMAGES;


/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Richard McKenna
 * @version 1.0
 */
public class Workspace extends AppWorkspaceComponent {

    // THESE CONSTANTS ARE FOR TYING THE PRESENTATION STYLE OF
    // THIS Workspace'S COMPONENTS TO A STYLE SHEET THAT IT USES
    static final String CLASS_FILE_EDIT_TOOLBAR = "file_edit_toolbar";
    static final String CLASS_FILE_VIEW_TOOLBAR = "file_view_toolbar";
    static final String CLASS_RENDER_CANVAS = "render_canvas";
    static final String CLASS_SCROLL_PANE = "scroll_pane";
    static final String CLASS_RIGHT_PANE = "right_pane";
    static final String CLASS_PHOTO = "photo";
    static final String CLASS_SPECIAL_BUTTONS = "special_bottons";
    static final String CLASS_FILE_EDIT_TOOLBAR_BUTTONS = "file_edit_toolbar_button";
    static final String CLASS_FILE_VIEW_TOOLBAR_BUTTONS = "file_view_toolbar_button";
    static final String CLASS_CHECKBOX = "checkboxes";
    static final String EMPTY_TEXT = "";
    static final String CLASS_VBOX = "class_vbox_region";
    static final String CLASS_VBOX_VM = "variables_methods_vbox";
    static final String CLASS_TABLE_VBOX = "tables_vbox";
    static final int RIGHT_PANE = 500;
    double zoom = 1;
    // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;
    //Our photo and code in our file edit controller
    VBox innerMiddleFrame;
    Button photo;
    Button code;

    // HAS ALL THE CONTROLS FOR EDITING
    ScrollPane canvasScroll;
    HBox editToolbar;
    Button selectionTool;
    Button resizeTool;
    Button addClass;
    Button addInterface;
    Button remove;
    Button undo;
    Button redo;
    BorderPane viewOptions;
    HBox viewToolbar;
    VBox checkBoxToolbar;
    Button zoomIn;
    Button zoomOut;
    CheckBox snap;
    CheckBox grid;
    //BlankRightPane
    Pane rightPane;
    BorderPane workArea;
    // THIS IS WHERE WE'LL RENDER OUR DRAWING
    Pane canvas;
    
    // HERE ARE THE CONTROLLERS
    CanvasController canvasController;
    JClassEditController jClassEditController;    

    // HERE ARE OUR DIALOGS
    AppMessageDialogSingleton messageDialog;
    AppYesNoCancelDialogSingleton yesNoCancelDialog;
    
    // FOR DISPLAYING DEBUG STUFF
    Text debugText;
    //Right Pane
    JClassClassPane jClassPane;
    JClassInterfacePane jInterfacePane;

    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
	// KEEP THIS FOR LATER
	app = initApp;

	// KEEP THE GUI FOR LATER
	gui = app.getGUI();

	layoutGUI();
	setupHandlers();
    }
    
    private void layoutGUI() {
        //access OverallFileToolbar
        BorderPane appGUIToolBar = (BorderPane)app.getGUI().getAppPane().getTop();
        //FrameworkStuff
        BorderPane appGUIFileToolbar = (BorderPane)appGUIToolBar.getLeft();
        innerMiddleFrame = new VBox();
        photo = gui.initChildButton(innerMiddleFrame, SNAPSHOT_ICON.toString(), SNAPSHOT_TOOLTIP.toString(), true);
        code = gui.initChildButton(innerMiddleFrame, CODE_ICON.toString(), Code_TOOLTIP.toString(), true);
        appGUIFileToolbar.setCenter(innerMiddleFrame);
        
	// THIS WILL GO IN THE LEFT SIDE OF THE WORKSPACE
	editToolbar = new HBox();
	
	// edit toolbar
        selectionTool = gui.initChildButton(editToolbar, SELECTION_TOOL_ICON.toString(), SELECTION_TOOL_TOOLTIP.toString(), true);
        resizeTool = gui.initChildButton(editToolbar, RESIZE_ICON.toString(), RESIZE_TOOLTIP.toString(), true);
        addClass = gui.initChildButton(editToolbar, ADD_CLASS_ICON.toString(), ADD_CLASS_TOOLTIP.toString(), true); 
        addInterface = gui.initChildButton(editToolbar, ADD_INTERFACE_ICON.toString(), ADD_INTERFACE_TOOLTIP.toString(), true);
        remove = gui.initChildButton(editToolbar, REMOVE_ICON.toString(), REMOVE_TOOLTIP.toString(), true);
        undo = gui.initChildButton(editToolbar, UNDO_ICON.toString(), UNDO_TOOLTIP.toString(), true);
        redo = gui.initChildButton(editToolbar, REDO_ICON.toString(), REDO_TOOLTIP.toString(), true);
        // view bar
        viewToolbar = new HBox();
        zoomIn = gui.initChildButton(viewToolbar, ZOOM_IN_ICON.toString(), ZOOM_IN_TOOLTIP.toString(), true);
        zoomOut = gui.initChildButton(viewToolbar, ZOOM_OUT_ICON.toString(), ZOOM_OUT_TOOLTIP.toString(), true);
        checkBoxToolbar = new VBox();
        grid = new CheckBox("Grid");
        grid.setDisable(true);
        snap = new CheckBox("Snap");
        snap.setDisable(true);
        checkBoxToolbar.getChildren().addAll(grid,snap);
        viewOptions = new BorderPane();
        viewOptions.setLeft(viewToolbar);
        viewOptions.setRight(checkBoxToolbar);
        
        appGUIToolBar.setCenter(editToolbar);
        appGUIToolBar.setRight(viewOptions);
       
	// WE'LL RENDER OUR STUFF HERE IN THE CANVAS
        
	canvas = new Pane();
        
        canvasScroll =  new ScrollPane(canvas);
        canvasScroll.setHbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        canvasScroll.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        rightPane = new Pane();
        rightPane.setPrefWidth(RIGHT_PANE);
        
        jClassPane = new JClassClassPane(app);
        jInterfacePane = new JClassInterfacePane(app);
	
	// AND MAKE SURE THE DATA MANAGER IS IN SYNCH WITH THE PANE
	DataManager data = (DataManager)app.getDataComponent();
	data.setBoxes(canvas.getChildren());
        data.setGrid(grid);
        data.setSnap(snap);
        workArea = new BorderPane();
        workArea.setCenter(canvasScroll);
        workArea.setRight(rightPane);
	// AND NOW SETUP THE WORKSPACE
        setWorkspace(workArea);
    }
    
    public void setDebugText(String text) {
	debugText.setText(text);
    }
    
    
    private void setupHandlers() {
	// MAKE THE EDIT CONTROLLER
	jClassEditController = new JClassEditController(app);
        
        //File tool bar handler
        photo.setOnAction(e ->{
            jClassEditController.processSnapshot();
        });
        code.setOnAction(e ->{
            try {
                jClassEditController.processCode();
            } catch (Exception ex) {
                Logger.getLogger(Workspace.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

	
	// NOW CONNECT THE BUTTONS TO THEIR HANDLERS
        
	selectionTool.setOnAction(e ->{
            jClassEditController.processSelectSelectionTool();
        });
        resizeTool.setOnAction(e ->{
            jClassEditController.processResize();
        });
        addClass.setOnAction(e ->{
            jClassEditController.processCreateClassBox();
        });
        addInterface.setOnAction(e ->{
            jClassEditController.processCreateInterfaceBox();
        });
        remove.setOnAction(e ->{
            jClassEditController.processRemoveSelectedBox();
        });
        undo.setOnAction(e ->{
            jClassEditController.processUndo();
        });
        redo.setOnAction(e ->{
            jClassEditController.processRedo();
        });
        zoomIn.setOnAction(e ->{
            jClassEditController.processZoomIn();
        });
        zoomOut.setOnAction(e ->{
            jClassEditController.processZoomOut();
        });
        grid.setOnAction(e ->{
            jClassEditController.processGrid(grid.isSelected());
        });
        snap.setOnAction(e ->{
            jClassEditController.processSnap(snap.isSelected());
        });
        
	// MAKE THE CANVAS CONTROLLER	
	canvasController = new CanvasController(app);
	canvas.setOnMousePressed(e->{
	    canvasController.processCanvasMousePress((int)e.getX(), (int)e.getY(), e.getPickResult().getIntersectedNode());
	});
	canvas.setOnMouseReleased(e->{
	    canvasController.processCanvasMouseRelease((int)e.getX(), (int)e.getY());
	});
	canvas.setOnMouseDragged(e->{
	    canvasController.processCanvasMouseDragged((int)e.getX(), (int)e.getY(), e.getPickResult().getIntersectedNode());
	});
	canvas.setOnMouseExited(e->{
	    canvasController.processCanvasMouseExited((int)e.getX(), (int)e.getY());
	});
        canvasScroll.setOnKeyReleased(e ->{
            String key = e.getText();
            if(key.equals("s")){
                canvasController.processSplit();
            }
            else if(key.equals("m")){
                canvasController.processMerge();
            }
            
        });

        
        
    }
    
    public Pane getCanvas() {
	return canvas;
    }
    
    public void setImage(ButtonBase button, String fileName) {
	// LOAD THE ICON FROM THE PROVIDED FILE
        String imagePath = FILE_PROTOCOL + PATH_IMAGES + fileName;
        Image buttonImage = new Image(imagePath);
	
	// SET THE IMAGE IN THE BUTTON
        button.setGraphic(new ImageView(buttonImage));	
    }

    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamicaly as the application
     * runs so they will have their style setup separately.
     */
    @Override
    public void initStyle() {
	// NOTE THAT EACH CLASS SHOULD CORRESPOND TO
	// A STYLE CLASS SPECIFIED IN THIS APPLICATION'S
	// CSS FILE
        canvas.getStyleClass().add(CLASS_RENDER_CANVAS);
	canvasScroll.getStyleClass().add(CLASS_SCROLL_PANE);
        rightPane.getStyleClass().add(CLASS_RIGHT_PANE);
        editToolbar.getStyleClass().add(CLASS_FILE_EDIT_TOOLBAR);
        viewOptions.getStyleClass().add(CLASS_FILE_VIEW_TOOLBAR);
        photo.getStyleClass().add(CLASS_PHOTO);
        selectionTool.getStyleClass().add(CLASS_FILE_EDIT_TOOLBAR_BUTTONS);
        resizeTool.getStyleClass().add(CLASS_FILE_EDIT_TOOLBAR_BUTTONS);
        addClass.getStyleClass().add(CLASS_FILE_EDIT_TOOLBAR_BUTTONS);
        addInterface.getStyleClass().add(CLASS_FILE_EDIT_TOOLBAR_BUTTONS);
        remove.getStyleClass().add(CLASS_FILE_EDIT_TOOLBAR_BUTTONS);
        undo.getStyleClass().add(CLASS_FILE_BUTTON);
        redo.getStyleClass().add(CLASS_FILE_BUTTON);
        zoomIn.getStyleClass().add(CLASS_FILE_VIEW_TOOLBAR_BUTTONS);
        zoomOut.getStyleClass().add(CLASS_FILE_VIEW_TOOLBAR_BUTTONS);
	checkBoxToolbar.getStyleClass().add(CLASS_CHECKBOX);
        
    }

    /**
     * This function reloads all the controls for editing tag attributes into
     * the workspace.
     */
    @Override
    public void reloadWorkspace() {
	DataManager dataManager = (DataManager)app.getDataComponent();
        if(dataManager.getBoxes().size() > 0){
            selectionTool.setDisable(false);
            resizeTool.setDisable(false);
            
        }
        else{
            selectionTool.setDisable(true);
            resizeTool.setDisable(true);
        }
	if (dataManager.undo.isEmpty()){
            undo.setDisable(true);

	}
        else{
            undo.setDisable(false);
        }
        if(dataManager.redo.isEmpty()){
            redo.setDisable(true);
        }
        else{
            redo.setDisable(false);
        }
        remove.setDisable(dataManager.getSelectedBox() == null);
        photo.setDisable(false);
        code.setDisable(false);
	zoomIn.setDisable(false);
        zoomOut.setDisable(false);
        addClass.setDisable(false);
        addInterface.setDisable(false);
        grid.setDisable(false);
        snap.setDisable(false);
    }
    
    public void loadSelectedBoxSettings(Node box) {
        DataManager data = (DataManager)app.getDataComponent();
	if (box != null) {
            if(box instanceof JClassClassBox){
                JClassClassBox classBox = (JClassClassBox)box;
                //class name
                jClassPane.getClassInterfaceHbox().setText(classBox.getClassName().getText());
                //class package
                jClassPane.getPackageField().setText(classBox.getPackage());
                //load combobox
                jClassPane.loadParents(classBox, data);
                jClassPane.loadTable(classBox);
                workArea.setRight(jClassPane);
            }
            else if(box instanceof JClassInterfaceBox){
                JClassInterfaceBox interfaceBox = (JClassInterfaceBox)box;
                
                //interface name
                jInterfacePane.getClassInterfaceHbox().setText(interfaceBox.getClassName().getText());
                //package name
                jInterfacePane.getPackageField().setText(interfaceBox.getPackage());
                //load combo box
                jInterfacePane.loadParents(interfaceBox, data);
                jInterfacePane.loadTable(interfaceBox);
                
                workArea.setRight(jInterfacePane);
                
            }
    }
    }
    public BorderPane getWorkArea(){
        return workArea;
    }
    public Pane getBlankRightPane(){
        return rightPane;
    }
    public JClassClassPane getJClassPane(){
        return jClassPane;
    }
    public JClassInterfacePane getJClassInterfacePane(){
        return jInterfacePane;
    }
    public double getZoom(){
        return zoom;
    }
    public void setZoom(double d){
        zoom = d;
    }
    public ScrollPane getCanvasScroll(){
        return  canvasScroll;
    }
}

