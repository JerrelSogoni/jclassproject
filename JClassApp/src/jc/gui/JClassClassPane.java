package jc.gui;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.ArrayList;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.layout.VBoxBuilder;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import jc.controller.JClassEditController;
import jc.data.Arguments;
import jc.data.ClassAttributes;
import jc.data.ClassMethods;
import jc.data.DataManager;
import jc.data.JClassBoxes;
import static jc.data.JClassBoxes.CLASS;
import static jc.data.JClassBoxes.INTERFACE;
import jc.data.JClassClassBox;
import static jc.gui.Workspace.CLASS_RIGHT_PANE;
import static jc.gui.Workspace.CLASS_TABLE_VBOX;
import static jc.gui.Workspace.CLASS_VBOX;
import static jc.gui.Workspace.CLASS_VBOX_VM;
import static jc.gui.Workspace.RIGHT_PANE;
import saf.AppTemplate;

/**
 *
 * @author Jerrel
 */
//debating whether or no to put
public class JClassClassPane extends VBox {

    // HERE'S THE APP
    private AppTemplate app;
    DataManager dataManager;
    private JClassEditController jClassEditController;
    private VBox classInterfaceRegion;
    private HBox classInterfaceHbox;
    private Label name;
    private TextField classOrInterface;
    //region
    private VBox packageRegion;
    private HBox packageHbox;
    private Label pack;
    private TextField packageField;
    private TextField packageName;
    //parent
    private VBox parentRegion;
    private HBox parentHbox;
    private Label parent;
    private ComboBox<String> parents;
    //variables
    private VBox variableRegion;
    private HBox variableHbox;
    private Label variable;
    private Button plusVariable;
    private Button minusVariable;
    //variable table
    private VBox variableRegionTable;
    private TableView<ClassAttributes> variables;
    private ScrollPane variableScroll;
    private TableColumn vName;
    private TableColumn vType;
    private TableColumn<ClassAttributes, Boolean> vStatic;
    private TableColumn vAccess;
    private TableColumn<ClassAttributes, Boolean> vFinal;
    //methods
    private VBox methodRegion;
    private HBox methodHbox;
    private Button plusMethod;
    private Button minusMethod;
    private Label method;
    //method table
    private VBox methodRegionTable;
    private ScrollPane methodScroll;
    private TableView<ClassMethods> methods;
    private TableColumn mName;
    private TableColumn mReturn;
    private TableColumn mAccess;
    private TableColumn mStatic;
    private TableColumn mAbstract;
    private TableColumn mException;
    private TableColumn mArg;
    ObservableList<ClassAttributes> classAttributeInfo;
    ObservableList<ClassMethods> classMethodInfo;

    public JClassClassPane(AppTemplate initApp) {
        super();
        app = initApp;
        dataManager = (DataManager)app.getDataComponent();
        initGUI();
        initHandlers();
        initStyle();

    }

    public void initGUI() {

        //init Interface Region
        classInterfaceHbox = new HBox();
        classOrInterface = new TextField();
        name = new Label("Class Name:");
        classOrInterface.setPromptText("Enter Class Name");
        classInterfaceHbox.getChildren().addAll(name, classOrInterface);
        //Package
        packageHbox = new HBox();
        pack = new Label("Package:");
        packageField = new TextField();
        packageField.setPromptText("Enter Package Name");
        packageHbox.getChildren().addAll(pack, packageField);
        //Parent
        parentHbox = new HBox();
        parent = new Label("Parent:");
        parents = new ComboBox();
        parentHbox.getChildren().addAll(parent, parents);
        //Variables
        variableHbox = new HBox();
        variable = new Label("Variables:");
        plusVariable = new Button("+");
        minusVariable = new Button("-");
        variableHbox.getChildren().addAll(variable, plusVariable, minusVariable);
        //Table variables
        classAttributeInfo = FXCollections.observableArrayList();
        classMethodInfo = FXCollections.observableArrayList();

        variables = new TableView<>();
        variables.setEditable(true);

        // variables
        vName = new TableColumn("Name");
        vName.setCellValueFactory(new PropertyValueFactory<>("attributeName"));
        vType = new TableColumn("Type");
        vType.setCellValueFactory(new PropertyValueFactory<ClassAttributes, String>("attributeType"));
        vFinal = new TableColumn<>("Final");
        vFinal.setCellValueFactory(new PropertyValueFactory<ClassAttributes, Boolean>("isFinal"));
        vStatic = new TableColumn<>("Static");
        vStatic.setCellValueFactory(new PropertyValueFactory<ClassAttributes, Boolean>("isStatic"));
        vAccess = new TableColumn("Access");
        vAccess.setCellValueFactory(new PropertyValueFactory<ClassAttributes, String>("attributeAccess"));
        variables.setItems(classAttributeInfo);
        variables.getColumns().addAll(vName, vType, vFinal, vStatic, vAccess);

        //Methods
        methodHbox = new HBox();
        plusMethod = new Button("+");
        minusMethod = new Button("-");
        method = new Label("Methods:");
        methodHbox.getChildren().addAll(method, plusMethod, minusMethod);
        //method table

        methods = new TableView();
        methods.setEditable(true);
        mName = new TableColumn("Name");
        mName.setCellValueFactory(new PropertyValueFactory<>("methodName"));
        mReturn = new TableColumn("Return");
        mReturn.setCellValueFactory(new PropertyValueFactory<>("methodReturn"));
        mStatic = new TableColumn("Static");
        mStatic.setCellValueFactory(new PropertyValueFactory<>("isStatic"));
        mAbstract = new TableColumn("Abstract");
        mAbstract.setCellValueFactory(new PropertyValueFactory<>("isAbstract"));
        mAccess = new TableColumn("Access");
        mAccess.setCellValueFactory(new PropertyValueFactory<>("methodAccess"));
        mException = new TableColumn("Exception");
        mException.setCellValueFactory(new PropertyValueFactory<>("isException"));
        mArg = new TableColumn("Arg");
        methods.setItems(classMethodInfo);
        methods.getColumns().addAll(mName, mReturn, mStatic, mAbstract, mException, mAccess, mArg);

        this.getChildren().addAll(classInterfaceHbox, packageHbox, parentHbox, variableHbox,
                variables, methodHbox, methods);

        this.setPrefWidth(RIGHT_PANE);

    }

    public void initHandlers() {
        // MAKE THE EDIT CONTROLLER
        jClassEditController = new JClassEditController(app);
        // contentfilled right pane
        classOrInterface.setOnKeyReleased(e -> {
            jClassEditController.processClassName();
        });
        packageField.setOnKeyReleased(e -> {
            jClassEditController.processPackageName();
        });
        parents.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if (t1 != null) {
 dataManager.handleChange();
                    jClassEditController.processParentLines(t1);
                }

            }

        });
        plusVariable.setOnAction(e -> {
            dataManager.handleChange();
            ClassAttributes c = new ClassAttributes();
            classAttributeInfo.add(c);
            jClassEditController.processCAttributes(classAttributeInfo.get(classAttributeInfo.size() - 1));
        });
        minusVariable.setOnAction(e -> {
            ClassAttributes selected = variables.getSelectionModel().getSelectedItem();
            if (selected != null) {
                 dataManager.handleChange();
                
                jClassEditController.processRemoveCAttributes(selected);
                classAttributeInfo.remove(selected);

            }
        });
        plusMethod.setOnAction(e -> {
             dataManager.handleChange();
            ClassMethods c = new ClassMethods();
            classMethodInfo.add(c);
            jClassEditController.processCMethods(classMethodInfo.get(classMethodInfo.size() - 1));

        });
        minusMethod.setOnAction(e -> {
            
            ClassMethods selected = methods.getSelectionModel().getSelectedItem();
            if (selected != null) {
                 dataManager.handleChange();
                jClassEditController.processRemoveCMethods(selected);
                classMethodInfo.remove(selected);

            }
        });
        vName.setCellFactory(TextFieldTableCell.forTableColumn());
        vName.setOnEditCommit(
                new EventHandler<CellEditEvent<ClassAttributes, String>>() {
            @Override
            public void handle(CellEditEvent<ClassAttributes, String> t) {
                 dataManager.handleChange();
                ((ClassAttributes) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())).setAttributeName(t.getNewValue());
                jClassEditController.processCRefresh(((ClassAttributes) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())));

            }
        }
        );
        vType.setCellFactory(TextFieldTableCell.forTableColumn());
        vType.setOnEditCommit(
                new EventHandler<CellEditEvent<ClassAttributes, String>>() {
            @Override
            public void handle(CellEditEvent<ClassAttributes, String> t) {
                 dataManager.handleChange();
                ((ClassAttributes) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())).setAttributeType(t.getNewValue());
                jClassEditController.processCAttributeTypeRefresh(((ClassAttributes) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())));
                
                
            }
        }
        );
        vFinal.setCellFactory(new Callback<TableColumn<ClassAttributes, Boolean>, TableCell<ClassAttributes, Boolean>>() {
            public TableCell<ClassAttributes, Boolean> call(TableColumn<ClassAttributes, Boolean> p) {
                final TableCell<ClassAttributes, Boolean> cell = new TableCell<ClassAttributes, Boolean>() {
                    @Override
                    public void updateItem(final Boolean item, boolean empty) {
                        if (item == null) {
                            return;
                        }
                        super.updateItem(item, empty);
                        if (!isEmpty()) {
                             dataManager.handleChange();
                            final ClassAttributes selected = getTableView().getItems().get(getIndex());
                            CheckBox checkBox = new CheckBox();
                            checkBox.selectedProperty().bindBidirectional(selected.isFinalProperty());
                            setGraphic(checkBox);
                            jClassEditController.processCRefresh(selected);

                        }
                    }
                };
                cell.setAlignment(Pos.CENTER);
                return cell;
            }
        });
        vStatic.setCellFactory(new Callback<TableColumn<ClassAttributes, Boolean>, TableCell<ClassAttributes, Boolean>>() {
            public TableCell<ClassAttributes, Boolean> call(TableColumn<ClassAttributes, Boolean> p) {
                final TableCell<ClassAttributes, Boolean> cell = new TableCell<ClassAttributes, Boolean>() {
                    @Override
                    public void updateItem(final Boolean item, boolean empty) {
                        if (item == null) {
                            return;
                        }
                        super.updateItem(item, empty);
                        if (!isEmpty()) {
                             dataManager.handleChange();
                            final ClassAttributes selected = getTableView().getItems().get(getIndex());
                            CheckBox checkBox = new CheckBox();
                            checkBox.selectedProperty().bindBidirectional(selected.isStaticProperty());
                            setGraphic(checkBox);
                            jClassEditController.processCRefresh((selected));
                        }
                    }
                };
                cell.setAlignment(Pos.CENTER);
                return cell;
            }
        });
        vAccess.setCellFactory(TextFieldTableCell.forTableColumn());
        vAccess.setOnEditCommit(
                new EventHandler<CellEditEvent<ClassAttributes, String>>() {
            @Override
            public void handle(CellEditEvent<ClassAttributes, String> t) {
                 dataManager.handleChange();
                ((ClassAttributes) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())).setAttributeAccess(t.getNewValue());
                jClassEditController.processCRefresh(((ClassAttributes) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())));

            }
        });
        mName.setCellFactory(TextFieldTableCell.forTableColumn());
        mName.setOnEditCommit(
                new EventHandler<CellEditEvent<ClassMethods, String>>() {
            @Override
            public void handle(CellEditEvent<ClassMethods, String> t) {
                 dataManager.handleChange();
                ((ClassMethods) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())).setMethodName(t.getNewValue());
                jClassEditController.processCMRefresh(((ClassMethods) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())));

            }
        }
        );
        mReturn.setCellFactory(TextFieldTableCell.forTableColumn());
        mReturn.setOnEditCommit(
                new EventHandler<CellEditEvent<ClassMethods, String>>() {
            @Override
            public void handle(CellEditEvent<ClassMethods, String> t) {
                 dataManager.handleChange();
                ((ClassMethods) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())).setMethodReturn(t.getNewValue());
                

          //      jClassEditController.processCMTypeRefresh(((ClassMethods) t.getTableView().getItems().get(
       //                 t.getTablePosition().getRow())));
                jClassEditController.processCMTypeRefreshArgs(((ClassMethods) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())), ((ClassMethods) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())).getArguments());
                
            }
        }
        );
        mAccess.setCellFactory(TextFieldTableCell.forTableColumn());
        mAccess.setOnEditCommit(
                new EventHandler<CellEditEvent<ClassMethods, String>>() {
            @Override
            public void handle(CellEditEvent<ClassMethods, String> t) {
                 dataManager.handleChange();
                ((ClassMethods) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())).setMethodAccess(t.getNewValue());
                                jClassEditController.processCMRefresh(((ClassMethods) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())));
            }
        }
        );
        mStatic.setCellFactory(TextFieldTableCell.forTableColumn());
        mStatic.setCellFactory(new Callback<TableColumn<ClassMethods, Boolean>, TableCell<ClassMethods, Boolean>>() {
            public TableCell<ClassMethods, Boolean> call(TableColumn<ClassMethods, Boolean> p) {
                final TableCell<ClassMethods, Boolean> cell = new TableCell<ClassMethods, Boolean>() {
                    @Override
                    public void updateItem(final Boolean item, boolean empty) {
                        if (item == null) {
                            return;
                        }
                        super.updateItem(item, empty);
                        if (!isEmpty()) {
                             dataManager.handleChange();
                            final ClassMethods selected = getTableView().getItems().get(getIndex());
                            CheckBox checkBox = new CheckBox();
                            checkBox.selectedProperty().bindBidirectional(selected.isStaticProperty());
                            setGraphic(checkBox);
                            jClassEditController.processCMRefresh(selected);

                        }
                    }
                };
                cell.setAlignment(Pos.CENTER);
                return cell;
            }
        });
        mAbstract.setCellFactory(TextFieldTableCell.forTableColumn());
        mAbstract.setCellFactory(new Callback<TableColumn<ClassMethods, Boolean>, TableCell<ClassMethods, Boolean>>() {
            public TableCell<ClassMethods, Boolean> call(TableColumn<ClassMethods, Boolean> p) {
                final TableCell<ClassMethods, Boolean> cell = new TableCell<ClassMethods, Boolean>() {
                    @Override
                    public void updateItem(final Boolean item, boolean empty) {
                        if (item == null) {
                            return;
                        }
                        super.updateItem(item, empty);
                        if (!isEmpty()) {
                             dataManager.handleChange();
                            final ClassMethods selected = getTableView().getItems().get(getIndex());
                            CheckBox checkBox = new CheckBox();
                            checkBox.selectedProperty().bindBidirectional(selected.isAbstractProperty());
                            setGraphic(checkBox);
                            jClassEditController.processCMRefresh(selected);

                        }
                    }
                };
                cell.setAlignment(Pos.CENTER);
                return cell;
            }
        });
        mException.setCellFactory(TextFieldTableCell.forTableColumn());
        mException.setCellFactory(new Callback<TableColumn<ClassMethods, Boolean>, TableCell<ClassMethods, Boolean>>() {
            public TableCell<ClassMethods, Boolean> call(TableColumn<ClassMethods, Boolean> p) {
                final TableCell<ClassMethods, Boolean> cell = new TableCell<ClassMethods, Boolean>() {
                    @Override
                    public void updateItem(final Boolean item, boolean empty) {
                        if (item == null) {
                            return;
                        }
                        super.updateItem(item, empty);
                        if (!isEmpty()) {
                           dataManager.handleChange();  
                            final ClassMethods selected = getTableView().getItems().get(getIndex());
                            CheckBox checkBox = new CheckBox();
                            checkBox.selectedProperty().bindBidirectional(selected.isExceptionProperty());
                            setGraphic(checkBox);
                            jClassEditController.processCMRefresh(selected);

                        }
                    }
                };
                cell.setAlignment(Pos.CENTER);
                return cell;
            }
        });

        methods.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getClickCount() > 1) {
                    ClassMethods meth = methods.getSelectionModel().getSelectedItem();

                    if (meth != null) {
                        dataManager.handleChange(); 
                        handleArguments(meth);
                    }
                }
            }
        });

    }

    public TextField getClassInterfaceHbox() {
        return classOrInterface;
    }

    public TextField getPackageField() {
        return packageField;
    }

    public void initStyle() {
        this.getStyleClass().add(CLASS_RIGHT_PANE);
        classInterfaceHbox.getStyleClass().add(CLASS_VBOX);
        packageHbox.getStyleClass().add(CLASS_VBOX);
        parentHbox.getStyleClass().add(CLASS_VBOX);
        variableHbox.getStyleClass().add(CLASS_VBOX_VM);
        variables.getStyleClass().add(CLASS_TABLE_VBOX);
        methodHbox.getStyleClass().add(CLASS_VBOX_VM);
        methods.getStyleClass().add(CLASS_TABLE_VBOX);
    }

    //public void addArg() {
    //   String argNumber = String.valueOf(mArg.size() + 1);
    //   TableColumn addA = new TableColumn("Arg" + argNumber);
    //    mArg.add(addA);
    //   methods.getColumns().add(addA);
    //}
    public void addAttributeRow() {
        classAttributeInfo.add(new ClassAttributes());

    }

    public void addMethodRow() {
        classMethodInfo.add(new ClassMethods());
    }

    public ComboBox getParents() {
        return parents;
    }

    public void loadParents(JClassClassBox classBox, DataManager data) {

        ObservableList<Node> names = data.getBoxes();
        ObservableList<String> potentialParents = parents.getItems();
        ArrayList<String> boxParent = classBox.getBoxParent();
        potentialParents.add("Insert Parent");
        ArrayList<String> classNames = new ArrayList<>();
        ArrayList<String> interfaceNames = new ArrayList<>();
        for (int i = 0; i < names.size(); i++) {
            Node boxType = names.get(i);
            if (boxType instanceof JClassBoxes) {
                if (((JClassBoxes) boxType).getBoxType().equals(CLASS)) {
                    classNames.add(((JClassBoxes) boxType).getName());
                } else if (((JClassBoxes) boxType).getBoxType().equals(INTERFACE)) {
                    interfaceNames.add(((JClassBoxes) boxType).getName());
                }

            }
        }
        for (int j = 0; j < names.size(); j++) {
            Node boxes = names.get(j);
            if (boxes instanceof JClassBoxes) {
                if (!((JClassBoxes) boxes).getName().equals(classBox.getName())) {
                    potentialParents.add(((JClassBoxes) boxes).getName());
                }
            }
        }
        parents.setCellFactory(
                new Callback<ListView<String>, ListCell<String>>() {
            @Override
            public ListCell<String> call(ListView<String> param) {
                final ListCell<String> cell = new ListCell<String>() {

                    @Override
                    public void updateItem(String item,
                            boolean empty) {
                        super.updateItem(item, empty);

                        if (item != null) {
                            setText(item);
                            if (classNames.contains(item)) {
                                setTextFill(Color.RED);
                            } else if (interfaceNames.contains(item)) {
                                setTextFill(Color.BLUE);

                            }
                            if (boxParent.contains(item)) {
                                for (String string : boxParent) {
                                    if (string.equals(item)) {

                                        setUnderline(true);
                                    }
                                }
                            }

                        } else {
                            setText("");
                        }
                    }
                };
                return cell;
            }

        });
    }

    public TableView getVariables() {
        return variables;
    }

    public void clearTable() {
        if (classAttributeInfo != null) {
            classAttributeInfo.clear();
        }
        if (classMethodInfo != null) {
            classMethodInfo.clear();
        }

    }

    public void loadTable(JClassClassBox classBox) {
        ObservableList<ClassAttributes> a = classBox.getClassAttributes();
        if (a != null) {
            for (int i = 0; i < a.size(); i++) {
                classAttributeInfo.add(a.get(i));
            }
        }
        ObservableList<ClassMethods> c = classBox.getClassMethods();
        if (c != null) {
            for (int i = 0; i < c.size(); i++) {
                classMethodInfo.add(c.get(i));
            }
        }

    }

    public void handleArguments(ClassMethods a) {
        Stage dialogStage = new Stage();
        TableView<Arguments> apples = new TableView();
        apples.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        apples.setEditable(true);
        ArrayList<String> b = a.getArguments();
        ObservableList<Arguments> args = FXCollections.observableArrayList();
        for (String string : b) {
            Arguments arg = new Arguments(string);
            args.add(arg);
        }
        TableColumn argss = new TableColumn("Arg Type");
        argss.setCellValueFactory(new PropertyValueFactory<>("argType"));
        apples.setItems(args);
        apples.getColumns().add(argss);

        Button ok = new Button("Done");
        Button plus = new Button("+");
        Button minus = new Button("-");
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.setScene(new Scene(VBoxBuilder.create().
                children(new Text("Insert Arguments"), apples, plus, minus, ok).
                alignment(Pos.CENTER).padding(new Insets(20)).build()));
        dialogStage.show();
        plus.setOnAction(e -> {
            Arguments arg1 = new Arguments("");
            args.add(arg1);
        });
        minus.setOnAction(e -> {
            Arguments args3 = (Arguments) apples.getSelectionModel().getSelectedItem();
            args.remove(args3);
        });
        ok.setOnAction(e -> {
            b.clear();
            for (int i = 0; i < args.size(); i++) {
                b.add(args.get(i).getArgType());
            }
             dataManager.handleChange();
            jClassEditController.processCMTypeRefreshArgs(a, a.getArguments());
            dialogStage.close();
        });
        argss.setCellFactory(TextFieldTableCell.forTableColumn());
        argss.setOnEditCommit(
                new EventHandler<CellEditEvent<Arguments, String>>() {
            @Override
            public void handle(CellEditEvent<Arguments, String> t) {
                ((Arguments) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())).setArgType(t.getNewValue());

            }
        });

    }

    public TableView getMethods() {
        return methods;
    }
}
