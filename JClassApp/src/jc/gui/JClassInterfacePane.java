/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jc.gui;

/**
 *
 * @author Jerrel
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.ArrayList;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.layout.VBoxBuilder;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import jc.controller.JClassEditController;
import jc.data.Arguments;
import jc.data.ClassAttributes;
import jc.data.ClassMethods;
import jc.data.DataManager;
import jc.data.InterfaceAttributes;
import jc.data.InterfaceMethods;
import jc.data.JClassBoxes;
import static jc.data.JClassBoxes.CLASS;
import static jc.data.JClassBoxes.INTERFACE;
import jc.data.JClassInterfaceBox;
import static jc.gui.Workspace.CLASS_RIGHT_PANE;
import static jc.gui.Workspace.CLASS_TABLE_VBOX;
import static jc.gui.Workspace.CLASS_VBOX;
import static jc.gui.Workspace.CLASS_VBOX_VM;
import static jc.gui.Workspace.RIGHT_PANE;
import saf.AppTemplate;

/**
 *
 * @author Jerrel
 */
//debating whether or no to put
public class JClassInterfacePane extends VBox {

    // HERE'S THE APP
    private AppTemplate app;
    DataManager dataManager;
    private JClassEditController jClassEditController;
    private VBox classInterfaceRegion;
    private HBox classInterfaceHbox;
    private Label name;
    private TextField classOrInterface;
    //region
    private VBox packageRegion;
    private HBox packageHbox;
    private Label pack;
    private TextField packageField;
    private TextField packageName;
    //parent
    private VBox parentRegion;
    private HBox parentHbox;
    private Label parent;
    private ComboBox<String> parents;
    //variables
    private VBox variableRegion;
    private HBox variableHbox;
    private Label variable;
    private Button plusVariable;
    private Button minusVariable;
    //variable table
    private VBox variableRegionTable;
    private TableView<InterfaceAttributes> variables;

    private TableColumn vName;
    private TableColumn vType;
    private TableColumn vStatic;
    private TableColumn vAccess;
    //methods
    private VBox methodRegion;
    private HBox methodHbox;
    private Button plusMethod;
    private Button minusMethod;
    private Label method;
    //method table
    private VBox methodRegionTable;

    private TableView<InterfaceMethods> methods;
    private TableColumn mName;
    private TableColumn mReturn;
    private TableColumn mException;
    private TableColumn mAccess;
    private TableColumn mArg;
    ObservableList<InterfaceAttributes> interfaceAttributeInfo;
    ObservableList<InterfaceMethods> interfaceMethodInfo;

    public JClassInterfacePane(AppTemplate initApp) {
        super();
        app = initApp;
        dataManager = (DataManager)app.getDataComponent();
        initGUI();
        initHandlers();
        initStyle();

    }

    public void initGUI() {

        //init Interface Region
        classInterfaceHbox = new HBox();
        classOrInterface = new TextField();
        name = new Label("Interface Name:");
        classOrInterface.setPromptText("Enter Class Name");
        classInterfaceHbox.getChildren().addAll(name, classOrInterface);
        //Package
        packageHbox = new HBox();
        pack = new Label("Package:");
        packageField = new TextField();
        packageField.setPromptText("Enter Package Name");
        packageHbox.getChildren().addAll(pack, packageField);
        //Parent
        parentHbox = new HBox();
        parent = new Label("Parent:");
        parents = new ComboBox();
        parentHbox.getChildren().addAll(parent, parents);
        //Variables
        variableHbox = new HBox();
        variable = new Label("Variables:");
        plusVariable = new Button("+");
        minusVariable = new Button("-");
        variableHbox.getChildren().addAll(variable, plusVariable, minusVariable);
        //Table variables
        interfaceAttributeInfo = FXCollections.observableArrayList();
        interfaceMethodInfo = FXCollections.observableArrayList();
        variables = new TableView<>();
        variables.setEditable(true);
        vName = new TableColumn("Name");
        vName.setCellValueFactory(new PropertyValueFactory<>("attributeName"));
        vType = new TableColumn("Type");
        vType.setCellValueFactory(new PropertyValueFactory<InterfaceAttributes, String>("attributeType"));
        vAccess = new TableColumn("Access");
        vAccess.setCellValueFactory(new PropertyValueFactory<InterfaceAttributes, String>("attributeAccess"));
        variables.setItems(interfaceAttributeInfo);
        variables.getColumns().addAll(vName, vType, vAccess);

        //Methods
        methodHbox = new HBox();
        plusMethod = new Button("+");
        minusMethod = new Button("-");
        method = new Label("Methods:");
        methodHbox.getChildren().addAll(method, plusMethod, minusMethod);
        //method table
        methods = new TableView();
        methods.setEditable(true);
        mName = new TableColumn("Name");
        mName.setCellValueFactory(new PropertyValueFactory<>("methodName"));
        mReturn = new TableColumn("Return");
        mReturn.setCellValueFactory(new PropertyValueFactory<>("methodReturn"));
        mException = new TableColumn("isException");
        mException.setCellValueFactory(new PropertyValueFactory<>("isException"));
        mAccess = new TableColumn("Access");
        mAccess.setCellValueFactory(new PropertyValueFactory<>("methodAccess"));
        mArg = new TableColumn("Arg");
        methods.setItems(interfaceMethodInfo);
        methods.getColumns().addAll(mName, mReturn, mAccess, mException, mArg);

        this.getChildren().addAll(classInterfaceHbox, packageHbox, parentHbox, variableHbox,
                variables, methodHbox, methods);

        this.setPrefWidth(RIGHT_PANE);

    }

    public void initHandlers() {
        // MAKE THE EDIT CONTROLLER
        jClassEditController = new JClassEditController(app);
        // contentfilled right pane
        classOrInterface.setOnKeyReleased(e -> {
            jClassEditController.processClassName();
        });
        packageField.setOnKeyReleased(e -> {
            jClassEditController.processPackageName();
        });
        parents.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if (t1 != null) {
                     dataManager.handleChange();
                    jClassEditController.processParentLines(t1);
                }

            }

        });
        plusVariable.setOnAction(e -> {
             dataManager.handleChange();
            InterfaceAttributes c = new InterfaceAttributes();
            interfaceAttributeInfo.add(c);
            jClassEditController.processIAttributes(interfaceAttributeInfo.get(interfaceAttributeInfo.size() - 1));
        });
        minusVariable.setOnAction(e -> {
             dataManager.handleChange();
            InterfaceAttributes selected = variables.getSelectionModel().getSelectedItem();
            if (selected != null) {
                jClassEditController.processRemoveIAttributes(selected);
                interfaceAttributeInfo.remove(selected);

            }
        });
        plusMethod.setOnAction(e -> {
            
             dataManager.handleChange();
            InterfaceMethods c = new InterfaceMethods();
            interfaceMethodInfo.add(c);
            jClassEditController.processIMethods(interfaceMethodInfo.get(interfaceMethodInfo.size() - 1));

        });
        minusMethod.setOnAction(e -> {
            
 dataManager.handleChange();
            InterfaceMethods selected = methods.getSelectionModel().getSelectedItem();
            if (selected != null) {
                 dataManager.handleChange();
                jClassEditController.processRemoveIMethods(selected);
                interfaceMethodInfo.remove(selected);

            }
        });
        vName.setCellFactory(TextFieldTableCell.forTableColumn());
        vName.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<InterfaceAttributes, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<InterfaceAttributes, String> t) {
                 dataManager.handleChange();
                ((InterfaceAttributes) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())).setAttributeName(t.getNewValue());
                jClassEditController.processIRefresh(((InterfaceAttributes) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())));
            }
        }
        );
        vType.setCellFactory(TextFieldTableCell.forTableColumn());
        vType.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<InterfaceAttributes, String>>() {
                    
            @Override
            public void handle(TableColumn.CellEditEvent<InterfaceAttributes, String> t) {
                 dataManager.handleChange();
                ((InterfaceAttributes) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())).setAttributeType(t.getNewValue());
                jClassEditController.processIAttributeTypeRefresh(((InterfaceAttributes) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())));
            }
        }
        );
        vAccess.setCellFactory(TextFieldTableCell.forTableColumn());
        vAccess.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<InterfaceAttributes, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<InterfaceAttributes, String> t) {
                 dataManager.handleChange();
                ((InterfaceAttributes) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())).setAttributeAccess(t.getNewValue());
                jClassEditController.processIRefresh(((InterfaceAttributes) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())));
            }
        });
        mName.setCellFactory(TextFieldTableCell.forTableColumn());
        mName.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<InterfaceMethods, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<InterfaceMethods, String> t) {
                 dataManager.handleChange();
                ((InterfaceMethods) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())).setMethodName(t.getNewValue());
                jClassEditController.processIMRefresh(((InterfaceMethods) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())));
            }
        });
        mReturn.setCellFactory(TextFieldTableCell.forTableColumn());
        mReturn.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<InterfaceMethods, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<InterfaceMethods, String> t) {
                 dataManager.handleChange();
                ((InterfaceMethods) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())).setMethodReturn(t.getNewValue());
                jClassEditController.processIMTypeRefreshArgs(((InterfaceMethods) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())), ((InterfaceMethods) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())).getArguments());
            }
        });
        mAccess.setCellFactory(TextFieldTableCell.forTableColumn());
        mAccess.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<InterfaceMethods, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<InterfaceMethods, String> t) {
                 dataManager.handleChange();
                ((InterfaceMethods) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())).setMethodAccess(t.getNewValue());
                jClassEditController.processIMRefresh(((InterfaceMethods) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())));
            }
        });
        mException.setCellFactory(TextFieldTableCell.forTableColumn());
        mException.setCellFactory(new Callback<TableColumn<InterfaceMethods, Boolean>, TableCell<InterfaceMethods, Boolean>>() {
            public TableCell<InterfaceMethods, Boolean> call(TableColumn<InterfaceMethods, Boolean> p) {
                final TableCell<InterfaceMethods, Boolean> cell = new TableCell<InterfaceMethods, Boolean>() {
                    @Override
                    public void updateItem(final Boolean item, boolean empty) {
                        if (item == null) {
                            return;
                        }
                        super.updateItem(item, empty);
                        if (!isEmpty()) {
                             dataManager.handleChange();
                            final InterfaceMethods selected = getTableView().getItems().get(getIndex());
                            CheckBox checkBox = new CheckBox();
                            checkBox.selectedProperty().bindBidirectional(selected.isExceptionProperty());
                            setGraphic(checkBox);
                            jClassEditController.processIMRefresh(selected);

                        }
                    }
                };
                cell.setAlignment(Pos.CENTER);
                return cell;
            }
        });

        methods.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getClickCount() > 1) {
                    InterfaceMethods meth = methods.getSelectionModel().getSelectedItem();

                    if (meth != null) {
                         dataManager.handleChange();
                        handleArguments(meth);
                    }
                }
            }
        });

    }

    public TextField getClassInterfaceHbox() {
        return classOrInterface;
    }

    public TextField getPackageField() {
        return packageField;
    }

    public void initStyle() {
        this.getStyleClass().add(CLASS_RIGHT_PANE);
        classInterfaceHbox.getStyleClass().add(CLASS_VBOX);
        packageHbox.getStyleClass().add(CLASS_VBOX);
        parentHbox.getStyleClass().add(CLASS_VBOX);
        variableHbox.getStyleClass().add(CLASS_VBOX_VM);
        variables.getStyleClass().add(CLASS_TABLE_VBOX);
        methodHbox.getStyleClass().add(CLASS_VBOX_VM);
        methods.getStyleClass().add(CLASS_TABLE_VBOX);
    }

    public void addAttributeRow() {
        interfaceAttributeInfo.add(new InterfaceAttributes());

    }

    public void addMethodRow() {
        interfaceMethodInfo.add(new InterfaceMethods());

    }

    public ComboBox getParents() {
        return parents;
    }

    public void loadParents(JClassInterfaceBox interfaceBox, DataManager data) {
        ObservableList<Node> names = data.getBoxes();
        Platform.runLater(() -> {
            ObservableList<String> potentialParents = parents.getItems();
            String parentsInterface = interfaceBox.getBoxParent();
            potentialParents.add("Insert Parent");
            ArrayList<String> classNames = new ArrayList<>();
            ArrayList<String> interfaceNames = new ArrayList<>();
            for (Node boxType : names) {
                if (boxType instanceof JClassBoxes) {
                    if (((JClassBoxes) boxType).getBoxType().equals(CLASS)) {
                        classNames.add(((JClassBoxes) boxType).getName());
                    } else if (((JClassBoxes) boxType).getBoxType().equals(INTERFACE)) {
                        interfaceNames.add(((JClassBoxes) boxType).getName());
                    }

                }
            }
            for (Node boxes : names) {
                if (boxes instanceof JClassBoxes) {
                    if (!((JClassBoxes) boxes).getName().equals(interfaceBox.getName())) {
                        potentialParents.add(((JClassBoxes) boxes).getName());
                    }
                }
            }
            parents.setCellFactory(
                    new Callback<ListView<String>, ListCell<String>>() {
                @Override
                public ListCell<String> call(ListView<String> param) {
                    final ListCell<String> cell = new ListCell<String>() {
                        {
                        }

                        @Override
                        public void updateItem(String item,
                                boolean empty) {
                            super.updateItem(item, empty);
                            if (item != null) {
                                setText(item);
                                if (classNames.contains(item)) {
                                    setTextFill(Color.RED);
                                } else if (interfaceNames.contains(item)) {
                                    setTextFill(Color.BLUE);

                                }
                                if (parentsInterface.contains(item)) {
                                    setUnderline(true);
                                }

                            } else {
                                setText(null);
                            }
                        }
                    };
                    return cell;
                }

            });
        });

    }

    public TableView getVariables() {
        return variables;
    }

    public void clearTable() {
        if (interfaceAttributeInfo != null) {
            interfaceAttributeInfo.clear();
        }
        if (interfaceMethodInfo != null) {
            interfaceMethodInfo.clear();
        }


    }

    public void loadTable(JClassInterfaceBox interfaceBox) {
        ObservableList<InterfaceAttributes> a = interfaceBox.getInterfaceAttributes();
        if (a != null) {

            for (int i = 0; i < a.size(); i++) {
                interfaceAttributeInfo.add(a.get(i));
            }
        }

        ObservableList<InterfaceMethods> c = interfaceBox.getInterfaceMethods();
        if (c != null) {
            for (int i = 0; i < c.size(); i++) {
                interfaceMethodInfo.add(c.get(i));
            }
        }
    }

    public TableView getMethods() {
        return methods;
    }

    public void handleArguments(InterfaceMethods a) {
        Stage dialogStage = new Stage();
        TableView<Arguments> apples = new TableView();
        apples.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        apples.setEditable(true);
        ArrayList<String> b = a.getArguments();
        ObservableList<Arguments> args = FXCollections.observableArrayList();
        for (String string : b) {
            Arguments arg = new Arguments(string);
            args.add(arg);
        }
        TableColumn argss = new TableColumn("Arg Type");
        argss.setCellValueFactory(new PropertyValueFactory<>("argType"));
        apples.setItems(args);
        apples.getColumns().add(argss);

        Button ok = new Button("Done");
        Button plus = new Button("+");
        Button minus = new Button("-");
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.setScene(new Scene(VBoxBuilder.create().
                children(new Text("Insert Arguments"), apples, plus, minus, ok).
                alignment(Pos.CENTER).padding(new Insets(20)).build()));
        dialogStage.show();
        plus.setOnAction(e -> {
            Arguments arg1 = new Arguments("");
            args.add(arg1);
        });
        minus.setOnAction(e -> {
            Arguments args3 = (Arguments) apples.getSelectionModel().getSelectedItem();
            args.remove(args3);
        });
        ok.setOnAction(e -> {
            b.clear();
            for (int i = 0; i < args.size(); i++) {
                b.add(args.get(i).getArgType());
            }
 jClassEditController.processIMTypeRefreshArgs(a, a.getArguments());
            dialogStage.close();
        });
        argss.setCellFactory(TextFieldTableCell.forTableColumn());
        argss.setOnEditCommit(
                new EventHandler<CellEditEvent<Arguments, String>>() {
            @Override
            public void handle(CellEditEvent<Arguments, String> t) {
                ((Arguments) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())).setArgType(t.getNewValue());

            }
        });

    }

}
