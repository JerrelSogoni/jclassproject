package jc.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.VBoxBuilder;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import jc.data.ClassAttributes;
import jc.data.ClassMethods;
import jc.data.DataManager;
import static jc.data.DataManager.EXTENDS;
import static jc.data.DataManager.IMPLEMENTS;
import jc.data.InterfaceAttributes;
import jc.data.InterfaceMethods;
import jc.data.JClassBoxes;
import static jc.data.JClassBoxes.CLASS;
import static jc.data.JClassBoxes.INTERFACE;
import jc.data.JClassClassBox;
import jc.data.JClassInterfaceBox;
import jc.data.JClassMakerState;
import jc.data.JLine;
import jc.gui.Workspace;
import properties_manager.PropertiesManager;
import saf.AppTemplate;
import static saf.settings.AppPropertyType.PROPERTIES_LOAD_ERROR_MESSAGE;
import static saf.settings.AppPropertyType.PROPERTIES_LOAD_ERROR_TITLE;
import static saf.settings.AppPropertyType.SAVE_COMPLETED_MESSAGE;
import static saf.settings.AppPropertyType.SAVE_COMPLETED_TITLE;
import static saf.settings.AppPropertyType.SAVE_ERROR_MESSAGE;
import static saf.settings.AppPropertyType.SAVE_ERROR_TITLE;
import saf.ui.AppMessageDialogSingleton;

/**
 * This class responds to interactions with other UI pose editing controls.
 *
 * @author McKillaGorilla
 * @version 1.0
 */
public class JClassEditController {

    AppTemplate app;

    DataManager dataManager;

    public JClassEditController(AppTemplate initApp) {
        app = initApp;
        dataManager = (DataManager) app.getDataComponent();
    }

    public void processSelectSelectionTool() {
        // CHANGE THE CURSOR
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.DEFAULT);

        // CHANGE THE STATE
        dataManager.setState(JClassMakerState.SELECTING_BOX);

        // ENABLE/DISABLE THE PROPER BUTTONS
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace();
    }

    public void processRemoveSelectedBox() {
        // REMOVE THE SELECTED SHAPE IF THERE IS ONE
        dataManager.handleChange();
        dataManager.removeSelectedBox();

        // ENABLE/DISABLE THE PROPER BUTTONS
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        workspace.getWorkArea().setRight(workspace.getBlankRightPane());
        workspace.reloadWorkspace();
        app.getGUI().updateToolbarControls(false);
    }

    public void processSnapshot() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        WritableImage ourCanvas = workspace.getCanvas().snapshot(new SnapshotParameters(), null);
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save Picture");
        fileChooser.setInitialFileName(".png");
        File selectedFile = fileChooser.showSaveDialog(app.getGUI().getWindow());
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(ourCanvas, null), "png", selectedFile);
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show("Photo Successful", "Photo Saved in Specified Location");
        } catch (IOException e) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show("Alert", "Critical error while saving picture");
        }
    }

    public void processCode() throws Exception {
        try {
            ObservableList<Node> boxes = dataManager.getBoxes();
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Save Code");
            fileChooser.setInitialFileName("");
            File selectedFile = fileChooser.showSaveDialog(app.getGUI().getWindow());
            if (selectedFile.getPath().contains("JClass")) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show("Alert", "Cannot Save in Project Directory");
            }
            String referenceMainFolder = selectedFile.getAbsolutePath() + "/";
            selectedFile.mkdir();
            String referenceSourceFolder = referenceMainFolder + "src";
            File file = new File(selectedFile.getAbsolutePath());

            for (Node node : boxes) {
                if (node instanceof JClassBoxes) {
                    StringBuilder javaBuilder = new StringBuilder();
                    JClassBoxes box = (JClassBoxes) node;
                    if (primitivetype(box.getName(), box)) {
                        continue;
                    }
                    File src = new File(referenceSourceFolder);
                    if (!src.exists()) {
                        src.mkdir();
                    }
                    String packages = covertdotsToSlashes(box.getPackage());
                    if (packages.equals("")) {

                        File mkSrc = new File(src.getAbsoluteFile() + "/" + box.getName() + ".java");
                        createJavaImports(box, javaBuilder);
                        makeClassorInt(boxes, box, javaBuilder);
                        makeAttributes(box, javaBuilder);
                        makeMethods(box, javaBuilder);
                        javaBuilder.append("\n")
                                .append("}");
                        makeJavaFile(mkSrc, javaBuilder);

                    } else {
                        File packageable = new File(referenceSourceFolder + "/" + packages);
                        if (!packageable.exists()) {
                            packageable.mkdirs();
                        }
                        File mkJava = new File(packageable + "/" + box.getName() + ".java");
                        String replaceSlashWithDots = packages.replace("/", ".");
                        javaBuilder.append("package " + replaceSlashWithDots + ";\n");
                        createJavaImports(box, javaBuilder);
                        makeClassorInt(boxes, box, javaBuilder);
                        makeAttributes(box, javaBuilder);
                        makeMethods(box, javaBuilder);
                        javaBuilder.append("\n")
                                .append("}");
                        makeJavaFile(mkJava, javaBuilder);

                    }

                }
            }
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            dialog.show(props.getProperty(SAVE_COMPLETED_TITLE), props.getProperty(SAVE_COMPLETED_MESSAGE));

        } catch (Exception e) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            dialog.show(props.getProperty(SAVE_ERROR_TITLE), props.getProperty(SAVE_ERROR_MESSAGE));
            e.printStackTrace();

        }

    }

    public void createJavaImports(JClassBoxes node, StringBuilder a) {
        //// remember do not import if in same package
        ArrayList<String> keys = new ArrayList<>();
        ArrayList<String> values = new ArrayList<>();
        if (node instanceof JClassClassBox) {
            ObservableList<ClassAttributes> atts = ((JClassClassBox) node).getClassAttributes();
            if (atts != null) {
                for (ClassAttributes attributes : atts) {
                    String pop = attributes.getAttributeType();
                    int hashSizeCheck = keys.size();
                    checkAPI(pop, keys, values);
                    // must not be a java api pack
                    if (hashSizeCheck == keys.size()) {
                        checkourStuff(pop, keys, values);
                    }
                }
            }
            ObservableList<ClassMethods> meths = ((JClassClassBox) node).getClassMethods();
            if (meths != null) {
                for (ClassMethods methss : meths) {
                    String type = methss.getMethodReturn();
                    String pop = type;
                    int hashSizeCheck = keys.size();
                    checkAPI(pop, keys, values);
                    // must not be a java api pack
                    if (hashSizeCheck == keys.size()) {
                        checkourStuff(pop, keys, values);
                    }

                    ArrayList<String> ourArgs = methss.getArguments();
                    if (ourArgs != null) {
                        for (String s : ourArgs) {
                            String pop2 = s;
                            int hashSizeCheck2 = keys.size();
                            checkAPI(pop2, keys, values);
                            // must not be a java api pack
                            if (hashSizeCheck2 == keys.size()) {
                                checkourStuff(pop2, keys, values);
                            }

                        }
                    }

                }
            }
        } else if (node instanceof JClassInterfaceBox) {
            ObservableList<InterfaceAttributes> atts = ((JClassInterfaceBox) node).getInterfaceAttributes();
            if (atts != null) {
                for (InterfaceAttributes attributes : atts) {
                    String pop = attributes.getAttributeType();
                    int hashSizeCheck = keys.size();
                    checkAPI(pop, keys, values);
                    // must not be a java api pack
                    if (hashSizeCheck == keys.size()) {
                        checkourStuff(pop, keys, values);
                    }
                }
            }
            ObservableList<InterfaceMethods> meths = ((JClassInterfaceBox) node).getInterfaceMethods();
            if (meths != null) {
                for (InterfaceMethods methss : meths) {
                    String type = methss.getMethodReturn();
                    String pop = type;
                    int hashSizeCheck = keys.size();
                    checkAPI(pop, keys, values);
                    // must not be a java api pack
                    if (hashSizeCheck == keys.size()) {
                        checkourStuff(pop, keys, values);
                    }
                    ArrayList<String> ourArgs = methss.getArguments();
                    if (ourArgs != null) {
                        for (String s : ourArgs) {
                            String pop2 = s;
                            int hashSizeCheck2 = keys.size();
                            checkAPI(pop2, keys, values);
                            // must not be a java api pack
                            if (hashSizeCheck == keys.size()) {
                                checkourStuff(pop2, keys, values);
                            }

                        }
                    }

                }
            }
        }
        ArrayList<String> classes = new ArrayList<>();
        ArrayList<String> interfaces = new ArrayList<>();
        for (Node l : dataManager.getBoxes()) {
            if (l instanceof JLine) {
                JLine ourLine = (JLine) l;
                if (ourLine.getJChildren().equals(node)) {
                    if (ourLine.getType().equals(EXTENDS)) {
                        classes.add(ourLine.getJParent().getName());
                    } else if (ourLine.getType().equals(IMPLEMENTS)) {
                        interfaces.add(ourLine.getJParent().getName());

                    }
                }
            }
        }
        if (!classes.isEmpty()) {
            //should only be one extends
            //find again
            for (String s : classes) {
                String pop2 = s;
                int hashSizeCheck2 = keys.size();
                checkAPI(pop2, keys, values);
                // must not be a java api pack
                if (hashSizeCheck2 == keys.size()) {
                    checkourStuff(pop2, keys, values);
                }

            }

        }
        if (!interfaces.isEmpty()) {
            for (String s : interfaces) {
                String pop2 = s;
                int hashSizeCheck2 = keys.size();
                checkAPI(pop2, keys, values);
                // must not be a java api pack
                if (hashSizeCheck2 == keys.size()) {
                    checkourStuff(pop2, keys, values);
                }

            }
        }

        for (int i = 0; i < keys.size(); i++) {
            if (values.get(i).equals(node.getPackage())) {
                continue;
            }
            if (!node.getPackage().equals("") && values.get(i).equals("")) {
                continue;
            }
            if (values.get(i).equals("java.lang")) {
                continue;
            }
            if (values.get(i).equals(node.getName())) {
                continue;
            }
            String thishasToWork = values.get(i) + "." + keys.get(i);

            a.append("import " + thishasToWork + ";\n");
        }
    }

    public void processResize() {
        dataManager.setState(JClassMakerState.SIZING_BOX);
    }

    public void processUndo() {
        dataManager.handleUndo();
    }

    public void processRedo() {
        dataManager.handleRedo();
    }

    public void processZoomIn() {
        dataManager.handleChange();
        dataManager.zoomIn();

    }

    public void processZoomOut() {
        dataManager.handleChange();
        dataManager.zoomOut();
    }

    public void processGrid(boolean a) {
        dataManager.handleChange();
        if (a) {
            dataManager.initGrid();
            dataManager.getBoxes().addAll(0, dataManager.getGridLines());
        } else {
            dataManager.removeGridLine();

        }

    }

    public void processSnap(boolean a) {
        dataManager.handleChange();
        dataManager.snapPlease(a);

    }

    public void processCreateClassBox() {
        // CHANGE THE CURSOR
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.CROSSHAIR);
        // CHANGE THE STATE
        dataManager.setState(JClassMakerState.STARTING_CLASS);

        // ENABLE/DISABLE THE PROPER BUTTONS
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace();

    }

    public void processClassName() {
        dataManager.handleChange();
        dataManager.changeClassName();
    }

    public void processPackageName() {
        dataManager.handleChange();
        dataManager.changePackageName();

    }

    public void processCreateInterfaceBox() {
        // CHANGE THE CURSOR
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.CROSSHAIR);
        // CHANGE THE STATE
        dataManager.setState(JClassMakerState.STARTING_INTERFACE);

        // ENABLE/DISABLE THE PROPER BUTTONS
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace();

    }

    private String covertdotsToSlashes(String aPackage) {
        if (aPackage.contains(".")) {
            String replaced = aPackage.replace(".", "/");
            if (replaced.endsWith("/")) {
                String uniformity = replaced.substring(0, replaced.length() - 1);
                return uniformity;
            }
            return replaced;
        }
        return aPackage;
    }

    public boolean disAllowPrimitives(String a) {
        ArrayList<String> prims = new ArrayList<>();
        return true;

    }

    public void makeAttributes(JClassBoxes box, StringBuilder a) {
        if (box instanceof JClassClassBox) {
            ObservableList<ClassAttributes> ats = ((JClassClassBox) box).getClassAttributes();
            if (ats != null) {
                for (ClassAttributes attribute : ats) {
                    String access = attribute.getAttributeAccess();
                    String returnType = attribute.getAttributeType();
                    String name = attribute.getAttributeName();
                    boolean isFinal = attribute.getIsFinal();
                    boolean isStatic = attribute.getIsStatic();
                    a.append(access);
                    if (isStatic) {

                        a.append(" ")
                                .append("static");

                    }
                    if (isFinal) {
                        a.append(" ")
                                .append("final");
                    }
                    a.append(" ");
                    a.append(returnType);
                    a.append(" ");
                    a.append(name);
                    a.append(";");
                    a.append("\n");
                }
            }
        }
        if (box instanceof JClassInterfaceBox) {
            ObservableList<InterfaceAttributes> ats = ((JClassInterfaceBox) box).getInterfaceAttributes();
            if (ats != null) {
                for (InterfaceAttributes attribute : ats) {
                    String access = attribute.getAttributeAccess();
                    String returnType = attribute.getAttributeType();
                    String name = attribute.getAttributeName();
                    a.append(access)
                            .append(" ")
                            .append(returnType)
                            .append(" ")
                            .append(name)
                            .append(";")
                            .append("\n");
                }
            }

        }

    }

    public void makeClassorInt(ObservableList<Node> lines, JClassBoxes box, StringBuilder a) {
        a.append("public ");
        if (box instanceof JClassClassBox) {
            if (box.isAbstract()) {
                a.append("abstract ");
            }
            a.append("class ");

        } else if (box instanceof JClassInterfaceBox) {
            a.append("interface ");

        }
        a.append(box.getName());
        ArrayList<String> classes = new ArrayList<>();
        ArrayList<String> interfaces = new ArrayList<>();
        for (Node l : lines) {
            if (l instanceof JLine) {
                JLine ourLine = (JLine) l;
                if (ourLine.getJChildren().equals(box)) {
                    if (ourLine.getType().equals(EXTENDS)) {
                        classes.add(ourLine.getJParent().getName());
                    } else if (ourLine.getType().equals(IMPLEMENTS)) {
                        interfaces.add(ourLine.getJParent().getName());

                    }
                }
            }
        }
        if (!classes.isEmpty()) {
            //should only be one extends
            a.append(" extends ")
                    .append(classes.get(0));
        }
        if (!interfaces.isEmpty()) {
            a.append(" implements ");
            int i = 0;
            for (String each : interfaces) {
                a.append(each);
                i++;
                if (i < interfaces.size()) {
                    a.append(",");
                }
            }

        }
        a.append("{")
                .append("\n");
    }

    public void makeMethods(JClassBoxes box, StringBuilder a) {
        if (box instanceof JClassClassBox) {
            ObservableList<ClassMethods> ats = ((JClassClassBox) box).getClassMethods();
            if (ats != null) {
                for (ClassMethods method : ats) {
                    String access = method.getMethodAccess();
                    String returnType = method.getMethodReturn();
                    String name = method.getMethodName();
                    boolean abst = method.getIsAbstract();
                    boolean except = method.getIsException();
                    boolean isStatic = method.getIsStatic();
                    ArrayList<String> args = method.getArguments();

                    a.append(access + " ");
                    if (abst) {
                        a.append("abstract ");
                    }
                    if (isStatic) {
                        a.append("static ");
                    }
                    a.append(returnType + " ");
                    a.append(name);
                    a.append("(");
                    if (args != null && args.size() != 0) {
                        int count = 1;
                        for (int i = 0; i < args.size(); i++) {
                            String arrrg = args.get(i);
                            a.append(arrrg + " ");
                            a.append("arg" + count++);
                            if (i + 1 < args.size()) {
                                a.append(",");
                            }

                        }
                    }
                    a.append(") ");
                    if (except) {
                        a.append("throws Exception");
                    }
                    a.append("{");
                    a.append("\n");
                    if (returnType.contains("void")) {
                        a.append("System.out.println(\"Please implement this void Method\");\n");
                    } else {
                        a.append("System.out.println(\"Please implement this return Method\");\n");
                        a.append("return null;\n");
                    }
                    a.append("}\n");

                }
            }
        }
        if (box instanceof JClassInterfaceBox) {
            ObservableList<InterfaceMethods> ats = ((JClassInterfaceBox) box).getInterfaceMethods();
            if (ats != null) {
                for (InterfaceMethods method : ats) {
                    String access = method.getMethodAccess();
                    String returnType = method.getMethodReturn();
                    String name = method.getMethodName();
                    boolean excp = method.getIsException();
                    ArrayList<String> args = method.getArguments();
                    a.append(access + " ");
                    a.append(returnType + " ");
                    a.append(name);
                    a.append("(");
                    if (args != null && args.size() != 0) {
                        int count = 1;
                        for (int i = 0; i < args.size(); i++) {
                            String arrrg = args.get(i);
                            a.append(arrrg + " ");
                            a.append("arg" + count++);
                            if (i + 1 < args.size()) {
                                a.append(",");
                            }

                        }
                    }
                    a.append(") ");
                    if (excp) {
                        a.append("throws Exception");

                    }
                    a.append(";\n");
                }
            }

        }

    }

    public void makeJavaFile(File file, StringBuilder a) throws IOException {
        FileWriter javaFile = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(javaFile);
        bw.write(a.toString());
        bw.close();
        file.createNewFile();

    }

    public void processParentLines(String box) {
        Node node = dataManager.getSelectedBox();
        JClassBoxes child;
        if (box.equals("Insert Parent")) {
            Node selected = dataManager.getSelectedBox();
            JClassBoxes apples = (JClassBoxes) selected;
            Stage dialogStage = new Stage();
            TextField field = new TextField();
            Button ok = new Button("OK");
            Button cancel = new Button("Cancel");
            CheckBox classBox = new CheckBox("Class");
            CheckBox interfaceBox = new CheckBox("Interface");
            if (apples.getBoxType().equals(CLASS)) {

                dialogStage.initModality(Modality.WINDOW_MODAL);
                dialogStage.setScene(new Scene(VBoxBuilder.create().
                        children(new Text("Insert Parent"), field, classBox, interfaceBox, ok, cancel).
                        alignment(Pos.CENTER).padding(new Insets(20)).build()));

            } else {
                dialogStage.initModality(Modality.WINDOW_MODAL);
                dialogStage.setScene(new Scene(VBoxBuilder.create().
                        children(new Text("Insert Parent"), field, interfaceBox, ok, cancel).
                        alignment(Pos.CENTER).padding(new Insets(20)).build()));

            }
            dialogStage.show();
            classBox.setOnAction(e -> {
                interfaceBox.setSelected(false);
            });
            interfaceBox.setOnAction(e -> {
                classBox.setSelected(false);
            });
            ok.setOnAction(e -> {

                boolean sameName = false;
                ArrayList<String> a = dataManager.listofAllBoxes();
                for (int i = 0; i < a.size(); i++) {
                    String arg = a.get(i);
                    if (arg.equals(field.getText())) {
                        sameName = true;
                    }

                }
                if (field.getText().equals("")) {
                    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                    dialog.show("Nothing Warning", "Must not be blank or Same name of ");
                }
                if (!field.equals("") && !sameName && classBox.isSelected()) {

                    if (selected instanceof JClassBoxes) {
                        JClassClassBox jClassBox = (JClassClassBox) dataManager.initClassParentBox((int) ((JClassBoxes) selected).getX() + (int) ((JClassBoxes) selected).getWidth() + 100, (int) ((JClassBoxes) selected).getY() + (int) ((JClassBoxes) selected).getHeight() / 2, field.getText(), CLASS);
                        dataManager.addBox(jClassBox);
                        processParentLines(field.getText());

                    }

                } else if (!field.equals("") && interfaceBox.isSelected() && !sameName) {
                    if (selected instanceof JClassBoxes) {
                        if (selected instanceof JClassClassBox) {
                            JClassInterfaceBox iClassBox = (JClassInterfaceBox) dataManager.initClassParentBox((int) ((JClassBoxes) selected).getX() + (int) ((JClassBoxes) selected).getWidth() + 100, (int) ((JClassBoxes) selected).getY() + (int) ((JClassBoxes) selected).getHeight() / 2, field.getText(), INTERFACE);
                            dataManager.addBox(iClassBox);
                            processParentLines(field.getText());

                        } else {
                            JClassInterfaceBox people = (JClassInterfaceBox) selected;
                            if (people.getBoxParent().equals("")) {
                                JClassInterfaceBox iClassBox = (JClassInterfaceBox) dataManager.initClassParentBox((int) ((JClassBoxes) selected).getX() + (int) ((JClassBoxes) selected).getWidth() + 100, (int) ((JClassBoxes) selected).getY() + (int) ((JClassBoxes) selected).getHeight() / 2, field.getText(), INTERFACE);
                                dataManager.addBox(iClassBox);
                                processParentLines(field.getText());

                            }

                        }
                    }

                }

                dialogStage.close();
            });
            cancel.setOnAction(e -> {
                field.setText("");
                dialogStage.close();
            });

        } else if (node instanceof JClassBoxes) {
            child = (JClassBoxes) node;
            String childType = child.getBoxType();
            JClassBoxes par = dataManager.findParent(box);
            JClassClassBox parentss = null;
            JClassInterfaceBox parentsI = null;
            boolean isChildOfParent = false;
            if (par instanceof JClassClassBox) {
                parentss = (JClassClassBox) par;
                ArrayList<String> parentChildren = parentss.getBoxParent();
                for (int i = 0; i < parentChildren.size(); i++) {
                    String n = parentChildren.get(i);
                    if (n.equals(child.getName())) {
                        isChildOfParent = true;
                    }
                }
            }
            if (par instanceof JClassInterfaceBox) {
                parentsI = (JClassInterfaceBox) par;
                String parents = parentsI.getBoxParent();
                if (parents.equals(child.getName())) {
                    isChildOfParent = true;
                }
            }
            if (childType.equals(CLASS)) {
                if (node instanceof JClassClassBox) {
                    JClassClassBox n = (JClassClassBox) node;
                    ArrayList<String> p = n.getBoxParent();
                    boolean containsSameName = false;

                    if (par.getBoxType().equals(CLASS)) {
                        for (int i = 0; i < p.size(); i++) {
                            String next = p.get(i);
                            if (next.equals(par.getName())) {
                                dataManager.removeParents(child, par);
                                i--;
                                containsSameName = true;
                            }
                        }
                        if (!isChildOfParent && !containsSameName) {
                            ObservableList<Node> lines = dataManager.getBoxes();
                            for (int i = 0; i < lines.size(); i++) {
                                Node apple = lines.get(i);
                                if (apple instanceof JLine) {
                                    if (((JLine) apple).getType().equals(EXTENDS)) {
                                        JClassBoxes childdd = ((JLine) apple).getJChildren();
                                        JClassBoxes parente = ((JLine) apple).getJParent();
                                        if (p.contains(parente.getName())) {
                                            dataManager.removeParents(child, parente);
                                        }
                                    }

                                }
                            }

                            p.add(par.getName());
                            dataManager.makeLine(child, box, EXTENDS);
                        }
                    } else if (par.getBoxType().equals(INTERFACE)) {
                        for (int i = 0; i < p.size(); i++) {
                            String next = p.get(i);
                            if (next.equals(par.getName())) {
                                dataManager.removeParents(child, par);
                                i--;
                            }
                        }
                        if (!isChildOfParent) {
                            p.add(par.getName());
                            dataManager.makeLine(child, box, IMPLEMENTS);
                        }
                    }
                }
            } else if (childType.equals(INTERFACE)) {
                if (node instanceof JClassInterfaceBox) {
                    JClassInterfaceBox boxes = (JClassInterfaceBox) node;
                    if (boxes.getBoxParent().equals("") && par.getBoxType().equals(INTERFACE) && !isChildOfParent) {
                        boxes.setBoxParent(box);
                        dataManager.makeLine(child, box, EXTENDS);
                    } else if (boxes.getBoxParent().equals(box)) {
                        dataManager.removeParents(child, par);

                    }

                }
            }
            Workspace workspace = (Workspace) app.getWorkspaceComponent();

        }
    }

    public void processCAttributes(ClassAttributes a) {

        dataManager.processCAttributes(a);
    }

    public void processCRefresh(ClassAttributes a) {

        dataManager.processCRefresh(a);
    }

    public void processRemoveCAttributes(ClassAttributes selected) {

        dataManager.processCRemoval(selected);
    }

    public void processIAttributes(InterfaceAttributes get) {

        dataManager.processIAttributes(get);
    }

    public void processRemoveIAttributes(InterfaceAttributes selected) {

        dataManager.processIRemoval(selected);
    }

    public void processIRefresh(InterfaceAttributes interfaceAttributes) {

        dataManager.processIRefresh(interfaceAttributes);
    }

    public void processCMRefresh(ClassMethods a) {

        dataManager.processCMRefresh(a);
    }

    public void processCMTypeRefresh(ClassMethods a) {

        dataManager.processCMTypeRefresh(a);
    }

    public void processCMTypeRefreshArgs(ClassMethods a, ArrayList<String> b) {

        dataManager.processCMTypeRefreshArgs(a, b);
    }

    public void processCAttributeTypeRefresh(ClassAttributes a) {

        dataManager.processCATypeRefresh(a);
    }

    public void processIMTypeRefresh(InterfaceMethods a) {

        dataManager.processIMTypeRefresh(a);
    }

    public void processIMTypeRefreshArgs(InterfaceMethods a, ArrayList<String> b) {

        dataManager.processIMTypeRefreshArgs(a, b);
    }

    public void processIAttributeTypeRefresh(InterfaceAttributes a) {

        dataManager.processIATypeRefresh(a);
    }

    public void processCMethods(ClassMethods get) {

        dataManager.processCMethods(get);
    }

    public void processRemoveCMethods(ClassMethods selected) {

        dataManager.processCMRemoval(selected);
    }

    public void processIMethods(InterfaceMethods get) {

        dataManager.processIMethods(get);
    }

    public void processRemoveIMethods(InterfaceMethods selected) {

        dataManager.processIMRemoval(selected);
    }

    public void processIMRefresh(InterfaceMethods interfaceMethods) {

        dataManager.processIMRefresh(interfaceMethods);
    }

    public void checkAPI(String a, ArrayList<String> keys, ArrayList<String> values) {
        Package[] p = Package.getPackages();
        for (int i = 0; i < p.length; i++) {
            Package lol = p[i];
            String q = lol.getName() + "." + a;
            try {
                Class please = Class.forName(q);
                keys.add(a);
                values.add(lol.getName());
            } catch (ClassNotFoundException ex) {

            }
        }
    }

     public void checkourStuff(String a, ArrayList<String> keys, ArrayList<String> values) {
        ObservableList<Node> lastpart = dataManager.getBoxes();
        for (int i = 0; i < lastpart.size(); i++) {
            Node n = lastpart.get(i);
            if (n instanceof JClassBoxes) {
                String name = ((JClassBoxes) n).getName();
                if (name.equals(a)) {
                    String packages = ((JClassBoxes) n).getPackage();
                    if (!packages.equals("")) {
                        keys.add(a);
                        values.add(packages);
                    }
                }

            }

        }
    }

    public boolean primitivetype(String a, JClassBoxes n) {
        Package[] p = Package.getPackages();
        for (int i = 0; i < p.length; i++) {
            Package lol = p[i];
            String q = lol.getName() + "." + a;
            if (a.equals("int")) {
                q = lol.getName() + "." + "Integer";
            } else if (a.equals("double")) {
                q = lol.getName() + "." + "Double";
            } else if (a.equals("float")) {
                q = lol.getName() + "." + "Float";
            } else if (a.equals("boolean")) {
                q = lol.getName() + "." + "Boolean";
            } else if (a.equals("byte")) {
                q = lol.getName() + "." + "Byte";
            } else if (a.equals("character")) {
                q = lol.getName() + "." + "Character";
            } else if (a.equals("long")) {
                q = lol.getName() + "." + "Long";
            } else if (a.equals("object")) {
                q = lol.getName() + "." + "Object";
            } else if (a.equals("short")) {
                q = lol.getName() + "." + "Short";
            } else if (a.equals("math")) {
                q = lol.getName() + "." + "Math";
            }

            try {
                Class please = Class.forName(q);
                if (lol.getName().equals("java.lang") || lol.getName().contains("java") && (n.getPackage().equals("") || n.getPackage().contains("java"))) {
                    return true;
                }
            } catch (ClassNotFoundException ex) {

            }
        }
        return false;

    }

}
