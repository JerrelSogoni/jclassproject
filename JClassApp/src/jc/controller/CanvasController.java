package jc.controller;

import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import jc.data.DataManager;
import jc.data.JClassMakerState;
import static jc.data.JClassMakerState.BOX_DONE;
import static jc.data.JClassMakerState.DRAGGING_NOTHING;
import static jc.data.JClassMakerState.DRAGGING_BOX;
import static jc.data.JClassMakerState.SELECTING_BOX;
import static jc.data.JClassMakerState.SIZING_BOX;
import jc.gui.Workspace;
import saf.AppTemplate;
import jc.data.JClassBoxes;
import jc.data.JLine;
import jc.data.JLine.Anchor;

/**
 * This class responds to interactions with the rendering surface.
 * 
 * @author McKillaGorilla
 * @version 1.0
 */
public class CanvasController {
    AppTemplate app;
    
    public CanvasController(AppTemplate initApp) {
	app = initApp;
    }
    
    public void processCanvasMouseExited(int x, int y) {
	DataManager dataManager = (DataManager)app.getDataComponent();
	if (dataManager.isInState(JClassMakerState.DRAGGING_BOX)) {
            
	    
	}
	else if (dataManager.isInState(JClassMakerState.SIZING_BOX)) {
	    
	}
    }
    
    public void processCanvasMousePress(int x, int y, Node node) {
	DataManager dataManager = (DataManager)app.getDataComponent();
	if (dataManager.isInState(SELECTING_BOX)) {
	    // SELECT THE TOP SHAPE
	    Node box = dataManager.selectTopBox(x, y, node);
	    Scene scene = app.getGUI().getPrimaryScene();

	    // AND START DRAGGING IT
            dataManager.handleChange();
	    if (box != null) {
		scene.setCursor(Cursor.MOVE);
		dataManager.setState(JClassMakerState.DRAGGING_BOX);
		app.getGUI().updateToolbarControls(false);
	    }
	    else {
		scene.setCursor(Cursor.DEFAULT);
		dataManager.setState(DRAGGING_NOTHING);
		app.getWorkspaceComponent().reloadWorkspace();
	    }
	}
	else if (dataManager.isInState(JClassMakerState.STARTING_CLASS)) {
            dataManager.handleChange();
	    dataManager.startNewClassBox(x, y);
	}
	else if (dataManager.isInState(JClassMakerState.STARTING_INTERFACE)) {
            dataManager.handleChange();
	    dataManager.startNewInterfaceBox(x, y);
	}
	Workspace workspace = (Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace();
    }

    
    public void processCanvasMouseDragged(int x, int y, Node anchor) {
	DataManager dataManager = (DataManager)app.getDataComponent();
	if (dataManager.isInState(SIZING_BOX)) {


	}
	else if (dataManager.isInState(DRAGGING_BOX)) {
            Node node = dataManager.getSelectedBox();
            if(node instanceof JClassBoxes){
                JClassBoxes selectedJClassBox = (JClassBoxes)dataManager.getSelectedBox();
                selectedJClassBox.drag(x, y);
                ObservableList<Node> lines = dataManager.getBoxes();
                if(lines != null){
                    for(Node line: lines){
                        if(line instanceof JLine){
                            if(((JLine) line).getJParent().equals(node)){
                                Anchor end = ((JLine) line).getJEnd();
                                Anchor mid = ((JLine) line).getJMiddle();  
                                end.dragWithBox(x, y, end);
                                
                        
                            }
                            else if(((JLine) line).getJChildren().equals(node)){
                                 Anchor start = ((JLine) line).getJStart();
                                 Anchor mid = ((JLine) line).getJMiddle();
                                 start.dragWithBox(x, y, start);
                            
                            }
                        }
                    }
                }
            }
            else if(node instanceof Anchor){
                Anchor a = (Anchor)node;
                if(a.isVisible()){
                    a.dragged = true;
                    a.drag(x, y, a);
                }
                
            }
	    app.getGUI().updateToolbarControls(false);
	}
    }
    
    public void processCanvasMouseRelease(int x, int y) {
	DataManager dataManager = (DataManager)app.getDataComponent();
	if (dataManager.isInState(BOX_DONE)) {
	    dataManager.selectCreatedBox();
	    app.getGUI().updateToolbarControls(false);
	}
	else if (dataManager.isInState(JClassMakerState.DRAGGING_BOX)) {
	    dataManager.setState(SELECTING_BOX);
	    Scene scene = app.getGUI().getPrimaryScene();
	    scene.setCursor(Cursor.DEFAULT);
	    app.getGUI().updateToolbarControls(false);
            dataManager.snapPlease(dataManager.getSnap().isSelected());
            Node node = dataManager.getSelectedBox();
            if(node instanceof Anchor){
                Anchor a = (Anchor)node;
                a.dragged = false;
                a.drag(x, y, a);
                
            }
            
            
	}
	else if (dataManager.isInState(JClassMakerState.DRAGGING_NOTHING)) {
	    dataManager.setState(SELECTING_BOX);
	}
    }

    public void processSplit() {
        DataManager dataManager = (DataManager)app.getDataComponent();
           dataManager.handleChange();
        Node selected = dataManager.getSelectedBox();
        if(selected instanceof JLine){
            JLine selectedLine = (JLine)selected;
            selectedLine.setSplit(true);
            selectedLine.updateLine();
        }
        
 
    }

    public void processMerge() {
        DataManager dataManager = (DataManager)app.getDataComponent();
            dataManager.handleChange();
        Node selected = dataManager.getSelectedBox();
        if(selected instanceof JLine){
            JLine selectedLine = (JLine)selected;
            selectedLine.setSplit(false);
            selectedLine.updateLine();
        }  
        
    }
}
