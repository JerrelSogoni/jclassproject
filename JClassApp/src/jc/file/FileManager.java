package jc.file;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.VBox;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import jc.data.ClassAttributes;
import jc.data.ClassMethods;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;
import jc.data.DataManager;
import static jc.data.DataManager.AGGREGATE;
import static jc.data.DataManager.EXTENDS;
import static jc.data.DataManager.IMPLEMENTS;
import static jc.data.DataManager.USES;
import jc.data.InterfaceAttributes;
import jc.data.InterfaceMethods;
import jc.data.JClassClassBox;
import jc.data.JClassInterfaceBox;
import static jc.data.JClassBoxes.CLASS;
import jc.data.JClassBoxes;
import jc.data.JLine;

/**
 * This class serves as the file management component for this application,
 * providing all I/O services.
 *
 * @author Richard McKenna
 * @version 1.0
 */
public class FileManager implements AppFileComponent {

    // FOR JSON LOADING
    static final String JSON_GRID = "grid";
    static final String JSON_BOX = "box";
    static final String JSON_TYPE = "type";
    static final String JSON_SNAP = "snap";
    static final String JSON_CHILDREN = "children";
    static final String JSON_PACKAGE = "package";
    static final String JSON_PARENTS = "parent";
    static final String JSON_LINE = "line";
    static final String JSON_UML = "UML_BOXES";
    static final String JSON_NAME = "name";
    static final String JSON_ACCESS = "access";
    static final String JSON_ARG = "arg";
    static final String JSON_STATIC = "static";
    static final String JSON_ABSTRACT = "abstract";
    static final String JSON_CLASSBOX = "classbox";
    static final String JSON_INTERFACEBOX = "interfacebox";
    static final String JSON_ATTRIBUTES = "attributes";
    static final String JSON_METHODS = "methods";
    static final String JSON_X = "x";
    static final String JSON_Y = "y";
    static final String JSON_WIDTH = "width";
    static final String JSON_HEIGHT = "height";
    static final String JSON_WORK_AREA = "Canvas";
    static final String JSON_CANVAS_INFO = "Canvas_Info";
    static final String JSON_LINES = "workspace_lines";
    static final String JSON_RETURN_TYPE = "return";
    static final String JSON_START_X = "startx";
    static final String JSON_START_Y = "starty";
    static final String JSON_END_X = "endx";
    static final String JSON_END_Y = "endy";
    static final String JSON_FINAL = "final";
    static final String JSON_EXCEPTION = "exception";
    static final String JSON_ANCHOR_START = "AStart";
    static final String JSON_ANCHOR_MID = "AMid";
    static final String JSON_ANCHOR_END = "AEnd";
    static final String JSON_AS_C_X = "ASCenterx";
    static final String JSON_AS_C_Y = "ASCentery";
    static final String JSON_AM_C_X = "AMCenterx";
    static final String JSON_AM_C_Y = "AMCentery";
    static final String JSON_AE_C_X = "AECenterx";
    static final String JSON_AE_C_Y = "AECentery";
    static final String JSON_SPLIT = "split";
    static final String JSON_ZOOM = "zoom";

    /**
     * This method is for saving user work, which in the case of this
     * application means the data that constitutes the page DOM.
     *
     * @param data The data management component for this application.
     *
     * @param filePath Path (including file name/extension) to where to save the
     * data to.
     *
     * @throws IOException Thrown should there be an error writing out data to
     * the file.
     */
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        // GET THE DATA
        filePath = filePath + "json";
        DataManager dataManager = (DataManager) data;
        //Deselect if there is a box selected

        if (dataManager.getSelectedBox() != null) {
            dataManager.unhighlightBox(dataManager.getSelectedBox());
        }
        //get snap and grid

        CheckBox snap = dataManager.getSnap();
        CheckBox grid = dataManager.getGrid();
        JsonObject canvasCheckBox = makeJsonCheckBoxObject(grid, snap, dataManager);

        // NOW BUILD THE JSON OBJCTS TO SAVE
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        ObservableList<Node> boxes = dataManager.getBoxes();
        for (Node node : boxes) {
            if (node instanceof JClassBoxes) {
                JClassBoxes box = (JClassBoxes) node;
                String type = box.getBoxType();
                String name = box.getName();
                String packageName = box.getPackage();
                double x = box.getX();
                double y = box.getY();
                double width = ((VBox) box).getPrefWidth();
                double height = ((VBox) box).getPrefHeight();
                JsonArray fillAttributeArea = makeJsonAttributeArea(node);
                JsonArray fillMethodArea = makeJsonMethodArea(node);
                JsonArray fillParentArea = makeJsonParentArea(node);

                JsonObject generalBox = Json.createObjectBuilder()
                        .add(JSON_BOX, type)
                        .add(JSON_X, x)
                        .add(JSON_Y, y)
                        .add(JSON_WIDTH, width)
                        .add(JSON_HEIGHT, height)
                        .add(JSON_NAME, name)
                        .add(JSON_PACKAGE, packageName)
                        .add(JSON_PARENTS, fillParentArea)
                        .add(JSON_ATTRIBUTES, fillAttributeArea)
                        .add(JSON_METHODS, fillMethodArea)
                        .build();

                arrayBuilder.add(generalBox);
            }
        }
        JsonArray boxesArray = arrayBuilder.build();
        JsonArrayBuilder arrayBuilderLines = Json.createArrayBuilder();
        ObservableList<Node> lines = dataManager.getBoxes();
        int globalInd = 0;
        for (int i = 0; i < lines.size(); i++) {
            if (lines.get(i) instanceof JLine) {
                String ith = String.valueOf(++globalInd);
                JLine line = (JLine) lines.get(i);
                JsonObject workspaceLine = makeJsonLineArray(line);
                JsonObject lineData = Json.createObjectBuilder()
                        .add(JSON_LINE + ith, workspaceLine)
                        .build();
                arrayBuilderLines.add(lineData);
            }

        }
        JsonArray lineArray = arrayBuilderLines.build();

        // THEN PUT IT ALL TOGETHER IN A JsonObject
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_UML, boxesArray)
                .add(JSON_LINES, lineArray)
                .add(JSON_CANVAS_INFO, canvasCheckBox)
                .build();

        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();
    }

    /**
     * This method loads data from a JSON formatted file into the data
     * management component and then forces the updating of the workspace such
     * that the user may edit the data.
     *
     * @param data Data management component where we'll load the file into.
     *
     * @param filePath Path (including file name/extension) to where to load the
     * data from.
     *
     * @throws IOException Thrown should there be an error reading in data from
     * the file.
     */
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        DataManager dataManager = (DataManager) data;
        dataManager.cleanReset();

        JsonObject json = loadJSONFile(filePath);
      

        JsonArray jsonBoxes = json.getJsonArray(JSON_UML);
        for (int i = 0; i < jsonBoxes.size(); i++) {
            JsonObject jsonBox = jsonBoxes.getJsonObject(i);
            Node box = loadBox(jsonBox, dataManager);
            dataManager.getBoxes().add(box);

        }
        JsonArray lines = json.getJsonArray(JSON_LINES);
        for (int i = 0; i < lines.size(); i++) {
            String ith = String.valueOf(i + 1);
            JsonObject line = lines.getJsonObject(i);
            JsonObject lineInfo = line.getJsonObject(JSON_LINE + ith);
            double startX = getDataAsDouble(lineInfo, JSON_START_X);
            double startY = getDataAsDouble(lineInfo, JSON_START_Y);
            double endX = getDataAsDouble(lineInfo, JSON_END_X);
            double endY = getDataAsDouble(lineInfo, JSON_END_Y);
            double ASstartx = getDataAsDouble(lineInfo, JSON_AS_C_X);
            double ASstarty = getDataAsDouble(lineInfo, JSON_AS_C_Y);
            double AMstartx = getDataAsDouble(lineInfo, JSON_AM_C_X);
            double AMstarty = getDataAsDouble(lineInfo, JSON_AM_C_Y);
            double AEstartx = getDataAsDouble(lineInfo, JSON_AE_C_X);
            double AEstarty = getDataAsDouble(lineInfo, JSON_AE_C_Y);
            boolean split = loadBooleanInformation(lineInfo, JSON_SPLIT);
            String parentName = lineInfo.getString(JSON_PARENTS);
            String children = lineInfo.getString(JSON_CHILDREN);
            String type = lineInfo.getString(JSON_TYPE);

            JClassBoxes parent = dataManager.findParent(parentName);
            JClassBoxes child = dataManager.findParent(children);
            JLine almostDone = new JLine(parent, child, type);
            almostDone.getJStart().setCenterX(ASstartx);
            almostDone.getJStart().setCenterY(ASstarty);
            almostDone.getJMiddle().setCenterX(AMstartx);
            almostDone.getJMiddle().setCenterY(AMstarty);
            almostDone.setEndX(AMstartx);
            almostDone.setEndY(AMstarty);
            almostDone.getMidToParentLine().setStartX(AMstartx);
            almostDone.getMidToParentLine().setStartY(AMstarty);
            almostDone.getJEnd().setCenterX(AEstartx);
            almostDone.getJEnd().setCenterY(AEstarty);
            almostDone.setSplit(split);
            dataManager.addBox(almostDone);
            dataManager.addBox(almostDone.getMidToParentLine());
            if (type.equals(AGGREGATE)) {
                dataManager.addBox(almostDone.getAggregateHead());
            } else if (type.equals(EXTENDS)) {
                dataManager.addBox(almostDone.getExtendsHead());
            } else if (type.equals(IMPLEMENTS)) {
                dataManager.addBox(almostDone.getImplementsHead());
            } else if (type.equals(USES)) {
                dataManager.addBox(almostDone.getUsesHead());
            }
            dataManager.addBox(almostDone.getJStart());
            dataManager.addBox(almostDone.getJMiddle());
            dataManager.addBox(almostDone.getJEnd());

        }
        loadToolbarCheckBoxes(json, dataManager);

    }

    public Node loadBox(JsonObject j, DataManager dataManager) {
        String type = j.getString(JSON_BOX);
        JClassBoxes box;
        if (type.equals(CLASS)) {
            box = new JClassClassBox(dataManager);
        } else {
            box = new JClassInterfaceBox(dataManager);
        }
        //load box properties
        double x = getDataAsDouble(j, JSON_X);
        double y = getDataAsDouble(j, JSON_Y);
        double width = getDataAsDouble(j, JSON_WIDTH);
        double height = getDataAsDouble(j, JSON_HEIGHT);
        JClassBoxes boxLayout = (JClassBoxes) box;
        if (height <= 0 && width <= 0) {
            ((VBox) boxLayout).setLayoutX(x);
            ((VBox) boxLayout).setLayoutY(y);
        } else {
            boxLayout.setLocationAndSize(x, y, width, height);
        }
        loadClassRegion(box, j);
        loadPackage(box, j);
        loadParents(box, j);
        loadAttributes(box, j);
        loadMethods(box, j);
        Node ourNode = (Node) box;
        return ourNode;

    }

    public void loadToolbarCheckBoxes(JsonObject j, DataManager a) {
        JsonObject jsonToolbar = j.getJsonObject(JSON_CANVAS_INFO);
        boolean grid = loadBooleanInformation(jsonToolbar, JSON_GRID);
        boolean snap = loadBooleanInformation(jsonToolbar, JSON_SNAP);
        double zoom = getDataAsDouble(jsonToolbar, JSON_ZOOM);
        if (a.getSnap() != null || a.getGrid() != null) {
            a.getGrid().setSelected(grid);
            if(grid){
                 a.processGridLines(grid);
            }
            a.getSnap().setSelected(snap);
            a.snapPlease(snap);
            a.setZoom(zoom);
        }
    }

    public String loadBoxes(JsonObject j, String info) {
        JsonObject stringResult = j.getJsonObject(info);
        String result = stringResult.toString();
        return result;
    }

    public boolean loadBooleanInformation(JsonObject j, String info) {
        boolean result = j.getBoolean(info);
        return result;
    }

    public double getDataAsDouble(JsonObject json, String dataName) {
        JsonValue value = json.get(dataName);
        JsonNumber number = (JsonNumber) value;
        return number.bigDecimalValue().doubleValue();
    }

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }

    /**
     * This method exports the contents of the data manager to a Web page
     * including the html page, needed directories, and the CSS file.
     *
     * @param data The data management component.
     *
     * @param filePath Path (including file name/extension) to where to export
     * the page to.
     *
     * @throws IOException Thrown should there be an error writing out data to
     * the file.
     */
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {

    }

    /**
     * This method is provided to satisfy the compiler, but it is not used by
     * this application.
     */
    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        // NOTE THAT THE Web Page Maker APPLICATION MAKES
        // NO USE OF THIS METHOD SINCE IT NEVER IMPORTS
        // EXPORTED WEB PAGES
    }

    public JsonObject makeJsonCheckBoxObject(CheckBox a, CheckBox b, DataManager dataManager) {
        JsonObject checkBoxToolbar = Json.createObjectBuilder()
                .add(JSON_GRID, a.isSelected())
                .add(JSON_SNAP, b.isSelected())
                .add(JSON_ZOOM, dataManager.getZoom())
                .build();
        return checkBoxToolbar;

    }

    private JsonArray makeJsonAttributeArea(Node node) {
        if (node instanceof JClassClassBox) {
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            JClassClassBox classBox = (JClassClassBox) node;
            if (classBox.getHasVariables()) {
                ObservableList<ClassAttributes> attributes = classBox.getClassAttributes();
                for (int i = 0; i < attributes.size(); i++) {
                    String ith = String.valueOf(i + 1);
                    ClassAttributes list = attributes.get(i);
                    JsonObject attributeInfo = Json.createObjectBuilder()
                            .add(JSON_NAME, list.getAttributeName())
                            .add(JSON_TYPE, list.getAttributeType())
                            .add(JSON_STATIC, list.getIsStatic())
                            .add(JSON_FINAL, list.getIsFinal())
                            .add(JSON_ACCESS, list.getAttributeAccess())
                            .build();
                    JsonObject dataManagerJSO = Json.createObjectBuilder()
                            .add(JSON_ATTRIBUTES + ith, attributeInfo)
                            .build();
                    arrayBuilder.add(dataManagerJSO);

                }
            }
            JsonArray classArray = arrayBuilder.build();
            return classArray;
        } else {
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            JClassInterfaceBox InterfaceBox = (JClassInterfaceBox) node;
            if (InterfaceBox.getHasVariables()) {
                ObservableList<InterfaceAttributes> attributes = InterfaceBox.getInterfaceAttributes();
                for (int i = 0; i < attributes.size(); i++) {
                    String ith = String.valueOf(i + 1);
                    InterfaceAttributes list = attributes.get(i);
                    JsonObject attributeInfo = Json.createObjectBuilder()
                            .add(JSON_NAME, list.getAttributeName())
                            .add(JSON_TYPE, list.getAttributeType())
                            .add(JSON_ACCESS, list.getAttributeAccess())
                            .build();
                    JsonObject dataManagerJSO = Json.createObjectBuilder()
                            .add(JSON_ATTRIBUTES + ith, attributeInfo)
                            .build();
                    arrayBuilder.add(dataManagerJSO);

                }
            }
            JsonArray classArray = arrayBuilder.build();
            return classArray;
        }
    }

    private JsonArray makeJsonMethodArea(Node node) {
        if (node instanceof JClassClassBox) {
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            JClassClassBox classBox = (JClassClassBox) node;
            if (classBox.getHasMethods()) {
                ObservableList<ClassMethods> methods = classBox.getClassMethods();
                for (int i = 0; i < methods.size(); i++) {
                    JsonArrayBuilder innerMethods = Json.createArrayBuilder();
                    String ith = String.valueOf(i + 1);
                    ClassMethods list = methods.get(i);
                    JsonObject methodInfo = Json.createObjectBuilder()
                            .add(JSON_NAME, list.getMethodName())
                            .add(JSON_RETURN_TYPE, list.getMethodReturn())
                            .add(JSON_STATIC, list.getIsStatic())
                            .add(JSON_ABSTRACT, list.getIsAbstract())
                            .add(JSON_ACCESS, list.getMethodAccess())
                            .add(JSON_EXCEPTION, list.getIsException())
                            .build();
                    innerMethods.add(methodInfo);
                    ArrayList<String> args = list.getArguments();
                    for (int j = 0; j < args.size(); j++) {
                        String jthNumber = String.valueOf(j + 1);
                        JsonObject arguments = Json.createObjectBuilder()
                                .add(JSON_ARG + jthNumber, args.get(j))
                                .build();
                        innerMethods.add(arguments);
                    }
                    JsonArray innerMethod = innerMethods.build();

                    JsonObject dataManagerJSO = Json.createObjectBuilder()
                            .add(JSON_METHODS + ith, innerMethod)
                            .build();
                    arrayBuilder.add(dataManagerJSO);

                }
            }
            JsonArray methodArray = arrayBuilder.build();
            return methodArray;
        } else if(node instanceof JClassInterfaceBox){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            JClassInterfaceBox interfaceBox = (JClassInterfaceBox) node;
            if (interfaceBox.getHasMethods()) {
                ObservableList<InterfaceMethods> methods = interfaceBox.getInterfaceMethods();
                for (int i = 0; i < methods.size(); i++) {
                    JsonArrayBuilder innerMethods = Json.createArrayBuilder();
                    String ith = String.valueOf(i + 1);
                    InterfaceMethods list = methods.get(i);
                    JsonObject methodInfo = Json.createObjectBuilder()
                            .add(JSON_NAME, list.getMethodName())
                            .add(JSON_RETURN_TYPE, list.getMethodReturn())
                            .add(JSON_EXCEPTION, list.getIsException())
                            .add(JSON_ACCESS, list.getMethodAccess())
                            .build();
                    innerMethods.add(methodInfo);
                    ArrayList<String> args = list.getArguments();
                    for (int j = 0; j < args.size(); j++) {
                        String jthNumber = String.valueOf(j + 1);
                        JsonObject arguments = Json.createObjectBuilder()
                                .add(JSON_ARG + jthNumber, args.get(j))
                                .build();
                        innerMethods.add(arguments);
                    }
                    JsonArray innerMethod = innerMethods.build();
                    JsonObject dataManagerJSO = Json.createObjectBuilder()
                            .add(JSON_METHODS + ith, innerMethod)
                            .build();
                    arrayBuilder.add(dataManagerJSO);
                }
            }
            JsonArray methodArray = arrayBuilder.build();
            return methodArray;
        }
        return null;
        
    }

    private JsonArray makeJsonParentArea(Node node) {
        if (node instanceof JClassClassBox) {
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            JClassClassBox classBox = (JClassClassBox) node;
            ArrayList<String> parents = classBox.getBoxParent();
            for (int i = 0; i < parents.size(); i++) {
                String ithNumber = String.valueOf(i + 1);
                JsonObject classBoxParent = Json.createObjectBuilder()
                        .add(JSON_PARENTS + ithNumber, parents.get(i))
                        .build();
                arrayBuilder.add(classBoxParent);
            }
            JsonArray parentClass = arrayBuilder.build();
            return parentClass;

        } else {
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            JClassInterfaceBox interfaceBox = (JClassInterfaceBox) node;
            String parents = interfaceBox.getBoxParent();
            if (!parents.equals("")) {
                JsonObject classBoxParent = Json.createObjectBuilder()
                        .add(JSON_PARENTS, parents)
                        .build();
                arrayBuilder.add(classBoxParent);
            }
            JsonArray parentInterface = arrayBuilder.build();
            return parentInterface;
        }

    }

    public JsonObject makeJsonLineArray(JLine node) {
        JLine line = (JLine) node;
        JsonObject lineInfo = Json.createObjectBuilder()
                .add(JSON_TYPE, line.getType())
                .add(JSON_START_X, line.getStartX())
                .add(JSON_START_Y, line.getStartY())
                .add(JSON_END_X, line.getEndX())
                .add(JSON_END_Y, line.getEndY())
                .add(JSON_PARENTS, line.getJParent().getName())
                .add(JSON_CHILDREN, line.getJChildren().getName())
                .add(JSON_AS_C_X, line.getJStart().getCenterX())
                .add(JSON_AS_C_Y, line.getJStart().getCenterY())
                .add(JSON_AM_C_X, line.getJMiddle().getCenterX())
                .add(JSON_AM_C_Y, line.getJMiddle().getCenterY())
                .add(JSON_AE_C_X, line.getJEnd().getCenterX())
                .add(JSON_AE_C_Y, line.getJEnd().getCenterY())
                .add(JSON_SPLIT, line.getSplit())
                .build();

        return lineInfo;
    }

    private void loadClassRegion(JClassBoxes box, JsonObject j) {
        if (box instanceof JClassClassBox) {
            String name = j.getString(JSON_NAME);
            ((JClassClassBox) box).getClassName().setText(name);
        } else {
            String name = j.getString(JSON_NAME);
            ((JClassInterfaceBox) box).getClassName().setText(name);
        }
    }

    private void loadPackage(JClassBoxes box, JsonObject j) {
        String name = j.getString(JSON_PACKAGE);
        if (box instanceof JClassClassBox) {
            ((JClassClassBox) box).setPackageName(name);
        } else {
            ((JClassInterfaceBox) box).setPackageName(name);
        }
    }

    private void loadParents(JClassBoxes box, JsonObject j) {
        if (box instanceof JClassClassBox) {
            JsonArray jsonParent = j.getJsonArray(JSON_PARENTS);
            for (int i = 0; i < jsonParent.size(); i++) {
                String ith = String.valueOf(i + 1);
                JsonObject innerLoop = jsonParent.getJsonObject(i);
                String parent = innerLoop.getString(JSON_PARENTS + ith);

                ((JClassClassBox) box).addParent(parent);

            }

        } else {
            JClassInterfaceBox boxInt = (JClassInterfaceBox) box;
            JsonArray jsonParent = j.getJsonArray(JSON_PARENTS);
            for (int i = 0; i < jsonParent.size(); i++) {
                JsonObject parentInfo = jsonParent.getJsonObject(i);
                String parent = parentInfo.getString(JSON_PARENTS);
                boxInt.setBoxParent(parent);

            }
        }
    }

    public void loadAttributes(JClassBoxes box, JsonObject j) {

        if (box instanceof JClassClassBox) {
            JsonArray jsonAttribute = j.getJsonArray(JSON_ATTRIBUTES);
            for (int i = 0; i < jsonAttribute.size(); i++) {
                String ith = String.valueOf(i + 1);
                JsonObject attributeInfo = jsonAttribute.getJsonObject(i);
                JsonObject innerAttribute = attributeInfo.getJsonObject(JSON_ATTRIBUTES + ith);
                if (!innerAttribute.isEmpty()) {
                    ClassAttributes classAtt = new ClassAttributes();
                    String name = innerAttribute.getString(JSON_NAME);
                    String type = innerAttribute.getString(JSON_TYPE);
                    boolean isStatic = loadBooleanInformation(innerAttribute, JSON_STATIC);
                    boolean isFinal = loadBooleanInformation(innerAttribute, JSON_FINAL);
                    String access = innerAttribute.getString(JSON_ACCESS);
                    classAtt.setAttributeName(name);
                    classAtt.setAttributeType(type);
                    classAtt.setAttributeAccess(access);
                    classAtt.setIsStatic(isStatic);
                    classAtt.setIsFinal(isFinal);
                    ((JClassClassBox) box).addAttribute(classAtt);
                }
            }

        } else {
            JClassInterfaceBox intbox = (JClassInterfaceBox) box;
            JsonArray jsonAttribute = j.getJsonArray(JSON_ATTRIBUTES);
            for (int i = 0; i < jsonAttribute.size(); i++) {
                String ith = String.valueOf(i + 1);
                JsonObject attributeInfo = jsonAttribute.getJsonObject(i);
                JsonObject innerAttribute = attributeInfo.getJsonObject(JSON_ATTRIBUTES + ith);
                if (!innerAttribute.isEmpty()) {
                    InterfaceAttributes interfaceAtt = new InterfaceAttributes();
                    String name = innerAttribute.getString(JSON_NAME);
                    String type = innerAttribute.getString(JSON_TYPE);
                    String access = innerAttribute.getString(JSON_ACCESS);
                    interfaceAtt.setAttributeName(name);
                    interfaceAtt.setAttributeType(type);
                    interfaceAtt.setAttributeAccess(access);
                    intbox.addAttribute(interfaceAtt);

                }
            }

        }

    }

    private void loadMethods(JClassBoxes box, JsonObject j) {
        if (box instanceof JClassClassBox) {
            JsonArray jsonMethod = j.getJsonArray(JSON_METHODS);
            for (int i = 0; i < jsonMethod.size(); i++) {
                String ith = String.valueOf(i + 1);
                JsonObject innerMethods = jsonMethod.getJsonObject(i);
                JsonArray accessLocalMethod = innerMethods.getJsonArray(JSON_METHODS + ith);
                if (!accessLocalMethod.isEmpty()) {
                    JsonObject methodsInfo = accessLocalMethod.getJsonObject(0);

                    ClassMethods methods = new ClassMethods();
                    ArrayList<String> argList = new ArrayList<>();
                    String name = methodsInfo.getString(JSON_NAME);
                    String returnType = methodsInfo.getString(JSON_RETURN_TYPE);
                    String access = methodsInfo.getString(JSON_ACCESS);
                    boolean isStatic = loadBooleanInformation(methodsInfo, JSON_STATIC);
                    boolean isAbstract = loadBooleanInformation(methodsInfo, JSON_ABSTRACT);
                    if (isAbstract) {
                        ((JClassClassBox) box).putAbstractLabel();
                    }
                    boolean isException = loadBooleanInformation(methodsInfo, JSON_EXCEPTION);
                    if (accessLocalMethod.size() >= 2) {
                        int argNumber = 0;
                        for (int k = 1; k < accessLocalMethod.size(); k++) {
                            JsonObject arg = accessLocalMethod.getJsonObject(k);
                            String kth = String.valueOf(argNumber + 1);
                            String arguments = arg.getString(JSON_ARG + kth);
                            argList.add(arguments);
                            argNumber++;
                        }
                    }
                    methods.setMethodName(name);
                    methods.setMethodReturn(returnType);
                    methods.setMethodAccess(access);
                    methods.setIsAbstract(isAbstract);
                    methods.setIsException(isException);
                    methods.setIsStatic(isStatic);
                    methods.setArguments(argList);
                    ((JClassClassBox) box).addMethods(methods);
                }
            }

        } else {
            JClassInterfaceBox intBox = (JClassInterfaceBox) box;
            JsonArray jsonMethod = j.getJsonArray(JSON_METHODS);
            for (int i = 0; i < jsonMethod.size(); i++) {
                String ith = String.valueOf(i + 1);
                JsonObject innerMethods = jsonMethod.getJsonObject(i);
                JsonArray accessLocalMethod = innerMethods.getJsonArray(JSON_METHODS + ith);
                if (!accessLocalMethod.isEmpty()) {
                    JsonObject methodsInfo = accessLocalMethod.getJsonObject(0);
                    InterfaceMethods methods = new InterfaceMethods();
                    ArrayList<String> argList = new ArrayList<>();
                    String name = methodsInfo.getString(JSON_NAME);
                    String returnType = methodsInfo.getString(JSON_RETURN_TYPE);
                    String access = methodsInfo.getString(JSON_ACCESS);
                    boolean isException = loadBooleanInformation(methodsInfo, JSON_EXCEPTION);
                    if (accessLocalMethod.size() >= 2) {
                        int argNumber = 0;
                        for (int k = 1; k < accessLocalMethod.size(); k++) {
                            JsonObject arg = accessLocalMethod.getJsonObject(k);
                            String kth = String.valueOf(argNumber + 1);
                            String arguments = arg.getString(JSON_ARG + kth);
                            argList.add(arguments);
                            argNumber++;
                        }
                    }
                    methods.setMethodName(name);
                    methods.setMethodReturn(returnType);
                    methods.setMethodAccess(access);
                    methods.setIsException(isException);
                    methods.setArguments(argList);
                    intBox.addMethods(methods);

                }
            }

        }

    }

}
