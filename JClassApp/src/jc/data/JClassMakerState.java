package jc.data;

/**
 * This enum has the various possible states of the pose maker app.
 * 
 * @author McKillaGorilla
 * @version 1.0
 */
public enum JClassMakerState {
    SELECTING_BOX,
    DRAGGING_BOX,
    STARTING_CLASS,
    STARTING_INTERFACE,
    SIZING_BOX,
    DRAGGING_NOTHING,
    SIZING_NOTHING, 
    BOX_DONE
}
