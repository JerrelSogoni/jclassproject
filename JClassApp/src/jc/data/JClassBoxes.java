/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jc.data;

import javafx.beans.property.DoubleProperty;

/**
 *
 * @author Jerrel
 */
public interface JClassBoxes{
    public static final String STATIC = "$";
    public static final String PRIVATE = "-";
    public static final String PUBLIC = "+";
    public static final String PROTECTED = "#";
    public static final String NOTHING = "~";
    public static final String CLASS = "CLASS";
    public static final String INTERFACE = "INTERFACE";
    public JClassMakerState getStartingState();
    public void start(int x, int y);
    public void drag(int x, int y);
    public double getX();
    public double getY();
    public double getWidth();
    public double getHeight();
    public void setLocationAndSize(double initX, double initY,double width, double height);
    public String getBoxType();
    public void resize();
    public String getName();
    public String getPackage();
    public boolean isAbstract();
    
    

    
    
}
