/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jc.data;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import static jc.data.JClassMakerState.SIZING_BOX;
import saf.AppTemplate;
import saf.ui.AppGUI;

/**
 *
 * @author Jerrel
 */
public class JClassClassBox extends VBox implements JClassBoxes, JClassHistory {

    private static final String ABSTRACT = "{abstract}";
    private static final String DEFAULT_CLASS = "Class";
    private static final String J_BOXES = "j_boxes";
    private double x;
    private double y;
    private VBox classRegion;
    private VBox attributeRegion;
    private VBox methodRegion;
    private Label className;
    private String packageName;
    private ObservableList<Node> classArea;
    private ObservableList<Node> attributes;
    private ObservableList<Node> methods;
    private ObservableList<ClassAttributes> attributeData;
    private ObservableList<ClassMethods> methodData;
    private ArrayList<String> parent;
    private boolean isAbstract;
    private boolean hasVariables;
    private boolean hasMethods;
    private Label abstractLabel;
    private static int index = 0;
    public String sizingRegion = "";
    DataManager app;
    int tempx = 0;
    int tempy = 0;
    int tempprevwidth = 0;
    int tempprevheight = 0;

    public JClassClassBox(DataManager app) {
        this.app = app;
        this.setLayoutX(0.0);
        this.setLayoutY(0.0);
        x = 0.0;
        y = 0.0;
        index++;
        classRegion = new VBox();
        classArea = classRegion.getChildren();
        classRegion.setAlignment(Pos.TOP_CENTER);
        className = new Label(DEFAULT_CLASS + index);
        classRegion.getChildren().add(className);
        packageName = "";
        parent = new ArrayList<>();
        this.getChildren().add(classRegion);
        initStyle();
        initLonelyListener();

    }

    @Override
    public JClassMakerState getStartingState() {
        return JClassMakerState.STARTING_CLASS;
    }

    @Override
    public void start(int x, int y) {
        this.x = x;
        this.y = y;
        this.setLayoutX(x);
        this.setLayoutY(y);
    }

    @Override
    public void drag(int x, int y) {

        this.setLayoutX(x);
        this.setLayoutY(y);

    }

    public void addAttributeBox() {
        if (attributeRegion == null) {
            attributeData = FXCollections.observableArrayList();
            attributeRegion = new VBox();
            attributeRegion.getStyleClass().add(J_BOXES);
            attributes = attributeRegion.getChildren();
            this.getChildren().add(1, attributeRegion);
        }

    }

    public void removeAttributeBox() {
        if (attributeRegion != null) {
            this.getChildren().remove(attributeRegion);
            attributeRegion = null;
            hasVariables = false;
        }
    }

    public void addMethodBox() {
        if (methodRegion == null) {
            methodData = FXCollections.observableArrayList();
            methodRegion = new VBox();
            methodRegion.getStyleClass().add(J_BOXES);
            methods = methodRegion.getChildren();
            if (attributeRegion == null) {
                this.getChildren().add(1, methodRegion);
            } else {
                this.getChildren().add(2, methodRegion);
            }

        }

    }

    public void removeMethodBox() {
        if (methodRegion != null) {
            this.getChildren().remove(methodRegion);
            methodRegion = null;
            hasMethods = false;
        }
    }

    public void addAttribute(ClassAttributes attribute) {
        hasVariables = true;
        addAttributeBox();
        ClassAttributes add = attribute;
        StringBuilder buildAttribute = new StringBuilder();
        String attributeAccess = add.getAttributeAccess().toLowerCase();
        firstPart(buildAttribute, attributeAccess);
        if (add.getIsStatic()) {
            buildAttribute.append(STATIC);
        }
        if (add.getIsFinal()) {
            buildAttribute.append(add.getAttributeName().toUpperCase());
        } else {
            buildAttribute.append(add.getAttributeName());
        }
        buildAttribute.append(" : ")
                .append(add.getAttributeType());
        Label ourNewAttribute = new Label(buildAttribute.toString());
        attributes.add(ourNewAttribute);
        attributeData.add(add);

    }

    public void firstPart(StringBuilder a, String b) {

        switch (b) {
            case "public":
                a.append(PUBLIC);
                break;
            case "private":
                a.append(PRIVATE);
                break;
            case "protected":
                a.append(PROTECTED);
                break;
            case "":
                a.append(NOTHING);
                break;
            default:
                break;
        }
    }

    public void refreshAttributes(ClassAttributes a) {
        ClassAttributes add = a;
        StringBuilder buildAttribute = new StringBuilder();
        String attributeAccess = add.getAttributeAccess().toLowerCase();
        firstPart(buildAttribute, attributeAccess);
        if (add.getIsStatic()) {
            buildAttribute.append(STATIC);
        }
        if (add.getIsFinal()) {
            buildAttribute.append(add.getAttributeName().toUpperCase());
        } else {
            buildAttribute.append(add.getAttributeName());
        }
        buildAttribute.append(" : ")
                .append(add.getAttributeType());

        Label ourNewAttribute = new Label(buildAttribute.toString());
        int index = attributeData.indexOf(a);
        attributes.remove(index);
        attributes.add(index, ourNewAttribute);

    }

    public void refreshMethod(ClassMethods b) {
        ClassMethods add = b;
        StringBuilder buildMethod = new StringBuilder();
        String methodAccess = add.getMethodAccess();
        firstPart(buildMethod, methodAccess);
        if (add.getIsStatic()) {
            buildMethod.append(STATIC);
        }
        buildMethod.append(add.getMethodName())
                .append("(");
        int argSize = add.getArguments().size();
        ArrayList<String> methodArgs = add.getArguments();
        for (int i = 0; i < argSize; i++) {
            buildMethod.append("arg")
                    .append(i + 1)
                    .append(" : ")
                    .append(methodArgs.get(i));
            if (i + 1 < argSize) {
                buildMethod.append(" ,");
            }
        }
        String returnType = add.getMethodReturn();
        if (!returnType.equals("")) {
            buildMethod.append(")")
                    .append(" : ")
                    .append(returnType);
        }

        isAbstract = add.getIsAbstract();
        if (isAbstract) {
            putAbstractLabel();
            buildMethod.append(ABSTRACT);
        } else if (abstractLabel != null) {
            if (!trulyAbstract()) {
                deleteAbstract();
                isAbstract = false;

            }
        }
        Label addMethod = new Label(buildMethod.toString());
        int index = methodData.indexOf(b);
        methods.remove(index);
        methods.add(index, addMethod);

    }

    public void removeCAttribute(ClassAttributes a) {
        int index = attributeData.indexOf(a);
        attributeData.remove(a);
        attributes.remove(index);
        if (attributes.size() <= 0) {
            removeAttributeBox();
        }
    }

    public void removeCMethods(ClassMethods a) {
        int index = methodData.indexOf(a);
        methodData.remove(a);
        methods.remove(index);
        if (methods.size() <= 0) {
            removeMethodBox();
        }
    }

    public void addMethods(ClassMethods m) {
        hasMethods = true;
        addMethodBox();
        ClassMethods add = m;
        StringBuilder buildMethod = new StringBuilder();
        String methodAccess = add.getMethodAccess();
        firstPart(buildMethod, methodAccess);
        if (add.getIsStatic()) {
            buildMethod.append(STATIC);
        }
        buildMethod.append(add.getMethodName())
                .append("(");
        int argSize = add.getArguments().size();
        ArrayList<String> methodArgs = add.getArguments();
        for (int i = 0; i < argSize; i++) {
            buildMethod.append("arg")
                    .append(i + 1)
                    .append(" : ")
                    .append(methodArgs.get(i));
            if (i + 1 < argSize) {
                buildMethod.append(" ,");
            }
        }
        String returnType = add.getMethodReturn();
        if (!returnType.equals("")) {
            buildMethod.append(")")
                    .append(" : ")
                    .append(returnType);
        }

        isAbstract = add.getIsAbstract();
        if (isAbstract) {
            putAbstractLabel();
            buildMethod.append(ABSTRACT);
        } else if (abstractLabel != null) {
            if (!trulyAbstract()) {
                deleteAbstract();
                isAbstract = false;

            }

        }
        Label addMethod = new Label(buildMethod.toString());
        methods.add(addMethod);
        methodData.add(add);
    }

    @Override
    public double getX() {
        return getLayoutX();
    }

    @Override
    public double getY() {
        return getLayoutY();
    }

    @Override
    public String getBoxType() {
        return CLASS;
    }

    @Override
    public void resize() {
        System.out.println("jc.data.JClassClassBox.resize()");
    }

    @Override
    public void setLocationAndSize(double initX, double initY, double width, double height) {
        this.x = initX;
        this.y = initY;
        this.setLayoutX(initX);
        this.setLayoutY(initY);
        this.setPrefWidth(width);
        this.setPrefHeight(height);
    }

    public void setClassName(Label label) {
        className = label;
    }

    public Label getClassName() {
        return className;
    }

    @Override
    public String getName() {
        return className.getText();
    }

    @Override
    public String getPackage() {
        return packageName;
    }

    public void setPackageName(String name) {
        packageName = name;
    }

    public void initStyle() {
        classRegion.getStyleClass().add(J_BOXES);
        this.getStyleClass().add("BOXES");

    }

    public void putAbstractLabel() {
        if (abstractLabel == null ) {
            abstractLabel = new Label(ABSTRACT);
            isAbstract = true;
            classArea.add(abstractLabel);
        }
    }

    public void deleteAbstract() {
        classArea.remove(abstractLabel);
        abstractLabel = null;
    }

    public void setBoxParent(ArrayList<String> parent) {
        this.parent = parent;
    }

    public ArrayList<String> getBoxParent() {
        return parent;
    }

    public ObservableList<ClassAttributes> getClassAttributes() {
        return attributeData;
    }

    public ObservableList<ClassMethods> getClassMethods() {
        return methodData;
    }
    public void setClassAttributes(ObservableList<ClassAttributes> a){
        this.attributeData = a;
        
    }
   public void setClassClassMethods(ObservableList<ClassMethods> a){
        this.methodData = a;
        
    }
    

    public void setHasMethods(boolean a) {
        hasMethods = a;
    }

    public boolean getHasMethods() {
        return hasMethods;

    }

    public void setHasVariables(boolean a) {
        hasVariables = a;
    }

    public boolean getHasVariables() {
        return hasVariables;
    }

    public void addParent(String p) {
        parent.add(p);
    }

    @Override
    public boolean isAbstract() {
        return isAbstract;
    }

    public boolean trulyAbstract() {
        int counter = 0;
        for (int i = 0; i < methodData.size(); i++) {
            if (!methodData.get(i).getIsAbstract()) {
                counter++;
            }
        }
        if (counter == methodData.size()) {
            return false;
        } else {
            return true;
        }

    }

    public void initLonelyListener() {
        this.setOnMouseMoved(e -> {
            if (app.getState().equals(SIZING_BOX)) {
                AppGUI appG = app.getAppGUI();
                int x = (int) e.getX();
                int y = (int) e.getY();
                //top
                if (y <= 2 && y >= - 2) {
                    appG.getPrimaryScene().setCursor(Cursor.N_RESIZE);
                    sizingRegion = regions.NORTH.toString();

                } // bottom
                else if (y <= this.getHeight() + 2 && y >= this.getHeight() - 2) {
                    appG.getPrimaryScene().setCursor(Cursor.S_RESIZE);
                    sizingRegion = regions.SOUTH.toString();
                } else if (x <= 2 && x >= -2) {
                    appG.getPrimaryScene().setCursor(Cursor.W_RESIZE);
                    sizingRegion = regions.WEST.toString();
                } else if (x <= this.getWidth() + 2 && x >= this.getWidth() - 2) {
                    appG.getPrimaryScene().setCursor(Cursor.E_RESIZE);
                    sizingRegion = regions.EAST.toString();
                } else {
                    appG.getPrimaryScene().setCursor(Cursor.DEFAULT);
                    sizingRegion = "";
                }
                this.setOnMousePressed(t -> {
                    tempx = (int) t.getSceneX() + (int) this.getWidth();
                    tempprevwidth = (int) this.getWidth();
                    tempy = (int) t.getSceneY() + (int) this.getHeight();
                    tempprevheight = (int) this.getHeight();
                    app.handleChange();
                });
                this.setOnMouseDragged(b -> {
                    if (app.getState().equals(SIZING_BOX)) {
                        if (sizingRegion.equals(regions.NORTH.toString())) {
                            int diff2 = tempy - (int) b.getSceneY();
                            if (diff2 - 40 >= 0) {
                                this.setLayoutY(this.getY() + (b.getSceneY() - this.getY()) - 85);
                                this.y = this.getY() + (b.getSceneY() - this.getY() - 85);
                                this.setLayoutX(this.getX());

                                this.setPrefHeight(diff2);
                            }

                        } else if (sizingRegion.equals(regions.SOUTH.toString())) {
                            this.setLayoutX(this.getX());
                            this.setLayoutY(this.getY());
                            this.setPrefHeight(this.getHeight() + (b.getY() - this.getHeight()));

                        } else if (sizingRegion.equals(regions.EAST.toString())) {
                            this.setLayoutX(this.getX());
                            this.setLayoutY(this.getY());
                            this.setPrefWidth(this.getWidth() + (b.getX() - this.getWidth()));

                        } else if (sizingRegion.equals(regions.WEST.toString())) {
                            int diff = tempx - (int) b.getSceneX();
                            if (diff - 50 >= 0) {

                                this.setLayoutX(this.getX() + (b.getSceneX() - this.getX()));
                                this.x = this.getX() + (b.getSceneX() - this.getX());
                                this.setLayoutY(this.getY());

                                this.setPrefWidth(diff);
                            }

                        }
                    }

                });
                //top

                //bottom
                //left
                //right
            }

        });
        this.setOnMouseExited(e -> {
            app.getAppGUI().getPrimaryScene().setCursor(Cursor.DEFAULT);
        });
    }

    public void resizeNorth(int y) {
        this.setPrefHeight(Math.abs(this.getY() - y) + this.getPrefHeight());
        this.setLayoutY(y);

    }

    enum regions {
        NORTH, SOUTH, EAST, WEST
    }

    public void setSizingRegion(String a) {
        sizingRegion = a;
    }
    public static int decrementIndex(){
        index--;
        return index;
    }

}
