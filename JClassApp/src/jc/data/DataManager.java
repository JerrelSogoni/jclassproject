package jc.data;

import java.util.ArrayList;
import java.util.Stack;
import javafx.collections.ObservableList;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Effect;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Line;
import static jc.data.JClassBoxes.CLASS;
import static jc.data.JClassBoxes.INTERFACE;
import static jc.data.JClassMakerState.BOX_DONE;
import jc.data.JLine.Anchor;
import jc.gui.JClassClassPane;
import jc.gui.JClassInterfacePane;
import jc.gui.Workspace;
import saf.components.AppDataComponent;
import saf.AppTemplate;
import saf.ui.AppGUI;
import saf.ui.AppMessageDialogSingleton;

/**
 * This class serves as the data management component for this application.
 *
 * @author Richard McKenna
 * @version 1.0
 */
public class DataManager implements AppDataComponent {

    public static final String AGGREGATE = "AGGREGATE";
    public static final String EXTENDS = "EXTENDS";
    public static final String IMPLEMENTS = "IMPLEMENTS";
    public static final String USES = "USES";
    // FIRST THE THINGS THAT HAVE TO BE SAVED TO FILES
    public Stack undo;
    public Stack redo;
    public boolean undid = false;

    // THESE ARE THE boxes TO DRAW
    ObservableList<Node> boxes;
    //   ObservableList<Node> lines;
    ArrayList<Line> gridLines;

    // AND NOW THE EDITING DATA
    // THIS IS THE box CURRENTLY BEING SIZED BUT NOT YET ADDED
    Node box;

    // THIS IS THE SHAPE CURRENTLY SELECTED
    Node selectedBox;

    // CURRENT STATE OF THE APP
    JClassMakerState state;

    // THIS IS A SHARED REFERENCE TO THE APPLICATION
    AppTemplate app;

    // USE THIS WHEN THE SHAPE IS SELECTED
    Effect highlightedEffect;
    CheckBox snap;
    CheckBox grid;
    public static final String YELLOW_HEX = "#EEEE00";
    public static final Paint HIGHLIGHTED_COLOR = Paint.valueOf(YELLOW_HEX);
    public static final int HIGHLIGHTED_STROKE_THICKNESS = 3;

    /**
     * THis constructor creates the data manager and sets up the
     *
     *
     * @param initApp The application within which this data manager is serving.
     */
    public DataManager(AppTemplate initApp) throws Exception {
        // KEEP THE APP FOR LATER
        app = initApp;
        // NO SHAPE STARTS OUT AS SELECTED
        box = null;
        selectedBox = null;
        undo = new Stack();
        redo = new Stack();

        // THIS IS FOR THE SELECTED Box
        DropShadow dropShadowEffect = new DropShadow();
        dropShadowEffect.setOffsetX(0.0f);
        dropShadowEffect.setOffsetY(0.0f);
        dropShadowEffect.setSpread(1.0);
        dropShadowEffect.setColor(Color.YELLOW);
        dropShadowEffect.setBlurType(BlurType.GAUSSIAN);
        dropShadowEffect.setRadius(2);
        highlightedEffect = dropShadowEffect;

    }

    public ObservableList<Node> getBoxes() {
        return boxes;
    }

    public void setBoxes(ObservableList<Node> initBox) {
        boxes = initBox;
    }

    //  public ObservableList<Node> getLines() {
    //    return lines;
    // }
    // public void setLines(ObservableList<Node> initBox) {
    //     lines = initBox;
    //  }
    public void removeSelectedBox() {
        if (selectedBox != null) {
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            clearTable(workspace);
            cleanParents(selectedBox, workspace);
            if (selectedBox instanceof JLine) {
                JLine line = (JLine) selectedBox;
                removeParents(line.getJChildren(), line.getJParent());

            } else if (selectedBox instanceof Anchor) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show("Invalid Removal", "Cannot delete anchor, you must select the line then delete");

            } else if (selectedBox instanceof JClassBoxes) {
                for (int i = 0; i < boxes.size(); i++) {
                    Node lineRemove = boxes.get(i);
                    if (lineRemove instanceof JLine) {
                        JLine line = (JLine) lineRemove;
                        JClassBoxes p = line.getJParent();
                        JClassBoxes c = line.getJChildren();
                        if (c.equals(selectedBox)) {

                            removeParents(c, p);
                            i--;
                        } else if (p.equals(selectedBox)) {
                            removeParents(p, c);
                            i--;
                        }
                    }
                }
                boxes.remove(selectedBox);

            }
            selectedBox = null;
        }
    }

    /**
     * This function clears out the HTML tree and reloads it with the minimal
     * tags, like html, head, and body such that the user can begin editing a
     * page.
     */
    @Override
    public void reset() {
        if (selectedBox != null) {
            unhighlightBox(selectedBox);
        }
        selectedBox = null;
        box = null;
        boxes.clear();
        if (app != null) {
            setZoom(1);
            getSnap().setSelected(false);
            snapPlease(false);
            getGrid().setSelected(false);
            removeGridLine();
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            workspace.getWorkArea().setRight(workspace.getBlankRightPane());
            clearTable(workspace);
            workspace.reloadWorkspace();
        }

    }

    public void selectCreatedBox() {
        if (selectedBox != null) {
            unhighlightBox(selectedBox);
        }
        selectedBox = box;
        highlightBox(selectedBox);
        box = null;
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace();
        if (state == BOX_DONE) {
            state = ((JClassBoxes) selectedBox).getStartingState();
        }

    }

    public void unhighlightBox(Node box) {
        selectedBox.setEffect(null);
        if (box instanceof Anchor) {
            if (selectedBox instanceof JLine) {
                JLine line = (JLine) selectedBox;
                line.midToParent.setEffect(null);
            }
        } else if (selectedBox instanceof JLine) {
            JLine line = (JLine) selectedBox;
            line.midToParent.setEffect(null);
            line.end.setVisible(false);
            line.middle.setVisible(false);
            line.start.setVisible(false);
        } else {
            for (Node line : boxes) {
                if (line instanceof JLine) {
                    Anchor s = ((JLine) line).start;
                    Anchor m = ((JLine) line).middle;
                    Anchor e = ((JLine) line).end;
                    s.setVisible(false);
                    m.setVisible(false);
                    e.setVisible(false);
                }
            }
        }
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        workspace.getWorkArea().setRight(workspace.getBlankRightPane());
        cleanParents(box, workspace);
        clearTable(workspace);

    }

    public void highlightBox(Node box) {
        if (!(gridLines != null && gridLines.contains(box))) {
            box.setEffect(highlightedEffect);
        }
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        if (box instanceof JLine) {
            JLine selectedJLine = (JLine) box;
            Anchor start = selectedJLine.getJStart();
            Anchor end = selectedJLine.getJEnd();
            Anchor mid = selectedJLine.getJMiddle();
            start.setVisible(true);
            if (((JLine) box).split) {
                mid.setVisible(true);
            }
            end.setVisible(true);

        }
        cleanParents(box, workspace);
        workspace.loadSelectedBoxSettings(box);

    }

    public void startNewClassBox(int x, int y) {
        JClassClassBox newClassBox = new JClassClassBox(this);
        newClassBox.start(x, y);
        box = newClassBox;
        initNewBox();

    }

    public void startNewInterfaceBox(int x, int y) {
        JClassInterfaceBox newInterfaceBox = new JClassInterfaceBox(this);
        newInterfaceBox.start(x, y);
        box = newInterfaceBox;
        initNewBox();
    }

    public void initNewBox() {
        // DESELECT THE SELECTED SHAPE IF THERE IS ONE
        if (selectedBox != null) {
            Node selected = selectedBox;
            unhighlightBox(selected);
            selectedBox = null;
        }
        // ADD THE SHAPE TO THE CANVAS
        boxes.add(box);
        snapPlease(getSnap().isSelected());
        // GO INTO SHAPE SIZING MODE
        state = JClassMakerState.BOX_DONE;
    }

    public void makeLine(JClassBoxes child, String Jparent, String type) {
        JClassBoxes parent = findParent(Jparent);
        if (child != null) {
            switch (type) {
                case AGGREGATE:
                    JLine ag = new JLine(parent, child, AGGREGATE);
                    boxes.add(ag);
                    boxes.add(ag.getMidToParentLine());
                    boxes.add(ag.getAggregateHead());
                    boxes.add(ag.getJStart());
                    boxes.add(ag.getJMiddle());
                    boxes.add(ag.getJEnd());
                    break;
                case EXTENDS:
                    JLine ex = new JLine(parent, child, EXTENDS);
                    boxes.add(ex);
                    boxes.add(ex.getMidToParentLine());
                    boxes.add(ex.getExtendsHead());
                    boxes.add(ex.getJStart());
                    boxes.add(ex.getJMiddle());
                    boxes.add(ex.getJEnd());
                    break;
                case IMPLEMENTS:
                    JLine im = new JLine(parent, child, IMPLEMENTS);
                    boxes.add(im);
                    boxes.add(im.getMidToParentLine());
                    boxes.add(im.getImplementsHead());
                    boxes.add(im.getJStart());
                    boxes.add(im.getJMiddle());
                    boxes.add(im.getJEnd());
                    break;
                case USES:
                    JLine us = new JLine(parent, child, USES);
                    boxes.add(us);
                    boxes.add(us.getMidToParentLine());
                    boxes.add(us.getUsesHead());
                    boxes.add(us.getJStart());
                    boxes.add(us.getJMiddle());
                    boxes.add(us.getJEnd());
                default:
                    break;

            }
        }

    }

    public JClassBoxes findParent(String parent) {
        for (int i = 0; i < boxes.size(); i++) {
            Node box = (Node) boxes.get(i);
            if (box instanceof JClassBoxes) {
                if (((JClassBoxes) box).getName().equals(parent)) {
                    return (JClassBoxes) box;
                }
            }
        }
        return null;
    }

    public Node getNewBox() {
        return box;
    }

    public Node getSelectedBox() {
        return selectedBox;
    }

    public void setSelectedBox(Node initSelectedBox) {
        selectedBox = initSelectedBox;
    }

    public Node selectTopBox(int x, int y, Node node) {
        Node box = getTopBox(x, y, node);
        if (box == selectedBox) {
            return box;
        }
        if (selectedBox != null) {
            Node selected = selectedBox;
            unhighlightBox(box);
        }
        if (box != null) {
            highlightBox(box);
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
        }
        selectedBox = box;
        return box;
    }

    public Node getTopBox(int x, int y, Node node) {
        if (node instanceof Anchor) {
            Anchor a = (Anchor) node;
            if (a.isVisible()) {
                return node;
            }
        }
        if (node instanceof JLine) {
            highlightBox(((JLine) node).getMidToParentLine());
            return node;
        }
        if (node instanceof Line) {
            Line halfLine = (Line) node;
            for (int i = 0; i < boxes.size(); i++) {
                Node n = boxes.get(i);
                if (n instanceof JLine) {
                    if (((JLine) n).getMidToParentLine().equals(halfLine)) {
                        highlightBox(halfLine);
                        return n;
                    }

                }
            }
        }
        for (int i = boxes.size() - 1; i >= 0; i--) {
            Node box = (Node) boxes.get(i);
            if (doesItPointToBox(x, y, box)) {
                return box;
            }
        }
        return null;
    }

    public void addBox(Node boxToAdd) {
        boxes.add(boxToAdd);
    }

    public void removeBox(Node boxToRemove) {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        cleanParents(boxToRemove, workspace);
        boxes.remove(boxToRemove);
    }

    public JClassMakerState getState() {
        return state;
    }

    public void setState(JClassMakerState initState) {
        state = initState;
    }

    public boolean isInState(JClassMakerState testState) {
        return state == testState;
    }

    public boolean doesItPointToBox(int x, int y, Node box) {
        if (box instanceof JClassBoxes) {
            JClassBoxes uml = (JClassBoxes) box;
            if (uml instanceof JClassClassBox) {
                int boxX = (int) ((JClassClassBox) uml).getLayoutX();
                int boxY = (int) ((JClassClassBox) uml).getLayoutY();
                int width = (int) uml.getWidth() + boxX;
                int height = (int) uml.getHeight() + boxY;

                if ((x >= boxX && x <= width) && (y >= boxY && y <= height)) {
                    return true;
                } else {
                    return false;
                }

            } else if (uml instanceof JClassInterfaceBox) {
                int boxX = (int) ((JClassInterfaceBox) uml).getLayoutX();
                int boxY = (int) ((JClassInterfaceBox) uml).getLayoutY();
                int width = (int) uml.getWidth() + boxX;
                int height = (int) uml.getHeight() + boxY;

                if ((x >= boxX && x <= width) && (y >= boxY && y <= height)) {
                    return true;
                } else {
                    return false;
                }
            }

        } else if (box instanceof JLine) {
            JLine line = (JLine) box;
            return false;

        }
        return false;
    }

    public void changeClassName() {
        if (selectedBox != null) {
            this.handleChange();
            if (selectedBox instanceof JClassClassBox) {
                this.handleChange();
                JClassClassBox selectedClass = (JClassClassBox) selectedBox;
                String namebefore = selectedClass.getName();
                Workspace workspace = (Workspace) app.getWorkspaceComponent();
                TextField className = workspace.getJClassPane().getClassInterfaceHbox();
                selectedClass.getClassName().setText(className.getText());
                String nameAfter = selectedClass.getName();
                for (int i = 0; i < boxes.size(); i++) {
                    Node n = boxes.get(i);
                    if (n instanceof JClassClassBox) {
                        ArrayList<String> p = ((JClassClassBox) n).getBoxParent();
                        for (int j = 0; j < p.size(); j++) {
                            String c = p.get(j);
                            if (c.equals(namebefore)) {
                                p.set(j, nameAfter);
                            }
                        }
                    } else if (n instanceof JClassInterfaceBox) {
                        String c = ((JClassInterfaceBox) n).getBoxParent();
                        if (c.equals(namebefore)) {
                            ((JClassInterfaceBox) n).setBoxParent(nameAfter);
                        }
                    }
                }

            } else {
                JClassInterfaceBox selectedClass = (JClassInterfaceBox) selectedBox;
                String namebefore = selectedClass.getName();
                Workspace workspace = (Workspace) app.getWorkspaceComponent();
                TextField interfaceName = workspace.getJClassInterfacePane().getClassInterfaceHbox();
                selectedClass.getClassName().setText(interfaceName.getText());
                String nameAfter = selectedClass.getName();
                for (int i = 0; i < boxes.size(); i++) {
                    Node n = boxes.get(i);
                    if (n instanceof JClassInterfaceBox) {
                        if (((JClassInterfaceBox) n).getBoxParent().equals(namebefore)) {
                            ((JClassInterfaceBox) n).setBoxParent(nameAfter);
                        }
                    }
                }

            }

        }

    }

    public void changePackageName() {
        if (selectedBox != null) {
            if (selectedBox instanceof JClassClassBox) {
                this.handleChange();
                JClassClassBox selectedClass = (JClassClassBox) selectedBox;
                Workspace workspace = (Workspace) app.getWorkspaceComponent();
                TextField packageName = workspace.getJClassPane().getPackageField();
                selectedClass.setPackageName(packageName.getText());

            } else {
                JClassInterfaceBox selectedClass = (JClassInterfaceBox) selectedBox;
                Workspace workspace = (Workspace) app.getWorkspaceComponent();
                TextField packageName = workspace.getJClassInterfacePane().getPackageField();
                selectedClass.setPackageName(packageName.getText());
            }
        }

    }

    public void setSnap(CheckBox snap) {
        this.snap = snap;
    }

    public CheckBox getSnap() {
        return snap;
    }

    public void setGrid(CheckBox grid) {
        this.grid = grid;
    }

    public CheckBox getGrid() {
        return grid;
    }

    public void cleanParents(Node box, Workspace workspace) {
        if (box instanceof JClassClassBox) {
            JClassClassPane removeParents = workspace.getJClassPane();
            removeParents.getParents().getSelectionModel().clearSelection();
            removeParents.getParents().getItems().clear();

        } else if (box instanceof JClassInterfaceBox) {
            JClassInterfacePane removeParents = workspace.getJClassInterfacePane();
            removeParents.getParents().getSelectionModel().clearSelection();
            removeParents.getParents().getItems().clear();
        }
    }

    public void removeLine(JLine line) {

        Anchor start = line.getJStart();
        Anchor mid = line.getJMiddle();
        Anchor end = line.getJEnd();
        boxes.remove(line);
        boxes.remove(start);
        boxes.remove(mid);
        boxes.remove(end);
        boxes.remove(line.midToParent);
        if (line.getAggregateHead() != null) {
            boxes.remove(line.getAggregateHead());
        }
        if (line.getExtendsHead() != null) {
            boxes.remove(line.getExtendsHead());
        }
        if (line.getUsesHead() != null) {
            boxes.remove(line.getUsesHead());
        }
        if (line.getImplementsHead() != null) {
            boxes.remove(line.getImplementsHead());

        }

    }

    public void clearParents(JClassClassBox box) {
        for (int i = 0; i < boxes.size(); i++) {
            Node n = boxes.get(i);
            if (n instanceof JLine) {

            }
        }

    }

    public void clearParents(JClassInterfaceBox box) {

    }

    public void removeParents(JClassBoxes box, JClassBoxes par) {

        if (box instanceof JClassClassBox) {
            this.handleChange();
            JClassClassBox b = (JClassClassBox) box;
            ArrayList<String> p = b.getBoxParent();
            for (int i = 0; i < boxes.size(); i++) {
                Node n = boxes.get(i);
                if (n instanceof JLine) {
                    JLine boss = (JLine) n;
                    JClassBoxes parent = boss.getJParent();
                    JClassBoxes child = boss.getJChildren();
                    if (parent.equals(par) && child.equals(box)) {
                        removeLine(boss);
                        p.remove(par.getName());

                    } else if (parent.equals(box) && child.equals(par)) {
                        if (child instanceof JClassClassBox) {
                            ((JClassClassBox) child).getBoxParent().remove(box.getName());
                            removeLine(boss);
                        }

                    }
                }

            }
        } else {
            JClassInterfaceBox b = (JClassInterfaceBox) box;
            String p = b.getBoxParent();
            if (boxes != null) {
                for (int i = 0; i < boxes.size(); i++) {
                    Node n = boxes.get(i);
                    if (n instanceof JLine) {
                        JLine boss = (JLine) n;
                        JClassBoxes parent = boss.getJParent();
                        JClassBoxes child = boss.getJChildren();
                        if (parent.getName().equals(p) && child.equals(box)) {
                            removeLine(boss);
                            b.setBoxParent("");
                        } else if (parent.equals(box) && child.equals(par)) {
                            if (child instanceof JClassClassBox) {
                                ((JClassClassBox) child).getBoxParent().remove(box.getName());
                                removeLine(boss);
                            } else if (child instanceof JClassInterfaceBox) {
                                ((JClassInterfaceBox) child).setBoxParent("");
                                removeLine(boss);
                            }
                        }
                    }
                }

            }
        }

    }

    public void initGrid() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        gridLines = new ArrayList();
        for (int i = 25; i <= workspace.getCanvas().getPrefWidth(); i += 25) {
            Line line = new Line();
            line.setStartX(i);
            line.setStartY(0);
            line.setEndY(workspace.getCanvas().getPrefHeight());
            line.setEndX(i);
            line.setStrokeWidth(1);
            gridLines.add(line);

        }
        for (int i = 25; i <= workspace.getCanvas().getPrefHeight(); i += 25) {
            Line line = new Line();
            line.setStartY(i);
            line.setStartX(0);
            line.setEndX(workspace.getCanvas().getPrefWidth());
            line.setEndY(i);
            line.setStrokeWidth(2);
            gridLines.add(line);

        }

    }

    public void processGridLines(boolean a) {
        if (a) {
            initGrid();
            getBoxes().addAll(0, getGridLines());
        } else {
            removeGridLine();

        }
    }

    public void removeGridLine() {
        if (gridLines != null) {
            boxes.removeAll(gridLines);
        }
    }

    public ArrayList<Line> getGridLines() {
        return gridLines;
    }

    public void snapPlease(boolean a) {
        if (a && grid.isSelected()) {
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            double x = 0;
            double y = 0;
            for (Node box : boxes) {
                if (box instanceof JClassBoxes) {
                    if (box instanceof JClassClassBox) {

                        for (int i = 0; i <= workspace.getCanvas().getPrefWidth(); i += 25) {
                            if (i > ((JClassClassBox) ((JClassBoxes) box)).getLayoutX()) {
                                x = i - 25;
                                break;
                            }
                        }
                        for (int i = 0; i <= workspace.getCanvas().getHeight(); i += 25) {
                            if (i > ((JClassClassBox) ((JClassBoxes) box)).getLayoutY()) {
                                y = i - 25;
                                break;
                            }
                        }
                        ((JClassBoxes) box).drag((int) x, (int) y);

                    }

                } else if (box instanceof JClassInterfaceBox) {

                    for (int i = 0; i <= workspace.getCanvas().getPrefWidth(); i += 25) {
                        if (i > ((JClassInterfaceBox) ((JClassBoxes) box)).getLayoutX()) {
                            x = i - 25;
                            break;
                        }
                    }
                    for (int i = 0; i <= workspace.getCanvas().getHeight(); i += 25) {
                        if (i > ((JClassInterfaceBox) ((JClassBoxes) box)).getLayoutY()) {
                            y = i - 25;
                            break;
                        }
                    }
                    ((JClassBoxes) box).drag((int) x, (int) y);
                }
            }
            for (Node line : boxes) {
                if (line instanceof JLine) {
                    JClassBoxes p = ((JLine) line).getJParent();
                    JClassBoxes c = ((JLine) line).getJChildren();
                    double pX = p.getX();
                    double pY = p.getY();
                    double cX = c.getX();
                    double cY = c.getY();
                    Anchor start = ((JLine) line).getJStart();
                    Anchor end = ((JLine) line).getJEnd();
                    start.dragWithBox((int) cX, (int) cY, start);
                    end.dragWithBox((int) pX, (int) pY, end);
                }
            }
        }
    }

    public JClassBoxes initClassParentBox(int x, int y, String string, String type) {
        if (type.equals(CLASS)) {
            JClassClassBox newClassBox = new JClassClassBox(this);
            newClassBox.start(x, y);
            newClassBox.getClassName().setText(string);
            return newClassBox;

        } else if (type.equals(INTERFACE)) {
            JClassInterfaceBox newInterfaceBox = new JClassInterfaceBox(this);
            newInterfaceBox.start(x, y);
            newInterfaceBox.getClassName().setText(string);
            return newInterfaceBox;
        }
        return null;
    }

    public ArrayList<String> listofAllBoxes() {
        ArrayList<String> a = new ArrayList();
        for (int i = 0; i < boxes.size(); i++) {
            Node n = boxes.get(i);
            if (n instanceof JClassBoxes) {
                JClassBoxes box = (JClassBoxes) n;
                a.add(box.getName());
            }
        }
        return a;
    }

    public void processCAttributes(ClassAttributes c) {
        ((JClassClassBox) selectedBox).addAttribute(c);

    }

    public void processCRefresh(ClassAttributes c) {
        ((JClassClassBox) selectedBox).refreshAttributes(c);

    }

    public void processCRemoval(ClassAttributes c) {
        ((JClassClassBox) selectedBox).removeCAttribute(c);
        int size = ((JClassClassBox) selectedBox).getClassAttributes().size();
        if (size == 0) {
            for (int i = 0; i < boxes.size(); i++) {
                Node n = boxes.get(i);
                if (n instanceof JLine) {
                    JClassBoxes lol = ((JLine) n).getJChildren();
                    JClassBoxes p = ((JLine) n).getJParent();
                    String c1 = lol.getName();
                    String c2 = p.getName();
                    if (c2.equals(((JClassClassBox) selectedBox).getName()) && ((JLine) n).getType().equals(AGGREGATE)) {
                        removeLine((JLine) n);
                    }

                }
            }
        } else {
            ObservableList<ClassAttributes> apples = ((JClassClassBox) selectedBox).getClassAttributes();
            ArrayList<String> apples2 = new ArrayList<>();
            for (ClassAttributes m : apples) {
                apples2.add(m.getAttributeType());
            }
            for (int i = 0; i < boxes.size(); i++) {
                Node n = boxes.get(i);
                if (n instanceof JLine) {
                    JClassBoxes apple = ((JLine) n).getJChildren();
                    JClassBoxes p = ((JLine) n).getJParent();
                    String c1 = apple.getName();
                    String c2 = p.getName();
                    if (c2.equals(((JClassBoxes) selectedBox).getName()) && !apples2.contains(c1) && ((JLine) n).getType().equals(AGGREGATE)) {
                        removeLine((JLine) n);
                    }

                }
            }
        }

    }

    public void processIAttributes(InterfaceAttributes get) {
        ((JClassInterfaceBox) selectedBox).addAttribute(get);

    }

    public void processIRemoval(InterfaceAttributes selected) {
        ((JClassInterfaceBox) selectedBox).removeIAttribute(selected);
        int size = ((JClassInterfaceBox) selectedBox).getInterfaceAttributes().size();
        if (size == 0) {
            for (int i = 0; i < boxes.size(); i++) {
                Node n = boxes.get(i);
                if (n instanceof JLine) {
                    JClassBoxes lol = ((JLine) n).getJChildren();
                    JClassBoxes p = ((JLine) n).getJParent();
                    String c1 = lol.getName();
                    String c2 = p.getName();
                    if (c2.equals(((JClassInterfaceBox) selectedBox).getName()) && ((JLine) n).getType().equals(AGGREGATE)) {
                        removeLine((JLine) n);
                    }

                }
            }
        } else {
            ObservableList<InterfaceAttributes> apples = ((JClassInterfaceBox) selectedBox).getInterfaceAttributes();
            ArrayList<String> apples2 = new ArrayList<>();
            for (InterfaceAttributes m : apples) {
                apples2.add(m.getAttributeType());
            }
            for (int i = 0; i < boxes.size(); i++) {
                Node n = boxes.get(i);
                if (n instanceof JLine) {
                    JClassBoxes apple = ((JLine) n).getJChildren();
                    JClassBoxes p = ((JLine) n).getJParent();
                    String c1 = apple.getName();
                    String c2 = p.getName();
                    if (c2.equals(((JClassInterfaceBox) selectedBox).getName()) && !apples2.contains(c1) && ((JLine) n).getType().equals(AGGREGATE)) {
                        removeLine((JLine) n);
                    }

                }
            }
        }

    }

    public void processIRefresh(InterfaceAttributes interfaceAttributes) {
        ((JClassInterfaceBox) selectedBox).refreshAttributes(interfaceAttributes);
    }

    private void clearTable(Workspace workspace) {
        if (workspace.getJClassInterfacePane() != null) {
            workspace.getJClassInterfacePane().getVariables().getSelectionModel().clearSelection();
            workspace.getJClassInterfacePane().clearTable();
        }
        if (workspace.getJClassPane() != null) {
            workspace.getJClassPane().getVariables().getSelectionModel().clearSelection();
            workspace.getJClassPane().clearTable();

        }
    }

    private void clearMethodTable(Workspace workspace) {
        if (workspace.getJClassInterfacePane() != null) {
            workspace.getJClassInterfacePane().getVariables().getSelectionModel().clearSelection();
            workspace.getJClassInterfacePane().getMethods().getSelectionModel().clearSelection();
            workspace.getJClassInterfacePane().clearTable();
        }
        if (workspace.getJClassPane() != null) {
            workspace.getJClassPane().getMethods().getSelectionModel().clearSelection();
            workspace.getJClassPane().getMethods().getSelectionModel().clearSelection();
            workspace.getJClassPane().clearTable();

        }

    }

    public void processCMRefresh(ClassMethods a) {
        ((JClassClassBox) selectedBox).refreshMethod(a);

    }

    public void processCMethods(ClassMethods get) {

        ((JClassClassBox) selectedBox).addMethods(get);
    }

    public void processCMRemoval(ClassMethods selected) {
        ((JClassClassBox) selectedBox).removeCMethods(selected);
        int size = ((JClassClassBox) selectedBox).getClassMethods().size();
        ObservableList<ClassMethods> apples = ((JClassClassBox) selectedBox).getClassMethods();
        if (size == 0) {
            for (int i = 0; i < boxes.size(); i++) {
                Node n = boxes.get(i);
                if (n instanceof JLine) {
                    JClassBoxes lol = ((JLine) n).getJChildren();
                    JClassBoxes p = ((JLine) n).getJParent();
                    String c1 = lol.getName();
                    String c2 = p.getName();
                    if (c2.equals(((JClassClassBox) selectedBox).getName()) && ((JLine) n).getType().equals(USES)) {
                        removeLine((JLine) n);
                    }

                }
            }
        } else {
            ArrayList<String> apples2 = new ArrayList<>();
            for (ClassMethods m : apples) {
                apples2.add(m.getMethodReturn());

                for (String aba2 : m.getArguments()) {
                    apples2.add(aba2);
                }
            }
            for (int i = 0; i < boxes.size(); i++) {
                Node n = boxes.get(i);
                if (n instanceof JLine) {
                    JClassBoxes c = ((JLine) n).getJChildren();
                    JClassBoxes p = ((JLine) n).getJParent();
                    String c1 = c.getName();
                    String c2 = p.getName();
                    if (c2.equals(((JClassBoxes) selectedBox).getName()) && !apples2.contains(c1) && ((JLine) n).getType().equals(USES)) {
                        removeLine((JLine) n);
                    }

                }
            }

        }

    }

    public void processIMethods(InterfaceMethods get) {

        ((JClassInterfaceBox) selectedBox).addMethods(get);

    }

    public void processIMRemoval(InterfaceMethods selected) {
        ((JClassInterfaceBox) selectedBox).removeIMethods(selected);
        int size = ((JClassInterfaceBox) selectedBox).getInterfaceMethods().size();
        ObservableList<InterfaceMethods> apples = ((JClassInterfaceBox) selectedBox).getInterfaceMethods();
        if (size == 0) {
            for (int i = 0; i < boxes.size(); i++) {
                Node n = boxes.get(i);
                if (n instanceof JLine) {
                    JClassBoxes lol = ((JLine) n).getJChildren();
                    JClassBoxes p = ((JLine) n).getJParent();
                    String c1 = lol.getName();
                    String c2 = p.getName();
                    if (c2.equals(((JClassInterfaceBox) selectedBox).getName()) && ((JLine) n).getType().equals(USES)) {
                        removeLine((JLine) n);
                    }

                }
            }
        } else {
            ArrayList<String> apples2 = new ArrayList<>();
            for (InterfaceMethods m : apples) {
                apples2.add(m.getMethodReturn());

                for (String aba2 : m.getArguments()) {
                    apples2.add(aba2);
                }
            }
            for (int i = 0; i < boxes.size(); i++) {
                Node n = boxes.get(i);
                if (n instanceof JLine) {
                    JClassBoxes c = ((JLine) n).getJChildren();
                    JClassBoxes p = ((JLine) n).getJParent();
                    String c1 = c.getName();
                    String c2 = p.getName();
                    if (c2.equals(((JClassBoxes) selectedBox).getName()) && !apples2.contains(c1) && ((JLine) n).getType().equals(USES)) {
                        removeLine((JLine) n);
                    }

                }
            }

        }

    }

    public void processIMRefresh(InterfaceMethods interfaceAttributes) {
        ((JClassInterfaceBox) selectedBox).refreshMethod(interfaceAttributes);
    }

    public void processCMTypeRefresh(ClassMethods a) {
        boolean exists = false;
        ObservableList<ClassMethods> apples = ((JClassClassBox) selectedBox).getClassMethods();
        ArrayList<String> apples2 = new ArrayList<>();
        for (ClassMethods m : apples) {
            apples2.add(m.getMethodReturn());
        }
        for (int i = 0; i < apples.size(); i++) {
            String a1 = apples.get(i).getMethodReturn();
            JClassBoxes con = findParent(a1);
            if (con != null) {
                if (con.getName().equals(a.getMethodReturn())) {
                    exists = true;
                    JLine line = findoutLine(a.getMethodReturn(), ((JClassClassBox) selectedBox).getName(), USES);
                    if (line == null) {
                        if (!a.getMethodReturn().equals("void")) {
                            makeLine(con, ((JClassClassBox) selectedBox).getName(), USES);
                        }
                    }

                }
            }
        }
        for (int i = 0; i < boxes.size(); i++) {
            Node n = boxes.get(i);
            if (n instanceof JLine) {
                JClassBoxes c = ((JLine) n).getJChildren();
                JClassBoxes p = ((JLine) n).getJParent();
                String c1 = c.getName();
                String c2 = p.getName();
                if (c2.equals(((JClassBoxes) selectedBox).getName()) && !apples2.contains(c1) && ((JLine) n).getType().equals(USES)) {
                    removeLine((JLine) n);
                }

            }
        }
        if (!exists) {
            ((JClassClassBox) selectedBox).refreshMethod(a);
            if (!a.getMethodReturn().equals("void")) {
                JClassClassBox newClassBox = new JClassClassBox(this);
                newClassBox.start((int) ((JClassClassBox) selectedBox).getX() + (int) ((JClassClassBox) selectedBox).getWidth(), (int) ((JClassClassBox) selectedBox).getY() + (int) ((JClassClassBox) selectedBox).getHeight() / 2);
                newClassBox.getClassName().setText(a.getMethodReturn());
                boxes.add(newClassBox);
                makeLine(newClassBox, ((JClassClassBox) selectedBox).getName(), USES);
            }

        }
    }

    public void processCMTypeRefreshArgs(ClassMethods a, ArrayList<String> b) {
        boolean exists = false;
        ObservableList<ClassMethods> apples = ((JClassClassBox) selectedBox).getClassMethods();
        ArrayList<String> apples2 = new ArrayList<>();
        for (ClassMethods m : apples) {
            apples2.add(m.getMethodReturn());
            if (b != null) {
                for (String aba : b) {
                    if (!m.getMethodReturn().equals(aba)) {
                        apples2.add(aba);
                    }
                }
            }
            for (String aba2 : m.getArguments()) {
                apples2.add(aba2);
            }
        }
        for (int i = 0; i < apples2.size(); i++) {
            String a1 = apples2.get(i);
            JClassBoxes con = findParent(a1);
            if (con != null) {
                if (con.getName().equals(a1)) {
                    exists = true;
                    JLine line = findoutLine(a1, ((JClassClassBox) selectedBox).getName(), USES);
                    if (line == null) {
                        if (!a.getMethodReturn().equals("void")) {
                            makeLine(con, ((JClassClassBox) selectedBox).getName(), USES);
                        } else if (a.getMethodReturn().equals("void") && !a1.equals("void")) {
                            makeLine(con, ((JClassClassBox) selectedBox).getName(), USES);
                        }
                    }

                }
            }
            if (!exists) {

                if (!a1.equals("void")) {
                    JClassClassBox newClassBox = new JClassClassBox(this);
                    newClassBox.start((int) ((JClassClassBox) selectedBox).getX() + (int) ((JClassClassBox) selectedBox).getWidth(), (int) ((JClassClassBox) selectedBox).getY() + (int) ((JClassClassBox) selectedBox).getHeight() / 2);
                    newClassBox.getClassName().setText(a1);
                    boxes.add(newClassBox);
                    makeLine(newClassBox, ((JClassClassBox) selectedBox).getName(), USES);
                }

            }
            exists = false;

        }
        ((JClassClassBox) selectedBox).refreshMethod(a);
        for (int i = 0; i < boxes.size(); i++) {
            Node n = boxes.get(i);
            if (n instanceof JLine) {
                JClassBoxes c = ((JLine) n).getJChildren();
                JClassBoxes p = ((JLine) n).getJParent();
                String c1 = c.getName();
                String c2 = p.getName();
                if (c2.equals(((JClassBoxes) selectedBox).getName()) && !apples2.contains(c1) && ((JLine) n).getType().equals(USES)) {
                    removeLine((JLine) n);
                }

            }
        }

    }

    public void processCATypeRefresh(ClassAttributes a) {
        boolean exists = false;
        ObservableList<ClassAttributes> apples = ((JClassClassBox) selectedBox).getClassAttributes();
        ArrayList<String> apples2 = new ArrayList<>();
        for (ClassAttributes m : apples) {
            apples2.add(m.getAttributeType());
        }
        for (int i = 0; i < apples.size(); i++) {
            String a1 = apples.get(i).getAttributeType();
            JClassBoxes con = findParent(a1);
            if (con != null) {
                if (con.getName().equals(a.getAttributeType())) {
                    exists = true;
                    JLine line = findoutLine(a.getAttributeType(), ((JClassClassBox) selectedBox).getName(), AGGREGATE);
                    if (line == null) {
                        if (!a.getAttributeType().equals("void")) {
                            makeLine(con, ((JClassClassBox) selectedBox).getName(), AGGREGATE);
                        }
                    }

                }
            }
        }
        for (int i = 0; i < boxes.size(); i++) {
            Node n = boxes.get(i);
            if (n instanceof JLine) {
                JClassBoxes c = ((JLine) n).getJChildren();
                JClassBoxes p = ((JLine) n).getJParent();
                String c1 = c.getName();
                String c2 = p.getName();
                if (c2.equals(((JClassBoxes) selectedBox).getName()) && !apples2.contains(c1) && ((JLine) n).getType().equals(AGGREGATE)) {
                    removeLine((JLine) n);
                }

            }
        }
        if (!exists) {

            if (!a.getAttributeType().equals("void")) {
                JClassClassBox newClassBox = new JClassClassBox(this);
                newClassBox.start((int) ((JClassClassBox) selectedBox).getX() + (int) ((JClassClassBox) selectedBox).getWidth(), (int) ((JClassClassBox) selectedBox).getY() + (int) ((JClassClassBox) selectedBox).getHeight() / 2);
                newClassBox.getClassName().setText(a.getAttributeType());
                boxes.add(newClassBox);
                makeLine(newClassBox, ((JClassClassBox) selectedBox).getName(), AGGREGATE);
            }

        }
        ((JClassClassBox) selectedBox).refreshAttributes(a);
    }

    public void processIMTypeRefresh(InterfaceMethods a) {
        boolean exists = false;
        for (int i = 0; i < boxes.size(); i++) {
            Node n = boxes.get(i);
            if (n instanceof JLine) {
                JLine line = (JLine) n;
                JClassBoxes child = line.getJChildren();
                if (a.getMethodReturn().equals(child.getName())) {
                    exists = true;
                }

            }
        }
        if (!exists) {
            ((JClassInterfaceBox) selectedBox).addMethods(a);
            JClassInterfaceBox newClassBox = new JClassInterfaceBox(this);
            newClassBox.start((int) ((JClassClassBox) selectedBox).getX() + (int) ((JClassClassBox) selectedBox).getWidth(), (int) ((JClassClassBox) selectedBox).getY() + (int) ((JClassClassBox) selectedBox).getHeight() / 2);
            newClassBox.getClassName().setText(a.getMethodReturn());
            boxes.add(newClassBox);
            makeLine(newClassBox, ((JClassInterfaceBox) selectedBox).getName(), USES);

        }

    }

    public void processIMTypeRefreshArgs(InterfaceMethods a, ArrayList<String> b) {
        boolean exists = false;
        ObservableList<InterfaceMethods> apples = ((JClassInterfaceBox) selectedBox).getInterfaceMethods();
        ArrayList<String> apples2 = new ArrayList<>();
        for (InterfaceMethods m : apples) {
            apples2.add(m.getMethodReturn());
            if (b != null) {
                for (String aba : b) {
                    if (!m.getMethodReturn().equals(aba)) {
                        apples2.add(aba);
                    }
                }
            }
            for (String aba2 : m.getArguments()) {
                apples2.add(aba2);
            }
        }
        for (int i = 0; i < apples2.size(); i++) {
            String a1 = apples2.get(i);
            JClassBoxes con = findParent(a1);
            if (con != null) {
                if (con.getName().equals(a1)) {
                    exists = true;
                    JLine line = findoutLine(a1, ((JClassInterfaceBox) selectedBox).getName(), USES);
                    if (line == null) {
                        if (!a.getMethodReturn().equals("void")) {
                            makeLine(con, ((JClassInterfaceBox) selectedBox).getName(), USES);
                        } else if (a.getMethodReturn().equals("void") && !a1.equals("void")) {
                            makeLine(con, ((JClassInterfaceBox) selectedBox).getName(), USES);
                        }
                    }

                }
            }
            if (!exists) {

                if (!a1.equals("void")) {
                    JClassClassBox newClassBox = new JClassClassBox(this);
                    newClassBox.start((int) ((JClassInterfaceBox) selectedBox).getX() + (int) ((JClassInterfaceBox) selectedBox).getWidth(), (int) ((JClassInterfaceBox) selectedBox).getY() + (int) ((JClassInterfaceBox) selectedBox).getHeight() / 2);
                    newClassBox.getClassName().setText(a1);
                    boxes.add(newClassBox);
                    makeLine(newClassBox, ((JClassInterfaceBox) selectedBox).getName(), USES);
                }

            }
            exists = false;

        }
        ((JClassInterfaceBox) selectedBox).refreshMethod(a);
        for (int i = 0; i < boxes.size(); i++) {
            Node n = boxes.get(i);
            if (n instanceof JLine) {
                JClassBoxes c = ((JLine) n).getJChildren();
                JClassBoxes p = ((JLine) n).getJParent();
                String c1 = c.getName();
                String c2 = p.getName();
                if (c2.equals(((JClassInterfaceBox) selectedBox).getName()) && !apples2.contains(c1) && ((JLine) n).getType().equals(USES)) {
                    removeLine((JLine) n);
                }

            }
        }

    }

    public void processIATypeRefresh(InterfaceAttributes a) {

        boolean exists = false;
        ObservableList<InterfaceAttributes> apples = ((JClassInterfaceBox) selectedBox).getInterfaceAttributes();
        ArrayList<String> apples2 = new ArrayList<>();
        for (InterfaceAttributes m : apples) {
            apples2.add(m.getAttributeType());
        }
        for (int i = 0; i < apples.size(); i++) {
            String a1 = apples.get(i).getAttributeType();
            JClassBoxes con = findParent(a1);
            if (con != null) {
                if (con.getName().equals(a.getAttributeType())) {
                    exists = true;
                    JLine line = findoutLine(a.getAttributeType(), ((JClassInterfaceBox) selectedBox).getName(), AGGREGATE);
                    if (line == null) {
                        if (!a.getAttributeType().equals("void")) {
                            makeLine(con, ((JClassInterfaceBox) selectedBox).getName(), AGGREGATE);
                        }
                    }

                }
            }
        }
        for (int i = 0; i < boxes.size(); i++) {
            Node n = boxes.get(i);
            if (n instanceof JLine) {
                JClassBoxes c = ((JLine) n).getJChildren();
                JClassBoxes p = ((JLine) n).getJParent();
                String c1 = c.getName();
                String c2 = p.getName();
                if (c2.equals(((JClassInterfaceBox) selectedBox).getName()) && !apples2.contains(c1) && ((JLine) n).getType().equals(AGGREGATE)) {
                    removeLine((JLine) n);
                }

            }
        }
        if (!exists) {
            ((JClassInterfaceBox) selectedBox).refreshAttributes(a);
            if (!a.getAttributeType().equals("void")) {
                JClassClassBox newClassBox = new JClassClassBox(this);
                newClassBox.start((int) ((JClassInterfaceBox) selectedBox).getX() + (int) ((JClassInterfaceBox) selectedBox).getWidth(), (int) ((JClassInterfaceBox) selectedBox).getY() + (int) ((JClassInterfaceBox) selectedBox).getHeight() / 2);
                newClassBox.getClassName().setText(a.getAttributeType());
                boxes.add(newClassBox);
                makeLine(newClassBox, ((JClassInterfaceBox) selectedBox).getName(), AGGREGATE);
            }

        }
    }

    public JLine findoutLine(String a, String b, String c) {
        ObservableList<Node> aaaa = boxes;
        for (int i = 0; i < aaaa.size(); i++) {
            Node n = aaaa.get(i);
            if (n instanceof JLine) {

                if (((JLine) n).getJChildren().getName().equals(a) && ((JLine) n).getJParent().getName().equals(b) && ((JLine) n).getType().equals(c)) {
                    return (JLine) n;
                }

            }
        }
        return null;
    }

    // undo Redo State saving
    public void zoomOut() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        if (!(workspace.getZoom() - .05 <= 0)) {
            double zoomin = workspace.getZoom() - .05;
            workspace.setZoom(workspace.getZoom() - .05);
            workspace.getCanvas().setScaleX(zoomin);
            workspace.getCanvas().setScaleY(zoomin);
        }

    }

    public void zoomIn() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        if (!(workspace.getZoom() + .2 >= 2)) {
            double zoomin = workspace.getZoom() + .05;
            workspace.setZoom(workspace.getZoom() + .05);
            workspace.getCanvas().setScaleX(zoomin);
            workspace.getCanvas().setScaleY(zoomin);
        }

    }

    public AppGUI getAppGUI() {
        return app.getGUI();
    }

    public void handleChange() {
        if (undid) {
            redo.clear();
            this.reloadWorkspace();
        }
        States std = new States(this);
        std.saveWork();
        this.undo.add(std);
    }

    public void handleUndo() {
        undid = true;
        States std = new States(this);
        std.saveWork();
        this.redo.add(std);
        States stds = (States) undo.pop();
        stds.loadWork();
    }

    public void handleRedo() {
        undid = false;
        States std = new States(this);
        std.saveWork();
        this.undo.add(std);
        States lolz = (States) redo.pop();
        lolz.loadWork();
    }

    public Stack getUndoStack() {

        return undo;
    }

    public Stack getRedoStack() {
        undid = false;
        return redo;
    }

    public void reloadWorkspace() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace();
    }

    public void setZoom(double a) {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        workspace.getCanvas().setScaleX(a);
        workspace.getCanvas().setScaleY(a);
        workspace.setZoom(a);
    }

    public double getZoom() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        return workspace.getZoom();
    }

    @Override
    public void cleanReset() {
        undo.clear();
        redo.clear();
        reset();

    }

}
