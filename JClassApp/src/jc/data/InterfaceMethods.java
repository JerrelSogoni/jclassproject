/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jc.data;

import java.util.ArrayList;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Jerrel
 */
public class InterfaceMethods {
    private final SimpleStringProperty methodName;
    private final SimpleStringProperty methodReturn;
    private final SimpleStringProperty methodAccess;
    private final SimpleBooleanProperty isException;
    private ArrayList<String> arguments;
    public InterfaceMethods(){
        this.methodName = new SimpleStringProperty("default");
        methodReturn = new SimpleStringProperty("void");
        methodAccess = new SimpleStringProperty("public");
        isException = new SimpleBooleanProperty(false);
        arguments = new ArrayList<>();
    }

    /**
     * @return the methodName
     */
    public String getMethodName() {
        return methodName.get();
    }

    /**
     * @param attributeName the methodName to set
     */
    public void setMethodName(String attributeName) {
        this.methodName.set(attributeName);
    }

    /**
     * @return the methodReturn
     */
    public String getMethodReturn() {
        return methodReturn.get();
    }

    /**
     * @param attributeType the methodReturn to set
     */
    public void setMethodReturn(String attributeType) {
        this.methodReturn.set(attributeType);
    }

    /**
     * @return the methodAccess
     */
    public String getMethodAccess() {
        return methodAccess.get();
    }

    /**
     * @param attributeAccess the methodAccess to set
     */
    public void setMethodAccess(String attributeAccess) {
        this.methodAccess.set(attributeAccess);
    }


    /**
     * @return the arguments
     */
    public ArrayList<String> getArguments() {
        return arguments;
    }

    /**
     * @param arguments the arguments to set
     */
    public void setArguments(ArrayList<String> arguments) {
        this.arguments = arguments;
    }
    public boolean getIsException() {
        return isException.get();
    }

    /**
     * @param isException the isException to set
     */
    public void setIsException(boolean isException) {
        this.isException.set(isException);
    }
    public SimpleBooleanProperty isExceptionProperty(){
        return isException;
    }
        
    
}
