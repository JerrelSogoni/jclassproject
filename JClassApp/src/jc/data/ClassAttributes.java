/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jc.data;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Jerrel
 */
public class ClassAttributes {
    private final SimpleStringProperty attributeName;
    private final SimpleStringProperty attributeType;
    private final SimpleStringProperty attributeAccess;
    private  SimpleBooleanProperty isFinal;
    private  SimpleBooleanProperty isStatic;
    public ClassAttributes(){
        this.attributeName = new SimpleStringProperty("default");
        attributeType = new SimpleStringProperty("String");
        attributeAccess = new SimpleStringProperty("public");
        isStatic = new SimpleBooleanProperty(false);
        isFinal = new SimpleBooleanProperty(false);
        
    }

    /**
     * @return the attributeName
     */
    public String getAttributeName() {
        return attributeName.get();
    }

    /**
     * @param attributeName the attributeName to set
     */
    public void setAttributeName(String attributeName) {
        this.attributeName.set(attributeName);
    }

    /**
     * @return the attributeType
     */
    public String getAttributeType() {
        return attributeType.get();
    }

    /**
     * @param attributeType the attributeType to set
     */
    public void setAttributeType(String attributeType) {
        this.attributeType.set(attributeType);
    }

    /**
     * @return the attributeAccess
     */
    public String getAttributeAccess() {
        return attributeAccess.get();
    }

    /**
     * @param attributeAccess the attributeAccess to set
     */
    public void setAttributeAccess(String attributeAccess) {
        this.attributeAccess.set(attributeAccess);
    }

    /**
     * @return the isStatic
     */
    public boolean getIsStatic() {
        return isStatic.get();
    }

    /**
     * @param isStatic the isStatic to set
     */
    public void setIsStatic(boolean isStatic) {
        this.isStatic.set(isStatic);
    }
    public String getStaticStringValue(){
        return isStatic.get() ? "True": "False";
    }
      
    public void setIsFinal(boolean isFinal) {
        this.isFinal.set(isFinal);
    }
    public String getStaticStringValue(SimpleBooleanProperty a){
        return a.get() ? "True": "False";
    }
    public boolean getIsFinal() {
        return isFinal.get();
    }
    public SimpleBooleanProperty isFinalProperty(){
        return isFinal;
    }
    public void setIsStaticProperty(SimpleBooleanProperty prop){
        isStatic = prop;
    }
    public SimpleBooleanProperty isStaticProperty(){
        return isStatic;
    }
    public void isFinalProperty(SimpleBooleanProperty prop){
        isFinal = prop;
    }
}
