/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jc.data;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Jerrel
 */
public class InterfaceAttributes {
    private final SimpleStringProperty attributeName;
    private final SimpleStringProperty attributeType;
    private final SimpleStringProperty attributeAccess;
    public InterfaceAttributes(){
        this.attributeName = new SimpleStringProperty("default");
        attributeType = new SimpleStringProperty("String");
        attributeAccess = new SimpleStringProperty("public");
        
    }

    /**
     * @return the attributeName
     */
    public String getAttributeName() {
        return attributeName.get();
    }

    /**
     * @param attributeName the attributeName to set
     */
    public void setAttributeName(String attributeName) {
        this.attributeName.set(attributeName);
    }

    /**
     * @return the attributeType
     */
    public String getAttributeType() {
        return attributeType.get();
    }

    /**
     * @param attributeType the attributeType to set
     */
    public void setAttributeType(String attributeType) {
        this.attributeType.set(attributeType);
    }

    /**
     * @return the attributeAccess
     */
    public String getAttributeAccess() {
        return attributeAccess.get();
    }

    /**
     * @param attributeAccess the attributeAccess to set
     */
    public void setAttributeAccess(String attributeAccess) {
        this.attributeAccess.set(attributeAccess);
    }
    
}
