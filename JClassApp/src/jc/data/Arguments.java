/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jc.data;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Jerrel
 */
public class Arguments {
    private final SimpleStringProperty argType;
    public Arguments(String a){
        argType = new SimpleStringProperty(a);
    }
        public String getArgType() {
        return argType.get();
    }

    /**
     * @param attributeType the attributeType to set
     */
    public void setArgType(String attributeType) {
        this.argType.set(attributeType);
    }
    
}
