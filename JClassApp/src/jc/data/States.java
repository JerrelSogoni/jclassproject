/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jc.data;

import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import static jc.data.DataManager.AGGREGATE;
import static jc.data.DataManager.EXTENDS;
import static jc.data.DataManager.IMPLEMENTS;
import static jc.data.DataManager.USES;
import static jc.data.JClassBoxes.CLASS;
import static jc.data.JClassBoxes.INTERFACE;
import jc.data.JLine.Anchor;

/**
 *
 * @author Jerrel
 */
public class States {

    DataManager apps;
    ArrayList<JClassBoxes> boxes;
    ArrayList<JLine> lines;
    ArrayList<Node> saveInOrder;
    double zoomFactor = 1;
    boolean snap = false;
    boolean grid = false;

    public States(DataManager apps) {
        this.apps = apps;
        boxes = new ArrayList<>();
        lines = new ArrayList<>();
        saveInOrder = new ArrayList<>();
        zoomFactor = apps.getZoom();
        snap = apps.getSnap().isSelected();
        grid = apps.getGrid().isSelected();

    }

    public void saveWork() {

        ObservableList<Node> stateBoxes = apps.getBoxes();
        for (int i = 0; i < stateBoxes.size(); i++) {
            Node n = stateBoxes.get(i);
            if (n instanceof JClassClassBox) {
                JClassClassBox box1 = (JClassClassBox) n;
                deepCopyClassBox(box1);
            } else if (n instanceof JClassInterfaceBox) {
                JClassInterfaceBox box2 = (JClassInterfaceBox) n;
                deepCopyInterfaceBox(box2);

            }
        }
        for (int i = 0; i < stateBoxes.size(); i++) {
            Node n = stateBoxes.get(i);
            if (n instanceof JLine) {
                JLine line = (JLine) n;
                deepCopyJLines(line);
            }

        }

    }

    public void deepCopyClassBox(JClassClassBox box) {
        JClassClassBox iKnow = new JClassClassBox(apps);
        JClassClassBox.decrementIndex();
        // process name
        iKnow.getClassName().setText(box.getName());
        //Package
        iKnow.setPackageName(box.getPackage());
        //Parents
        ArrayList<String> p = box.getBoxParent();
        for (int k = 0; k < p.size(); k++) {
            String pp = p.get(k);
            iKnow.addParent(pp);
        }
        // proccess Location
        int x = (int) box.getLayoutX();
        int y = (int) box.getLayoutY();
        int x1 = (int) box.getX();
        int y1 = (int) box.getY();
        int prefH = (int) box.getPrefHeight();
        int prefW = (int) box.getPrefWidth();
        if (prefH <= 0 && prefW <= 0) {
            iKnow.setLayoutX(x);
            iKnow.setLayoutY(y);
        } else {
            iKnow.setLocationAndSize(x1, y1, prefW, prefH);

        }
        // attribute transfer
        ObservableList<ClassAttributes> apples1 = box.getClassAttributes();
        if (apples1 != null) {
            for (int i = 0; i < apples1.size(); i++) {
                ClassAttributes lol = apples1.get(i);
                String name = lol.getAttributeName();
                String type = lol.getAttributeType();
                String access = lol.getAttributeAccess();
                boolean isStatic = lol.getIsStatic();
                boolean isFinal = lol.getIsFinal();
                ClassAttributes b = new ClassAttributes();
                b.setAttributeAccess(access);
                b.setAttributeName(name);
                b.setAttributeType(type);
                b.setIsFinal(isFinal);
                b.setIsStatic(isStatic);
                iKnow.addAttribute(b);
            }
        }
        //methods transfer
        ObservableList<ClassMethods> apples2 = box.getClassMethods();
        if (apples2 != null) {
            for (int j = 0; j < apples2.size(); j++) {
                ClassMethods lol = apples2.get(j);
                String name = lol.getMethodName();
                String type = lol.getMethodReturn();
                String access = lol.getMethodAccess();
                boolean isStatic = lol.getIsStatic();
                boolean isAbstract = lol.getIsAbstract();
                boolean isException = lol.getIsException();
                ClassMethods b = new ClassMethods();
                b.setMethodAccess(access);
                b.setMethodName(name);
                b.setMethodReturn(type);
                b.setIsAbstract(isAbstract);
                b.setIsStatic(isStatic);
                b.setIsException(isException);
                ArrayList<String> args = lol.getArguments();
                if (args != null || args.size() > 0) {
                    ArrayList<String> bargs = new ArrayList<>();
                    for (String abs : args) {
                        bargs.add(abs);
                    }
                    b.setArguments(bargs);
                }

                iKnow.addMethods(b);
            }
        }
        boxes.add(iKnow);
        saveInOrder.add(iKnow);

    }

    public void deepCopyInterfaceBox(JClassInterfaceBox box) {
        JClassInterfaceBox iKnow = new JClassInterfaceBox(apps);
        JClassInterfaceBox.decrementIndex();
        // process name
        iKnow.getClassName().setText(box.getName());
        //Package
        iKnow.setPackageName(box.getPackage());
        // proccess Location
        int x = (int) box.getX();
        int y = (int) box.getY();
        int prefH = (int) box.getPrefHeight();
        int prefW = (int) box.getPrefWidth();
        if (!(prefH <= 0 && prefW <= 0)) {
            iKnow.setLayoutX(x);
            iKnow.setLayoutY(y);
        } else {
            iKnow.setLocationAndSize(x, y, prefW, prefH);

        }
        //Parent
        iKnow.setBoxParent(box.getBoxParent());
        // attribute transfer
        ObservableList<InterfaceAttributes> apples1 = box.getInterfaceAttributes();
        if (apples1 != null) {
            for (int i = 0; i < apples1.size(); i++) {
                InterfaceAttributes lol = apples1.get(i);
                String name = lol.getAttributeName();
                String type = lol.getAttributeType();
                String access = lol.getAttributeAccess();
                InterfaceAttributes b = new InterfaceAttributes();
                b.setAttributeAccess(access);
                b.setAttributeName(name);
                b.setAttributeType(type);
                iKnow.addAttribute(b);
            }
        }
        //methods transfer
        ObservableList<InterfaceMethods> apples2 = box.getInterfaceMethods();
        if (apples2 != null) {

            for (int j = 0; j < apples2.size(); j++) {
                InterfaceMethods lol = apples2.get(j);
                String name = lol.getMethodName();
                String type = lol.getMethodReturn();
                String access = lol.getMethodAccess();
                boolean isException = lol.getIsException();
                InterfaceMethods b = new InterfaceMethods();
                b.setMethodAccess(access);
                b.setMethodName(name);
                b.setMethodReturn(type);
                b.setIsException(isException);
                ArrayList<String> args = lol.getArguments();
                if (args != null || args.size() > 0) {
                    ArrayList<String> bargs = new ArrayList<>();
                    for (String abs : args) {
                        bargs.add(abs);
                    }
                    b.setArguments(bargs);
                }
                iKnow.addMethods(b);
            }
        }
        boxes.add(iKnow);
        saveInOrder.add(iKnow);

    }

    public void deepCopyJLines(JLine line) {

        JClassBoxes c = line.getJChildren();
        JClassBoxes p = line.getJParent();
        Anchor start = line.getJStart();
        Anchor middle = line.getJMiddle();
        Anchor end = line.getJEnd();
        JClassBoxes c2 = confirmBox(c);
        JClassBoxes p2 = confirmBox(p);
        double startx = start.getCenterX();
        double starty = start.getCenterY();
        double middlex = middle.getCenterX();
        double middley = middle.getCenterY();
        double endx = end.getCenterX();
        double endy = end.getCenterY();
        boolean split = line.getSplit();
        String type = line.getType();
        JLine linez = new JLine(p2, c2, type);
        linez.start.setCenterX(startx);
        linez.start.setCenterY(starty);
        linez.middle.setCenterX(middlex);
        linez.middle.setCenterY(middley);
        linez.setEndX(middlex);
        linez.setEndY(middley);
        linez.midToParent.setStartX(middlex);
        linez.midToParent.setStartY(middley);
        linez.end.setCenterX(endx);
        linez.end.setCenterY(endy);
        linez.setSplit(split);
        saveInOrder.add(linez);
        saveInOrder.add(linez.midToParent);
        if (type.equals(AGGREGATE)) {
            saveInOrder.add(linez.getAggregateHead());
        } else if (type.equals(EXTENDS)) {
            saveInOrder.add(linez.getExtendsHead());
        } else if (type.equals(IMPLEMENTS)) {
            saveInOrder.add(linez.getImplementsHead());
        } else if (type.equals(USES)) {
            saveInOrder.add(linez.getUsesHead());
        }
        saveInOrder.add(linez.start);
        saveInOrder.add(linez.middle);
        saveInOrder.add(linez.end);

    }

    public JClassBoxes confirmBox(JClassBoxes a) {
        String name = a.getName();
        String type = a.getBoxType();
        if (type.equals(CLASS)) {
            int x = (int) ((JClassClassBox) a).getLayoutX();
            int y = (int) ((JClassClassBox) a).getLayoutY();
            for (int i = 0; i < boxes.size(); i++) {
                JClassBoxes compare = boxes.get(i);
                String name2 = compare.getName();
                String type2 = compare.getBoxType();
                if (type2.equals(CLASS)) {
                    int x2 = (int) ((JClassClassBox) compare).getLayoutX();
                    int y2 = (int) ((JClassClassBox) compare).getLayoutY();
                    if (name.equals(name2) && type.equals(type2) && x == x2 && y == y2) {
                        return compare;
                    }

                } else if (type2.equals(INTERFACE)) {
                    int x2 = (int) ((JClassInterfaceBox) compare).getLayoutX();
                    int y2 = (int) ((JClassInterfaceBox) compare).getLayoutY();
                    if (name.equals(name2) && type.equals(type2) && x == x2 && y == y2) {
                        return compare;
                    }

                }

            }

        } else if (type.equals(INTERFACE)) {
            int x = (int) ((JClassInterfaceBox) a).getLayoutX();
            int y = (int) ((JClassInterfaceBox) a).getLayoutY();
            for (int i = 0; i < boxes.size(); i++) {
                JClassBoxes compare = boxes.get(i);
                String name2 = compare.getName();
                String type2 = compare.getBoxType();
                if (type2.equals(CLASS)) {
                    int x2 = (int) ((JClassClassBox) compare).getLayoutX();
                    int y2 = (int) ((JClassClassBox) compare).getLayoutY();
                    if (name.equals(name2) && type.equals(type2) && x == x2 && y == y2) {
                        return compare;
                    }

                } else if (type2.equals(INTERFACE)) {
                    int x2 = (int) ((JClassInterfaceBox) compare).getLayoutX();
                    int y2 = (int) ((JClassInterfaceBox) compare).getLayoutY();
                    if (name.equals(name2) && type.equals(type2) && x == x2 && y == y2) {
                        return compare;
                    }

                }

            }

        }

        System.out.println("Why would this even happen");
        return null;

    }

    public void loadWork() {
        apps.reset();
        ObservableList<Node> list = apps.getBoxes();
        for (int i = 0; i < saveInOrder.size(); i++) {
            list.add(saveInOrder.get(i));
        }
        apps.setZoom(zoomFactor);
        if (grid) {
            apps.initGrid();
            apps.getBoxes().addAll(0, apps.getGridLines());
        } else if (apps.getGridLines() != null) {
            apps.removeGridLine();
        }
        apps.getGrid().setSelected(grid);
        apps.getSnap().setSelected(snap);
        apps.snapPlease(snap);
        apps.reloadWorkspace();
    }

}
