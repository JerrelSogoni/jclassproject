/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jc.data;

import java.util.ArrayList;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.Node;
import static javafx.scene.paint.Color.BLACK;
import static javafx.scene.paint.Color.CYAN;
import static javafx.scene.paint.Color.GRAY;
import static javafx.scene.paint.Color.SILVER;
import static javafx.scene.paint.Color.WHITE;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Rotate;
import static jc.data.DataManager.AGGREGATE;
import static jc.data.DataManager.EXTENDS;
import static jc.data.DataManager.IMPLEMENTS;
import static jc.data.DataManager.USES;

/**
 *
 * @author Jerrel
 */
public class JLine extends Line {

    private JClassBoxes parent;
    private JClassBoxes children;
    private double width;
    private double height;
    private String type;
    double lenghtMidX;
    private DoubleProperty lengthMX;
    private DoubleProperty lengthMY;
    double lenghtMidY;
    Anchor middle;
    Anchor end;
    Anchor start;
    Line midToParent;
    boolean split = false;
    private AggregateHead agg;
    private ImplementsHead impl;
    private UsesHead use;
    private ExtendsHead extendsHead;

    public JLine() {
    }

    public JLine(JClassBoxes parent, JClassBoxes children, String type) {
        this.type = type;
        this.parent = parent;
        this.children = children;
        Node p = (Node) parent;
        Node c = (Node) children;
        midToParent = new Line();
        determineBestStartLine(parent, children);
        findMid();
        middle = new Anchor(lengthMX, lengthMY);
        start = new Anchor(this.startXProperty(), this.startYProperty());
        end = new Anchor(midToParent.endXProperty(), midToParent.endYProperty());
        this.setStrokeWidth(5);
        midToParent.setStrokeWidth(5);
        if (type.equals(EXTENDS)) {
            setToSolidLine();
            extendsHead = new ExtendsHead();

        } else if (type.equals(IMPLEMENTS)) {
            setToDashedLine();
            impl = new ImplementsHead();
        } else if (type.equals(AGGREGATE)) {
            agg = new AggregateHead();

        } else if (type.equals(USES)) {
            use = new UsesHead();
        }
        arrowHeadRotation((int)midToParent.getEndX() , (int)midToParent.getEndY());

    }

    public void setToSolidLine() {
        this.setStrokeDashOffset(0);
    }

    public void setToDashedLine() {
        this.getStrokeDashArray().addAll(25d, 10d);
        midToParent.getStrokeDashArray().addAll(25d, 10d);
    }

    /**
     * @return the parent
     */
    public JClassBoxes getJParent() {
        return parent;
    }

    /**
     * @param parent the parent to set
     */
    public void setJParent(JClassBoxes parent) {
        this.parent = parent;
    }

    /**
     * @return the children
     */
    public JClassBoxes getJChildren() {
        return children;
    }

    /**
     * @param children the children to set
     */
    public void setJChildren(JClassBoxes children) {
        this.children = children;
    }

    /**
     * @return the width
     */
    public double getWidth() {
        return width;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(double width) {
        this.width = width;
    }

    /**
     * @return the height
     */
    public double getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(double height) {
        this.height = height;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    private void makeExtendsHead() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void makeImplementsHead() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void determineBestStartLine(JClassBoxes parent, JClassBoxes child) {
        // if child on top
        double boxX = parent.getX();
        double boxY = parent.getY();
        double width = parent.getWidth();
        double height = parent.getHeight();
        double boxXC = child.getX();
        double boxYC = child.getY();
        double widthC = child.getWidth();
        double heightC = child.getHeight();
        int topDetermine = ((int) boxYC - ((int) height + (int) boxY));
        int bottomDetermine = (((int) heightC + (int) boxYC) - (int) boxY);
        int leftDetermine = ((int) boxXC - ((int) width + (int) boxX));
        int rightDetermine = (((int) widthC + (int) boxXC) - (int) boxX);
        if (topDetermine < 0) {
            topDetermine = 9999999;
        }
        if (bottomDetermine < 0) {
            bottomDetermine = 9999999;
        }
        if (leftDetermine < 0) {
            leftDetermine = 9999999;
        }
        if (rightDetermine < 0) {
            rightDetermine = 9999999;
        }
        int findMinY = Math.min(topDetermine, bottomDetermine);
        int findMinX = Math.min(leftDetermine, rightDetermine);
        int findMin = Math.min(findMinY, findMinX);
        if (topDetermine == findMin && topDetermine != 9999999) {
            //start from top mid child to bottom mid of parent
            int bottomMidP = ((int) width / 2);
            int xMidP = (int) boxX + bottomMidP;
            this.setEndX(xMidP);
            this.setEndY(boxY + height);
            int BottomMidC = ((int) widthC / 2);
            int xMidC = (int) boxXC + BottomMidC;
            this.setStartX(xMidC);
            this.setStartY(boxYC);

        } //start bottom mid child to topmid parent
        else if (bottomDetermine == findMin && bottomDetermine != 9999999) {
            int topMidP = ((int) width / 2);
            int xMidP = (int) boxX + topMidP;
            this.setEndX(xMidP);
            this.setEndY(boxY);
            int BottomMidC = ((int) widthC / 2);
            int xMidC = (int) boxXC + BottomMidC;
            this.setStartX(xMidC);
            this.setStartY(heightC + boxYC);
        } // start leftmid child to right mid parent
        else if (leftDetermine == findMin && leftDetermine != 9999999) {
            int rightMidP = ((int) height / 2);
            int xMidrightP = (int) boxY + rightMidP;
            this.setEndX(boxX + width);
            this.setEndY(xMidrightP);
            int leftMidC = ((int) heightC / 2);
            int xLeftMidC = (int) boxYC + leftMidC;
            this.setStartX(boxXC);
            this.setStartY(xLeftMidC);

        } else {
            //start right mid child to left mid parent
            int leftMidP = ((int) height / 2);
            int xMidLeftP = (int) boxY + leftMidP;
            this.setEndX(boxX);
            this.setEndY(leftMidP + boxY);
            int leftMidC = ((int) heightC / 2);
            int xLeftMidC = (int) boxYC + leftMidC;
            this.setStartX(boxXC + widthC);
            this.setStartY(xLeftMidC);

        }
        setMid();
        midToParent.setStartX(lenghtMidX);
        midToParent.setStartY(lenghtMidY);
        midToParent.setEndX(this.getEndX());
        midToParent.setEndY(this.getEndY());
        this.setEndX(lenghtMidX);
        this.setEndY(lenghtMidY);


    }

    public class Anchor extends Circle {
        public boolean dragged = false;

        public Anchor(DoubleProperty x, DoubleProperty y) {
            setRadius(7);
            setCenterX(x.getValue());
            setCenterY(y.getValue());
            x.bind(centerXProperty());
            y.bind(centerYProperty());
            setVisible(false);
        }

        public void drag(int x, int y, Node anchor) {
            if (anchor instanceof Anchor) {
                Anchor a = (Anchor) anchor;

                if (a.equals(end)) {
                    ArrayList<Double> xycoord = setLineInBorder(x, y, parent);
                    if (xycoord.size() != 0) {
                        a.setCenterX(xycoord.get(0));
                        a.setCenterY(xycoord.get(1));
                        if (!split) {
                            adjustMidToLine();
                        }
                        if(!dragged && a.equals(end)){
                            arrowHeadRotation(xycoord.get(0).intValue(), xycoord.get(1).intValue());
                
                        }
                    }

                } else if (a.equals(start)) {
                    ArrayList<Double> xycoord = setLineInBorder(x, y, children);
                    if (xycoord.size() != 0) {
                        a.setCenterX(xycoord.get(0));
                        a.setCenterY(xycoord.get(1));
                        middle.setCenterX(midToParent.getStartX());
                        middle.setCenterY(midToParent.getStartY());
                        if (!split) {
                            adjustMidToLine();
                        }

                    }
                } else if (a.equals(middle)) {
                    midToParent.setStartX(x);
                    midToParent.setStartY(y);
                    JLine.this.setEndX(x);
                    JLine.this.setEndY(y);
                    middle.setCenterX(x);
                    middle.setCenterY(y);

                }
            }

        }

        public ArrayList<Double> setLineInBorder(int x, int y, JClassBoxes box) {
            ArrayList<Double> xycoord = new ArrayList<>();
            double boxX = box.getX();
            double boxY = box.getY();
            double width = box.getWidth() + boxX;
            double height = box.getHeight() + boxY;
            if (x < (int) boxX) {
                //return topleft
                xycoord.add(boxX);
                if (y < boxY) {
                    xycoord.add(boxY);
                } else if (y > height) {
                    xycoord.add(height);
                } else {
                    xycoord.add((double) y);
                }
            } else if (y < boxY) {

                if (x < boxX) {
                    xycoord.add(boxX);
                } else if (x > width) {
                    xycoord.add(width);
                } else {
                    xycoord.add((double) x);
                }
                xycoord.add(boxY);

            } else if (x > width) {
                //topright
                xycoord.add(width);
                if (y < boxY) {
                    xycoord.add(boxY);
                } else if (y > height) {
                    xycoord.add(height);
                } else {
                    xycoord.add((double) y);
                }

            } else if (y > height) {
                // bottom Right
                if (x < boxX) {
                    xycoord.add(boxX);

                } else if (x > width) {
                    xycoord.add(width);
                } else {
                    xycoord.add((double) x);
                }
                xycoord.add(height);
            }
            return xycoord;
        }

        public void dragWithBox(int x, int y, Anchor a) {
            int diffX = x - (int) getCenterX();
            int diffY = y - (int) getCenterY();
            int dragX = (int) getCenterX() + diffX;
            int dragY = (int) getCenterY() + diffY;
            setCenterX(dragX);
            setCenterY(dragY);
            if (!split) {
                adjustMidToLine();
            }

        }
    }

    public Anchor getJMiddle() {
        return middle;
    }

    public Anchor getJStart() {
        return start;
    }

    public Anchor getJEnd() {
        return end;
    }
    

    public void findMid() {
        lengthMX = new SimpleDoubleProperty();
        lengthMY = new SimpleDoubleProperty();
        setMid();
        lengthMX.set(midToParent.getStartX());
        lengthMY.set(midToParent.getStartY());

    }

    public void setMid() {
        lenghtMidX = ((int) this.getEndX() - (int) this.getStartX()) / 2;
        lenghtMidY = ((int) this.getEndY() - (int) this.getStartY()) / 2;
        if (lenghtMidX < 0) {
            lenghtMidX = (int) this.getStartX() - Math.abs(lenghtMidX);

        } else {
            lenghtMidX = (int) this.getStartX() + Math.abs(lenghtMidX);
        }
        if (lenghtMidY < 0) {
            lenghtMidY = (int) this.getStartY() - Math.abs(lenghtMidY);
        } else {
            lenghtMidY = (int) this.getStartY() + Math.abs(lenghtMidY);
        }

    }

    public void midPoint() {
        lenghtMidX = ((int) midToParent.getEndX() - (int) this.getStartX()) / 2;
        lenghtMidY = ((int) midToParent.getEndY() - (int) this.getStartY()) / 2;
        if (lenghtMidX < 0) {
            lenghtMidX = (int) this.getStartX() - Math.abs(lenghtMidX);

        } else {
            lenghtMidX = (int) this.getStartX() + Math.abs(lenghtMidX);
        }
        if (lenghtMidY < 0) {
            lenghtMidY = (int) this.getStartY() - Math.abs(lenghtMidY);
        } else {
            lenghtMidY = (int) this.getStartY() + Math.abs(lenghtMidY);
        }

    }

    public void setLenghtMidX(int a) {
        lenghtMidX = a;
    }

    public double getLenghtMidX() {
        return lenghtMidX;
    }

    public void setLenghtMidY(int a) {
        lenghtMidX = a;
    }

    public double getLenghtMidY() {
        return lenghtMidY;
    }

    public DoubleProperty lengthMXProperty() {
        return lengthMX;
    }

    public DoubleProperty lengthMYProperty() {
        return lengthMY;
    }

    public final double getLengthMX() {
        return lengthMX.get();
    }

    public final double getLengthMY() {
        return lengthMY.get();
    }

    public final void setLengthMX(double value) {
        lengthMX.set(value);
    }

    public final void setLengthMY(double value) {
        lengthMY.set(value);
    }

    public class AggregateHead extends Rectangle {

        public AggregateHead() {
            this.setX(midToParent.getEndX());
            this.setY(midToParent.getEndY());
            this.setWidth(15);
            this.setHeight(15);
            this.setRotate(45);
            this.setFill(GRAY);
            this.setStroke(BLACK);
            this.setStrokeWidth(1);
            this.xProperty().bind(midToParent.endXProperty());
            this.yProperty().bind(midToParent.endYProperty());

        }

    }

    public Line getMidToParentLine() {
        return midToParent;
    }

    public void adjustMidToLine() {
        midPoint();
        middle.setCenterX(lenghtMidX);
        middle.setCenterY(lenghtMidY);
        JLine.this.setEndX(middle.getCenterX());
        JLine.this.setEndY(middle.getCenterY());
        midToParent.setStartX(middle.getCenterX());
        midToParent.setStartY(middle.getCenterY());
    }

    public void isSplit(boolean a) {
        split = a;
    }

    public Rectangle getAggregateHead() {
        return agg;
    }

    public Polygon getImplementsHead() {
        return impl;
    }

    public Polygon getUsesHead() {
        return use;
    }

    public Polygon getExtendsHead() {
        return extendsHead;
    }

    public class ImplementsHead extends Polygon {

        public ImplementsHead() {
            super(new double[]{0, 0, 10, 20, -10, 20});
            this.setLayoutX(midToParent.getEndX());
            this.setLayoutY(midToParent.getEndY());
            this.setFill(WHITE);
            this.setStroke(BLACK);
            this.setStrokeWidth(1);
            this.layoutXProperty().bind(midToParent.endXProperty());
            this.layoutYProperty().bind(midToParent.endYProperty());

        }

    }

    public class UsesHead extends Polygon {

        public UsesHead() {
            super(new double[]{0, 0, 10, 20, -10, 20});
            this.getStrokeDashArray().addAll(3d, 3d, 3d);
            this.setLayoutX(midToParent.getEndX());
            this.setLayoutY(midToParent.getEndY());
            this.setFill(CYAN);
            this.setStroke(BLACK);
            this.setStrokeWidth(1);
            this.layoutXProperty().bind(midToParent.endXProperty());
            this.layoutYProperty().bind(midToParent.endYProperty());

        }

    }

    public class ExtendsHead extends Polygon {

        public ExtendsHead() {
            super(new double[]{0, 0, 10, 20, -10, 20});
            this.setLayoutX(midToParent.getEndX());
            this.setLayoutY(midToParent.getEndY());
            this.setFill(SILVER);
            this.setStroke(BLACK);
            this.setStrokeWidth(1);
            this.layoutXProperty().bind(midToParent.endXProperty());
            this.layoutYProperty().bind(midToParent.endYProperty());

        }
    }

    public void arrowHeadRotation(int i , int j) {
        if (i <= parent.getX() + 2 && i >= parent.getX() - 2) {
            if (agg != null && agg.getRotate() != 90) {
                agg.setRotate(90);
            }
            if (impl != null && impl.getRotate() != 90 ) {
                impl.setRotate(90);
            }
            if (use != null&& use.getRotate() != 90 ) {
                use.setRotate(90);
            }
            if (extendsHead != null && extendsHead.getRotate() != 90) {
                extendsHead.setRotate(90);

            }
        }
        else if (i <= parent.getX() + parent.getWidth() + 2 && i >= parent.getX() + parent.getWidth() - 2 ) {
            if (agg != null && agg.getRotate() != 270) {
                agg.setRotate(270);
            }
            if (impl != null&& impl.getRotate() != 270 ) {
                impl.setRotate(270);
            }
            if (use != null && use.getRotate() != 270) {
                use.setRotate(270);
            }
            if (extendsHead != null && extendsHead.getRotate() != 270) {
                extendsHead.setRotate(270);

            }

        }
        else if (j<= parent.getY() + 2 && j >= parent.getY() - 2) {
            if (agg != null && agg.getRotate() != 180) {
                agg.setRotate(180);
                      
            }
            if (impl != null && impl.getRotate() != 180) {
                impl.setRotate(180);
            }
            if (use != null && use.getRotate() != 180) {
                use.setRotate(180);
            }
            if (extendsHead != null && extendsHead.getRotate() != 180) {
                extendsHead.setRotate(180);

            }
        }
        else if (j <= parent.getY() + parent.getHeight() + 2 && j >= parent.getY() + parent.getHeight() - 2) {
            if (agg != null &&  agg.getRotate() != 0) {
                agg.setRotate(0);
            }
            if (impl != null && impl.getRotate() != 0) {
                impl.setRotate(0);
            }
            if (use != null && use.getRotate() != 0) {
                use.setRotate(0);
            }
            if (extendsHead != null && extendsHead.getRotate() != 0) {
                extendsHead.setRotate(0);

            }
        }
    }
    
    public void setSplit(boolean a){
        split = a;
        
    }
    public boolean getSplit(){
        return split;
    }
    public void updateLine(){
        if(split){
            middle.setVisible(true);
            
        }
        else{
            middle.setVisible(false);
            adjustMidToLine();
            
        }
               
    }
    public DoubleProperty getlengthMX(){
        return lengthMX;
    }
 
    
    public  DoubleProperty getlengthMY(){
        return lengthMY;
    }

    
    

}
