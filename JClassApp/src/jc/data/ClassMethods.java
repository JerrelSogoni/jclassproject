/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jc.data;

import java.util.ArrayList;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Jerrel
 */
public class ClassMethods {

    private final SimpleStringProperty methodName;
    private final SimpleStringProperty methodReturn;
    private final SimpleStringProperty methodAccess;
    private SimpleBooleanProperty isStatic;
    private SimpleBooleanProperty isAbstract;
    private SimpleBooleanProperty isException;
    private ArrayList<String> arguments;

    public ClassMethods() {
        this.methodName = new SimpleStringProperty("default");
        methodReturn = new SimpleStringProperty("String");
        methodAccess = new SimpleStringProperty("public");
        arguments = new ArrayList<>();
        isStatic = new SimpleBooleanProperty(false);
        isAbstract = new SimpleBooleanProperty(false);
        isException = new SimpleBooleanProperty(false);

    }

    /**
     * @return the methodName
     */
    public String getMethodName() {
        return methodName.get();
    }

    /**
     * @param attributeName the methodName to set
     */
    public void setMethodName(String attributeName) {
        this.methodName.set(attributeName);
    }

    /**
     * @return the methodReturn
     */
    public String getMethodReturn() {
        return methodReturn.get();
    }

    /**
     * @param attributeType the methodReturn to set
     */
    public void setMethodReturn(String attributeType) {
        this.methodReturn.set(attributeType);
    }

    /**
     * @return the methodAccess
     */
    public String getMethodAccess() {
        return methodAccess.get();
    }

    /**
     * @param attributeAccess the methodAccess to set
     */
    public void setMethodAccess(String attributeAccess) {
        this.methodAccess.set(attributeAccess);
    }

    /**
     * @return the isStatic
     */
    public boolean getIsStatic() {
        return isStatic.get();
    }

    /**
     * @param isStatic the isException to set
     */
    public void setIsStatic(boolean isStatic) {
        this.isStatic.set(isStatic);
    }

    public boolean getIsException() {
        return isException.get();
    }

    /**
     * @param isException the isException to set
     */
    public void setIsException(boolean isException) {
        this.isException.set(isException);
    }

    public String getBooleanStringValue(SimpleBooleanProperty a) {
        return a.get() ? "True" : "False";
    }

    /**
     * @return the isAbstract
     */
    public boolean getIsAbstract() {
        return isAbstract.get();
    }

    /**
     * @param isAbstract the isAbstract to set
     */
    public void setIsAbstract(boolean isAbstract) {
        this.isAbstract.set(isAbstract);
    }


    /**
     * @return the arguments
     */
    public ArrayList<String> getArguments() {
        return arguments;
    }

    /**
     * @param arguments the arguments to set
     */
    public void setArguments(ArrayList<String> arguments) {
        this.arguments = arguments;
    }

    public SimpleBooleanProperty isStaticProperty() {
        return isStatic;
    }

    public void setIsStaticProperty(SimpleBooleanProperty prop) {
        isStatic = prop;
    }

    public SimpleBooleanProperty isAbstractProperty() {
        return isAbstract;
    }

    public void isAbstractProperty(SimpleBooleanProperty prop) {
        isAbstract = prop;
    }
    public SimpleBooleanProperty isExceptionProperty(){
        return isException;    
    }


}
