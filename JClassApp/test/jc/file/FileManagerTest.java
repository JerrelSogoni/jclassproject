///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package jc.file;
//
//import java.io.File;
//import java.util.ArrayList;
//import javafx.collections.FXCollections;
//import javafx.collections.ObservableList;
//import javafx.scene.Node;
//import javafx.scene.control.CheckBox;
//import javafx.scene.paint.Color;
//import javafx.scene.shape.Shape;
//import javax.json.JsonObject;
//import jc.data.ClassAttributes;
//import jc.data.ClassMethods;
//import jc.data.DataManager;
//import jc.data.InterfaceAttributes;
//import jc.data.InterfaceMethods;
//import jc.data.JClassBoxes;
//import jc.data.JClassClassBox;
//import jc.data.JClassInterfaceBox;
//import jc.data.JLine;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import static org.junit.Assert.*;
//import org.junit.runner.RunWith;
//import saf.components.AppDataComponent;
//
///**
// *
// * @author Jerrel
// */
//@RunWith( JfxTestRunner.class )
//public class FileManagerTest {
//    
//    public FileManagerTest() {
//    }
//    
//    @BeforeClass
//    public static void setUpClass() {
//    }
//    
//    @AfterClass
//    public static void tearDownClass() {
//    }
//    
//    @Before
//    public void setUp() {
//    }
//    
//    @After
//    public void tearDown() {
//    }
//
//    /**
//     * Test of hard coded design, of class FileManager.
//     */
//    @Test
//    public void Test1() throws Exception {
//        DataManager threadExample = new DataManager(null); 
//        threadExample.setBoxes(FXCollections.observableArrayList());
//    //    JClassClassBox ThreadExample = new JClassClassBox();
//        ThreadExample.setLocationAndSize(0, 0, 10, 10);
//        ThreadExample.getClassName().setText("ThreadExample");
//        ThreadExample.setPackageName("Dummy");
//        //Thread example Attributes
//        ThreadExample.addParent("Application");
//        ClassAttributes thread = new ClassAttributes();
//        thread.setAttributeAccess("public");
//        thread.setAttributeName("START_TEXT");
//        thread.setIsStatic(true);
//        thread.setAttributeType("String");
//        ClassAttributes thread2 = new ClassAttributes();
//        thread2.setAttributeAccess("public");
//        thread2.setAttributeName("PAUSE_TEXT");
//        thread2.setIsStatic(true);
//        thread2.setAttributeType("String");
//        ClassAttributes thread3 = new ClassAttributes();
//        thread3.setAttributeAccess("private");
//        thread3.setAttributeName("window");
//        thread3.setAttributeType("String");
//        ClassAttributes thread4 = new ClassAttributes();
//        thread4.setAttributeAccess("private");
//        thread4.setAttributeName("appPane");
//        thread4.setAttributeType("FlowPane");
//        ClassAttributes thread5 = new ClassAttributes();
//        thread5.setAttributeAccess("private");
//        thread5.setAttributeName("startButton");
//        thread5.setAttributeType("Button");
//        ClassAttributes thread6 = new ClassAttributes();
//        thread6.setAttributeAccess("private");
//        thread6.setAttributeName("startButton");
//        thread6.setAttributeType("Button");
//        ClassAttributes thread7 = new ClassAttributes();
//        thread7.setAttributeAccess("private");
//        thread7.setAttributeName("scrollPane");
//        thread7.setAttributeType("ScrollPane");
//        ClassAttributes thread8 = new ClassAttributes();
//        thread8.setAttributeAccess("private");
//        thread8.setAttributeName("dateThread");
//        thread8.setAttributeType("Thread");
//        ClassAttributes thread9 = new ClassAttributes();
//        thread9.setAttributeAccess("private");
//        thread9.setAttributeName("dateTask");
//        thread9.setAttributeType("Task");
//        ClassAttributes thread10 = new ClassAttributes();
//        thread10.setAttributeAccess("private");
//        thread10.setAttributeName("counterThread");
//        thread10.setAttributeType("Thread");
//        ClassAttributes thread11 = new ClassAttributes();
//        thread11.setAttributeAccess("private");
//        thread11.setAttributeName("counterTask");
//        thread11.setAttributeType("Task");
//        ClassAttributes thread12 = new ClassAttributes();
//        thread12.setAttributeAccess("private");
//        thread12.setAttributeName("work");
//        thread12.setAttributeType("boolean");
////        ThreadExample.addAttribute(thread);
////        ThreadExample.addAttribute(thread2);
////        ThreadExample.addAttribute(thread3);
////        ThreadExample.addAttribute(thread4);
////        ThreadExample.addAttribute(thread5);
////        ThreadExample.addAttribute(thread6);
////        ThreadExample.addAttribute(thread7);
////        ThreadExample.addAttribute(thread8);
////        ThreadExample.addAttribute(thread9);
////        ThreadExample.addAttribute(thread10);
////        ThreadExample.addAttribute(thread11);
////        ThreadExample.addAttribute(thread12);
////        
////        
////        
////        
////        
////        // Methods
////        
////        ClassMethods m1 = new ClassMethods();
////        m1.setMethodAccess("public");
////        m1.setMethodReturn("void");
////   //     m1.setArg("Stage");
////        m1.setMethodName("start");
////        m1.setIsException(true);
////        ClassMethods m2 = new ClassMethods();
////        m2.setMethodAccess("public");
////        m2.setMethodReturn("void");
////        m2.setMethodName("startWork");
////        ClassMethods m3 = new ClassMethods();
////        m3.setMethodAccess("public");
////        m3.setMethodReturn("void");
////        m3.setMethodName("pauseWork");
////        ClassMethods m4 = new ClassMethods();
////        m4.setMethodAccess("public");
////        m4.setMethodReturn("boolean");
////        m4.setMethodName("doWork");
////        ClassMethods m5 = new ClassMethods();
////        m5.setMethodAccess("public");
////        m5.setMethodReturn("void");
////        m5.setMethodName("appendText");
////     //   m5.setArg("String");
//        ClassMethods m6 = new ClassMethods();
//        m6.setMethodAccess("public");
//        m6.setMethodReturn("void");
//        m6.setMethodName("sleep");
//    //    m6.setArg("int");
//        ClassMethods m7 = new ClassMethods();
//        m7.setMethodAccess("public");
//        m7.setMethodReturn("void");
//        m7.setMethodName("initLayout");
//     
//        ClassMethods m8 = new ClassMethods();
//        m8.setMethodAccess("public");
//        m8.setMethodReturn("void");
//        m8.setMethodName("initHandlers");
//        ClassMethods m9 = new ClassMethods();
//        m9.setMethodAccess("public");
//        m9.setMethodReturn("void");
//        m9.setMethodName("initWindow");
//        ClassMethods m10 = new ClassMethods();
//        m10.setMethodAccess("public");
//        m10.setMethodReturn("void");
//        m10.setMethodName("initWindow");
//      //  m10.setArg("Stage");
//        ClassMethods m11 = new ClassMethods();
//        m11.setMethodAccess("public");
//        m11.setMethodReturn("void");
//        m11.setMethodName("startWork");
//        ClassMethods m12 = new ClassMethods();
//        m12.setMethodAccess("public");
//        m12.setMethodReturn("void");
//        m12.setMethodName("initThreads");
//        ClassMethods m13 = new ClassMethods();
//        m13.setMethodAccess("public");
//        m13.setMethodReturn("void");
//        m13.setMethodName("startWork");
//        m13.setIsStatic(true);
//       // m13.setArg("String[]");
////        
////        ThreadExample.addMethods(m1);
////        ThreadExample.addMethods(m2);
////        ThreadExample.addMethods(m3);
////        ThreadExample.addMethods(m4);
////        ThreadExample.addMethods(m5);
////        ThreadExample.addMethods(m6);
////        ThreadExample.addMethods(m7);
////        ThreadExample.addMethods(m8);
////        ThreadExample.addMethods(m9);
////        ThreadExample.addMethods(m10);
////        ThreadExample.addMethods(m11);
////        ThreadExample.addMethods(m12);
////        ThreadExample.addMethods(m13);
////        
////        //Start Handler
////        
////        JClassClassBox StartHandler = new JClassClassBox();
////        StartHandler.setLocationAndSize(200, 200, 30, 30);
////
////        StartHandler.getClassName().setText("StartHandler");
////        StartHandler.setPackageName("Dummy");
////        StartHandler.addParent("EventHandler");
////        ClassAttributes a1 = new ClassAttributes();
////        a1.setAttributeAccess("private");
////        a1.setAttributeType("ThreadExample");
////        a1.setAttributeName("app");
////        ClassMethods s1 = new ClassMethods();
////        s1.setMethodName("StartHandler");
////      //  s1.setArg("ThreadExample");
////        ClassMethods s2 = new ClassMethods();
////        s2.setMethodName("handle");
////       // s2.setArg("Event");
////        
////        StartHandler.addAttribute(a1);
////        StartHandler.addMethods(s1);
////        StartHandler.addMethods(s2);
////        
//        // Pause Handler
////        
////        JClassClassBox PauseHandler = new JClassClassBox();
////        PauseHandler.setLocationAndSize(300, 0, 40, 40);
////        PauseHandler.getClassName().setText("PauseHandler");
////        PauseHandler.setPackageName("Dummy");
////        StartHandler.addParent("PauseHandler");
//        ClassAttributes p1 = new ClassAttributes();
//        p1.setAttributeAccess("private");
//        p1.setAttributeType("ThreadExample");
//        p1.setAttributeName("app");
//        ClassMethods q1 = new ClassMethods();
//        q1.setMethodName("PauseHandler");
//      //  q1.setArg("ThreadExample");
//        ClassMethods y2 = new ClassMethods();
//        y2.setMethodName("handle");
//       // y2.setArg("Event");
//        
////        PauseHandler.addAttribute(p1);
////        PauseHandler.addMethods(q1);
////        PauseHandler.addMethods(y2);
////        // Date Task
////        
////        JClassClassBox DateTask = new JClassClassBox();
////        DateTask.setLocationAndSize(0, 400, 60, 60);
//        DateTask.getClassName().setText("DateTask");
//        DateTask.setPackageName("Dummy");
//        DateTask.addParent("Task<Void>");
//        ClassAttributes d1 = new ClassAttributes();
//        d1.setAttributeAccess("private");
//        d1.setAttributeType("ThreadExample");
//        d1.setAttributeName("app");
//        ClassAttributes d2 = new ClassAttributes();
//        d2.setAttributeAccess("private");
//        d2.setAttributeType("Date");
//        d2.setAttributeName("now");
//        ClassMethods d3 = new ClassMethods();
//      //  d3.setArg("ThreadExample");
//        d3.setMethodAccess("public");
//        d3.setMethodName("DateTask");
//        ClassMethods d4 = new ClassMethods();
//        d4.setMethodName("call");
//        d4.setIsException(true);
//        d4.setMethodAccess("protected");
//        d4.setMethodReturn("Void");
//        
//        DateTask.addAttribute(d1);
//        DateTask.addAttribute(d2);
//        DateTask.addMethods(d3);
//        DateTask.addMethods(d4);
//        
//        //COunter Example
//        JClassClassBox CounterTask = new JClassClassBox();
//        CounterTask.setLocationAndSize(250, 250, 100, 100);
//        CounterTask.setLayoutX(150);
//        CounterTask.setLayoutY(150);
//        CounterTask.getClassName().setText("CounterTask");
//        CounterTask.setPackageName("Dummy");
//        DateTask.addParent("Task<Void>");
//        ClassAttributes la1 = new ClassAttributes();
//        la1.setAttributeAccess("private");
//        la1.setAttributeType("ThreadExample");
//        la1.setAttributeName("app");
//        ClassAttributes la2 = new ClassAttributes();
//        la2.setAttributeAccess("private");
//        la2.setAttributeType("int");
//        la2.setAttributeName("counter");
//        ClassMethods la3 = new ClassMethods();
//     //   la3.setArg("ThreadExample");
//        la3.setMethodAccess("public");
//        la3.setMethodName("CounterTask");
//        ClassMethods la4 = new ClassMethods();
//        la4.setMethodName("call");
//        la4.setIsException(true);
//        la4.setMethodAccess("protected");
//        la4.setMethodReturn("Void");
//        
//        CounterTask.addAttribute(la1);
//        CounterTask.addAttribute(la2);
//        CounterTask.addMethods(la3);
//        CounterTask.addMethods(la4);
//        threadExample.getBoxes().addAll(ThreadExample,StartHandler, PauseHandler, DateTask, CounterTask
//                                        );
//        threadExample.setGrid(new CheckBox());
//        threadExample.getGrid().setSelected(false);
//        threadExample.setSnap(new CheckBox());
//        threadExample.getSnap().setSelected(false);
//        FileManager fileManager = new FileManager();
//	String currentDirectory;
//	File file = new File("");
//	currentDirectory = file.getAbsolutePath() + "/test/jc/file/Test1.";
//        //save 
//        fileManager.saveData(threadExample, currentDirectory);
//        //Load
//        currentDirectory = file.getAbsolutePath() + "/test/jc/file/Test1.json";
//        DataManager data = new DataManager(null);
//        data.setBoxes(FXCollections.observableArrayList());
//        
//        fileManager.loadData(data, currentDirectory);
//        System.out.println("TEST1");
//        System.out.println("Names");
//        
//        //test 2 names
//        Node one = threadExample.getBoxes().get(0);
//        Node one_2 = data.getBoxes().get(0);
//        if(one instanceof JClassClassBox){
//            JClassClassBox test = (JClassClassBox)one;
//            JClassClassBox test2 = (JClassClassBox)one_2;
//            assertEquals("ThreadExample", test.getName(), test2.getName());
//            if(test.getName().equals(test2.getName())){
//                System.out.println(test.getName() + "=" + test2.getName());
//            }
//           
//        }
//        else{
//            
//            JClassInterfaceBox test = (JClassInterfaceBox)one;
//            JClassInterfaceBox test2 = (JClassInterfaceBox)one_2;
//            assertEquals("ThreadExample", test.getName(), test2.getName());
//            if(test.getName().equals(test2.getName())){
//                System.out.println(test.getName() + "=" + test2.getName());
//            }
//           
//        }
//        Node two = threadExample.getBoxes().get(2);
//        Node two_2 = data.getBoxes().get(2);
//        if(two instanceof JClassClassBox){
//            JClassClassBox test = (JClassClassBox)two;
//            JClassClassBox test2 = (JClassClassBox)two_2;
//            assertEquals(test.getName(), test.getName(), test2.getName());
//            if(test.getName().equals(test2.getName())){
//                System.out.println(test.getName() + "=" + test2.getName());
//            }
//           
//        }
//        else{
//            
//            JClassInterfaceBox test = (JClassInterfaceBox)two;
//            JClassInterfaceBox test2 = (JClassInterfaceBox)two_2;
//            assertEquals(test.getName(), test.getName(), test2.getName());
//            if(test.getName().equals(test2.getName())){
//                System.out.println(test.getName() + "=" + test2.getName());
//            }
//           
//        }
//        
//        System.out.println("package");
//        
//        //test 2 packages
//        Node three = threadExample.getBoxes().get(1);
//        Node three_2 = data.getBoxes().get(1);
//        if(three instanceof JClassClassBox){
//            JClassClassBox test = (JClassClassBox)three;
//            JClassClassBox test2 = (JClassClassBox)three_2;
//            assertEquals(test.getPackage(), test.getPackage(), test2.getPackage());
//            if(test.getPackage().equals(test2.getPackage())){
//                System.out.println(test.getPackage()+ "=" + test2.getPackage());
//            }
//           
//        }
//        else{
//            
//            JClassInterfaceBox test = (JClassInterfaceBox)three;
//            JClassInterfaceBox test2 = (JClassInterfaceBox)three_2;
//            assertEquals(test.getPackage(), test.getPackage(), test2.getPackage());
//            if(test.getName().equals(test2.getName())){
//                System.out.println(test.getPackage()+ "=" + test2.getPackage());
//            }
//           
//        }
//        Node four = threadExample.getBoxes().get(3);
//        Node four_2 = data.getBoxes().get(3);
//        if(four instanceof JClassClassBox){
//            JClassClassBox test = (JClassClassBox)four;
//            JClassClassBox test2 = (JClassClassBox)four_2;
//            assertEquals(test.getPackage(), test.getPackage(), test2.getPackage());
//            if(test.getPackage().equals(test2.getPackage())){
//                System.out.println(test.getPackage()+ "=" + test2.getPackage());
//            }
//           
//        }
//        else{
//            
//            JClassInterfaceBox test = (JClassInterfaceBox)four;
//            JClassInterfaceBox test2 = (JClassInterfaceBox)four_2;
//            assertEquals(test.getPackage(), test.getPackage(), test2.getPackage());
//            if(test.getPackage().equals(test2.getPackage())){
//                System.out.println(test.getPackage()+ "=" + test2.getPackage());
//            }
//           
//        }
//        
//        System.out.println("X location");
//                
//      
//      
//        //test2 x location
//        Node five = threadExample.getBoxes().get(0);
//        Node five_2 = data.getBoxes().get(0);
//        if(five instanceof JClassClassBox){
//            JClassClassBox test = (JClassClassBox)five;
//            JClassClassBox test2 = (JClassClassBox)five_2;
//            assertEquals(test.getX(), test.getX(), test2.getX());
//            if(test.getX() == test2.getX()){
//                System.out.println(test.getX()+ "=" + test2.getX());
//            }
//           
//        }
//        else{
//            
//            JClassInterfaceBox test = (JClassInterfaceBox)five;
//            JClassInterfaceBox test2 = (JClassInterfaceBox)five_2;
//            assertEquals(test.getX(), test.getX(), test2.getX());
//            if(test.getX() == test2.getX()){
//                System.out.println(test.getX()+ "=" + test2.getX());
//            }
//           
//        }
//        Node six = threadExample.getBoxes().get(1);
//        Node six_2 = data.getBoxes().get(1);
//        if(six instanceof JClassClassBox){
//            JClassClassBox test = (JClassClassBox)six;
//            JClassClassBox test2 = (JClassClassBox)six_2;
//            assertEquals(test.getX(), test.getX(), test2.getX());
//            if(test.getX() == test2.getX()){
//                System.out.println(test.getX()+ "=" + test2.getX());
//            }
//           
//        }
//        else{
//            
//            JClassInterfaceBox test = (JClassInterfaceBox)six;
//            JClassInterfaceBox test2 = (JClassInterfaceBox)six_2;
//            assertEquals(test.getX(), test.getX(), test2.getX());
//            if(test.getX() == test2.getX()){
//                System.out.println(test.getX()+ "=" + test2.getX());
//            }
//           
//        }
//      
//        System.out.println("Test arg and parent");
//                
//        //test2 arguemnt / parent
//        Node seven = threadExample.getBoxes().get(0);
//        Node seven_2 = data.getBoxes().get(0);
//        if(seven instanceof JClassClassBox){
//            JClassClassBox test = (JClassClassBox)seven;
//            JClassClassBox test2 = (JClassClassBox)seven_2;
//            ObservableList<ClassMethods> ats = test.getClassMethods();
//            ObservableList<ClassMethods> ats2 = test2.getClassMethods();
//            ArrayList<String> arg1 = ats.get(0).getArguments();
//            ArrayList<String> arg2  = ats2.get(0).getArguments();
//            String argg1 = arg1.get(0);
//            String argg2 = arg1.get(0);
//
//            assertEquals(argg1, argg1, argg2);
//            if(argg1.equals(argg2)){
//                System.out.println(argg1+ "=" + argg2);
//            }
//           
//        }
//        else{
//            
//            JClassInterfaceBox test = (JClassInterfaceBox)seven;
//            JClassInterfaceBox test2 = (JClassInterfaceBox)seven_2;
//            ObservableList<InterfaceMethods> ats = test.getInterfaceMethods();
//            ObservableList<InterfaceMethods> ats2 = test2.getInterfaceMethods();
//         //   String arg1 = ats.get(0).getArg();
//          //  String arg2 = ats2.get(0).getArg();            
//            
////            assertEquals(arg1, arg1, arg2);
//        //    if(arg1.equals(arg2)){
//          //      System.out.println(arg1+ "=" + arg2);
//          //  }
//           
//        }
//        Node eight = threadExample.getBoxes().get(0);
//        Node eight_2 = data.getBoxes().get(0);
//        if(eight instanceof JClassClassBox){
//            JClassClassBox test = (JClassClassBox)eight;
//            JClassClassBox test2 = (JClassClassBox)eight_2;
//            ObservableList<ClassMethods> ats = test.getClassMethods();
//            ObservableList<ClassMethods> ats2 = test2.getClassMethods();
//         //   String arg1 = ats.get(0).getArg();
//         //   String arg2 = ats2.get(0).getArg();
//            ArrayList<String> parents = test.getBoxParent();
//            ArrayList<String> parents2 = test2.getBoxParent();
//            String yup = parents.get(0);
//            String yup2 = parents2.get(0);
//            
//            
//            assertEquals(yup, yup, yup2);
//            if(yup.equals(yup2)){
//                System.out.println(yup+ "=" + yup2);
//            }
//           
//        }
//        else{
//            
//            JClassInterfaceBox test = (JClassInterfaceBox)eight;
//            JClassInterfaceBox test2 = (JClassInterfaceBox)eight_2;
//            ObservableList<InterfaceMethods> ats = test.getInterfaceMethods();
//            ObservableList<InterfaceMethods> ats2 = test2.getInterfaceMethods();
//        //    String arg1 = ats.get(0).getArg();
//        //    String arg2 = ats2.get(0).getArg();
//         //   String int1 = test.getBoxParent();
//            String int2 = test2.getBoxParent();
//            
//        //    assertEquals(int1, int1, int2);
//          //  if(int1.equals(int2)){
//          //      System.out.println(int1+ "=" + int2);
//          //  }
//           
//        }
//        System.out.println("Test 2 args");
//        //test 2 attributes
//        
//        Node nine = threadExample.getBoxes().get(0);
//        Node nine_2 = data.getBoxes().get(0);
//        if(nine instanceof JClassClassBox){
//            JClassClassBox test = (JClassClassBox)nine;
//            JClassClassBox test2 = (JClassClassBox)nine_2;
//            ObservableList<ClassAttributes> ats = test.getClassAttributes();
//            ObservableList<ClassAttributes> ats2 = test2.getClassAttributes();
//            String yes10 = ats.get(0).getAttributeName();
//            String yes11 = ats2.get(0).getAttributeName();
//            
//            assertEquals(yes10, yes10, yes11);
//            if(yes10.equals(yes11)){
//                System.out.println(yes10+ "=" + yes11);
//            }
//           
//        }
//        else{
//            
//            JClassInterfaceBox test = (JClassInterfaceBox)nine;
//            JClassInterfaceBox test2 = (JClassInterfaceBox)nine_2;
//            ObservableList<InterfaceAttributes> ats = test.getInterfaceAttributes();
//            ObservableList<InterfaceAttributes> ats2 = test2.getInterfaceAttributes();
//            String yes10 = ats.get(0).getAttributeName();
//            String yes11 = ats2.get(0).getAttributeName();
//            
//            assertEquals(yes10, yes10, yes11);
//            if(yes10.equals(yes11)){
//                System.out.println(yes10+ "=" + yes11);
//            }
//           
//        }
//                
//        Node ten = threadExample.getBoxes().get(0);
//        Node ten_2 = data.getBoxes().get(0);
//        if(ten instanceof JClassClassBox){
//            JClassClassBox test = (JClassClassBox)ten;
//            JClassClassBox test2 = (JClassClassBox)ten_2;
//            ObservableList<ClassAttributes> ats = test.getClassAttributes();
//            ObservableList<ClassAttributes> ats2 = test2.getClassAttributes();
//            String yes10 = ats.get(0).getAttributeAccess();
//            String yes11 = ats2.get(0).getAttributeAccess();
//            
//            assertEquals(yes10, yes10, yes11);
//            if(yes10.equals(yes11)){
//                System.out.println(yes10+ "=" + yes11);
//            }
//           
//        }
//        else{
//            
//            JClassInterfaceBox test = (JClassInterfaceBox)ten;
//            JClassInterfaceBox test2 = (JClassInterfaceBox)ten_2;
//            ObservableList<InterfaceAttributes> ats = test.getInterfaceAttributes();
//            ObservableList<InterfaceAttributes> ats2 = test2.getInterfaceAttributes();
//            String yes10 = ats.get(0).getAttributeAccess();
//            String yes11 = ats2.get(0).getAttributeAccess();
//            
//            assertEquals(yes10, yes10, yes11);
//            if(yes10.equals(yes11)){
//                System.out.println(yes10+ "=" + yes11);
//            }
//           
//        }
//
//
//        
//        
//    }
//    @Test
//    public void Test2() throws Exception {
//    DataManager threadExample = new DataManager(null); 
//        threadExample.setBoxes(FXCollections.observableArrayList());
//        JClassClassBox ThreadExample = new JClassClassBox();
//        ThreadExample.setLocationAndSize(0, 0, 10, 10);
//        ThreadExample.getClassName().setText("ThreadExample");
//        ThreadExample.setPackageName("Dummy");
//        //Thread example Attributes
//        ThreadExample.addParent("Application");
//        ClassAttributes thread = new ClassAttributes();
//        thread.setAttributeAccess("public");
//        thread.setAttributeName("START_TEXT");
//        thread.setIsStatic(true);
//        thread.setAttributeType("String");
//        ClassAttributes thread2 = new ClassAttributes();
//        thread2.setAttributeAccess("public");
//        thread2.setAttributeName("PAUSE_TEXT");
//        thread2.setIsStatic(true);
//        thread2.setAttributeType("String");
//        ClassAttributes thread3 = new ClassAttributes();
//        thread3.setAttributeAccess("private");
//        thread3.setAttributeName("window");
//        thread3.setAttributeType("String");
//        ClassAttributes thread4 = new ClassAttributes();
//        thread4.setAttributeAccess("private");
//        thread4.setAttributeName("appPane");
//        thread4.setAttributeType("FlowPane");
//        ClassAttributes thread5 = new ClassAttributes();
//        thread5.setAttributeAccess("private");
//        thread5.setAttributeName("startButton");
//        thread5.setAttributeType("Button");
//        ClassAttributes thread6 = new ClassAttributes();
//        thread6.setAttributeAccess("private");
//        thread6.setAttributeName("startButton");
//        thread6.setAttributeType("Button");
//        ClassAttributes thread7 = new ClassAttributes();
//        thread7.setAttributeAccess("private");
//        thread7.setAttributeName("scrollPane");
//        thread7.setAttributeType("ScrollPane");
//        ClassAttributes thread8 = new ClassAttributes();
//        thread8.setAttributeAccess("private");
//        thread8.setAttributeName("dateThread");
//        thread8.setAttributeType("Thread");
//        ClassAttributes thread9 = new ClassAttributes();
//        thread9.setAttributeAccess("private");
//        thread9.setAttributeName("dateTask");
//        thread9.setAttributeType("Task");
//        ClassAttributes thread10 = new ClassAttributes();
//        thread10.setAttributeAccess("private");
//        thread10.setAttributeName("counterThread");
//        thread10.setAttributeType("Thread");
//        ClassAttributes thread11 = new ClassAttributes();
//        thread11.setAttributeAccess("private");
//        thread11.setAttributeName("counterTask");
//        thread11.setAttributeType("Task");
//        ClassAttributes thread12 = new ClassAttributes();
//        thread12.setAttributeAccess("private");
//        thread12.setAttributeName("work");
//        thread12.setAttributeType("boolean");
//        ThreadExample.addAttribute(thread);
//        ThreadExample.addAttribute(thread2);
//        ThreadExample.addAttribute(thread3);
//        ThreadExample.addAttribute(thread4);
//        ThreadExample.addAttribute(thread5);
//        ThreadExample.addAttribute(thread6);
//        ThreadExample.addAttribute(thread7);
//        ThreadExample.addAttribute(thread8);
//        ThreadExample.addAttribute(thread9);
//        ThreadExample.addAttribute(thread10);
//        ThreadExample.addAttribute(thread11);
//        ThreadExample.addAttribute(thread12);
//        
//        
//        
//        
//        
//        // Methods
//        
//        ClassMethods m1 = new ClassMethods();
//        m1.setMethodAccess("public");
//        m1.setMethodReturn("void");
//      //  m1.setArg("Stage");
//        m1.setMethodName("start");
//        m1.setIsException(true);
//        ClassMethods m2 = new ClassMethods();
//        m2.setMethodAccess("public");
//        m2.setMethodReturn("void");
//        m2.setMethodName("startWork");
//        ClassMethods m3 = new ClassMethods();
//        m3.setMethodAccess("public");
//        m3.setMethodReturn("void");
//        m3.setMethodName("pauseWork");
//        ClassMethods m4 = new ClassMethods();
//        m4.setMethodAccess("public");
//        m4.setMethodReturn("boolean");
//        m4.setMethodName("doWork");
//        ClassMethods m5 = new ClassMethods();
//        m5.setMethodAccess("public");
//        m5.setMethodReturn("void");
//        m5.setMethodName("appendText");
//      //  m5.setArg("String");
//        ClassMethods m6 = new ClassMethods();
//        m6.setMethodAccess("public");
//        m6.setMethodReturn("void");
//        m6.setMethodName("sleep");
//       // m6.setArg("int");
//        ClassMethods m7 = new ClassMethods();
//        m7.setMethodAccess("public");
//        m7.setMethodReturn("void");
//        m7.setMethodName("initLayout");
//     
//        ClassMethods m8 = new ClassMethods();
//        m8.setMethodAccess("public");
//        m8.setMethodReturn("void");
//        m8.setMethodName("initHandlers");
//        ClassMethods m9 = new ClassMethods();
//        m9.setMethodAccess("public");
//        m9.setMethodReturn("void");
//        m9.setMethodName("initWindow");
//        ClassMethods m10 = new ClassMethods();
//        m10.setMethodAccess("public");
//        m10.setMethodReturn("void");
//        m10.setMethodName("initWindow");
//       // m10.setArg("Stage");
//        ClassMethods m11 = new ClassMethods();
//        m11.setMethodAccess("public");
//        m11.setMethodReturn("void");
//        m11.setMethodName("startWork");
//        ClassMethods m12 = new ClassMethods();
//        m12.setMethodAccess("public");
//        m12.setMethodReturn("void");
//        m12.setMethodName("initThreads");
//        ClassMethods m13 = new ClassMethods();
//        m13.setMethodAccess("public");
//        m13.setMethodReturn("void");
//        m13.setMethodName("startWork");
//        m13.setIsStatic(true);
//      //  m13.setArg("String[]");
//        
//        ThreadExample.addMethods(m1);
//        ThreadExample.addMethods(m2);
//        ThreadExample.addMethods(m3);
//        ThreadExample.addMethods(m4);
//        ThreadExample.addMethods(m5);
//        ThreadExample.addMethods(m6);
//        ThreadExample.addMethods(m7);
//        ThreadExample.addMethods(m8);
//        ThreadExample.addMethods(m9);
//        ThreadExample.addMethods(m10);
//        ThreadExample.addMethods(m11);
//        ThreadExample.addMethods(m12);
//        ThreadExample.addMethods(m13);
//        
//        //Start Handler
//        
//        JClassClassBox StartHandler = new JClassClassBox();
//        StartHandler.setLocationAndSize(200, 200, 30, 30);
//
//        StartHandler.getClassName().setText("StartHandler");
//        StartHandler.setPackageName("Dummy");
//        StartHandler.addParent("EventHandler");
//        ClassAttributes a1 = new ClassAttributes();
//        a1.setAttributeAccess("private");
//        a1.setAttributeType("ThreadExample");
//        a1.setAttributeName("app");
//        ClassMethods s1 = new ClassMethods();
//        s1.setMethodName("StartHandler");
//      //  s1.setArg("ThreadExample");
//        ClassMethods s2 = new ClassMethods();
//        s2.setMethodName("handle");
//      //  s2.setArg("Event");
//        
//        StartHandler.addAttribute(a1);
//        StartHandler.addMethods(s1);
//        StartHandler.addMethods(s2);
//        
//        // Pause Handler
//        
//        JClassClassBox PauseHandler = new JClassClassBox();
//        PauseHandler.setLocationAndSize(300, 0, 40, 40);
//        PauseHandler.getClassName().setText("PauseHandler");
//        PauseHandler.setPackageName("Dummy");
//        StartHandler.addParent("PauseHandler");
//        ClassAttributes p1 = new ClassAttributes();
//        p1.setAttributeAccess("private");
//        p1.setAttributeType("ThreadExample");
//        p1.setAttributeName("app");
//        ClassMethods q1 = new ClassMethods();
//        q1.setMethodName("PauseHandler");
//      //  q1.setArg("ThreadExample");
//        ClassMethods y2 = new ClassMethods();
//        y2.setMethodName("handle");
//       // y2.setArg("Event");
//        
//        PauseHandler.addAttribute(p1);
//        PauseHandler.addMethods(q1);
//        PauseHandler.addMethods(y2);
//        // Date Task
//        
//        JClassClassBox DateTask = new JClassClassBox();
//        DateTask.setLocationAndSize(0, 400, 60, 60);
//        DateTask.getClassName().setText("DateTask");
//        DateTask.setPackageName("Dummy");
//        DateTask.addParent("Task<Void>");
//        ClassAttributes d1 = new ClassAttributes();
//        d1.setAttributeAccess("private");
//        d1.setAttributeType("ThreadExample");
//        d1.setAttributeName("app");
//        ClassAttributes d2 = new ClassAttributes();
//        d2.setAttributeAccess("private");
//        d2.setAttributeType("Date");
//        d2.setAttributeName("now");
//        ClassMethods d3 = new ClassMethods();
//      //  d3.setArg("ThreadExample");
//        d3.setMethodAccess("public");
//        d3.setMethodName("DateTask");
//        ClassMethods d4 = new ClassMethods();
//        d4.setMethodName("call");
//        d4.setIsException(true);
//        d4.setMethodAccess("protected");
//        d4.setMethodReturn("Void");
//        
//        DateTask.addAttribute(d1);
//        DateTask.addAttribute(d2);
//        DateTask.addMethods(d3);
//        DateTask.addMethods(d4);
//
//
//        // Application
//        
//        JClassClassBox abs = new JClassClassBox();
//        abs.setLocationAndSize(300, 0, 40, 40);
//        abs.setPackageName("javafx.application");
//        abs.getClassName().setText("Application");
//        abs.putAbstractLabel();
//        threadExample.getBoxes().addAll(ThreadExample,StartHandler, PauseHandler, DateTask, abs);
//        threadExample.setGrid(new CheckBox());
//        threadExample.getGrid().setSelected(false);
//        threadExample.setSnap(new CheckBox());
//        threadExample.getSnap().setSelected(false);
//        FileManager fileManager = new FileManager();
//        
//	String currentDirectory;
//	File file = new File("");
//	currentDirectory = file.getAbsolutePath() + "/test/jc/file/Test2.";
//        //save 
//         DataManager data = new DataManager(null);
//        data.setBoxes(FXCollections.observableArrayList());
//        
//        fileManager.saveData(threadExample, currentDirectory);
//        currentDirectory = file.getAbsolutePath() + "/test/jc/file/Test2.json";
//        fileManager.loadData(data, currentDirectory);
//        //load
//        System.out.println();
//        System.out.println();
//        System.out.println();
//        System.out.println("TEST2");
//        System.out.println("Type and Name");
//        
//        //test 2 names
//        Node one = threadExample.getBoxes().get(3);
//        Node one_2 = data.getBoxes().get(3);
//        if(one instanceof JClassClassBox){
//            JClassClassBox test = (JClassClassBox)one;
//            JClassClassBox test2 = (JClassClassBox)one_2;
//            assertEquals(test.getBoxType(), test.getBoxType(), test2.getBoxType());
//            if(test.getBoxType().equals(test2.getBoxType())){
//                System.out.println(test.getBoxType() + "=" + test2.getBoxType());
//            }
//           
//        }
//        else{
//            
//            JClassInterfaceBox test = (JClassInterfaceBox)one;
//            JClassInterfaceBox test2 = (JClassInterfaceBox)one_2;
//            assertEquals(test.getBoxType(), test.getBoxType(), test2.getBoxType());
//            if(test.getBoxType().equals(test2.getBoxType())){
//                System.out.println(test.getBoxType() + "=" + test2.getBoxType());
//            }
//           
//        }
//        Node two = threadExample.getBoxes().get(4);
//        Node two_2 = data.getBoxes().get(4);
//        if(two instanceof JClassClassBox){
//            JClassClassBox test = (JClassClassBox)two;
//            JClassClassBox test2 = (JClassClassBox)two_2;
//            assertEquals(test.getName(), test.getName(), test2.getName());
//            if(test.getName().equals(test2.getName())){
//                System.out.println(test.getName() + "=" + test2.getName());
//            }
//           
//        }
//        else{
//            
//            JClassInterfaceBox test = (JClassInterfaceBox)two;
//            JClassInterfaceBox test2 = (JClassInterfaceBox)two_2;
//            assertEquals(test.getName(), test.getName(), test2.getName());
//            if(test.getName().equals(test2.getName())){
//                System.out.println(test.getName() + "=" + test2.getName());
//            }
//           
//        }
//        
//        System.out.println("package");
//        
//        //test 2 packages
//        Node three = threadExample.getBoxes().get(4);
//        Node three_2 = data.getBoxes().get(4);
//        if(three instanceof JClassClassBox){
//            JClassClassBox test = (JClassClassBox)three;
//            JClassClassBox test2 = (JClassClassBox)three_2;
//            assertEquals(test.getPackage(), test.getPackage(), test2.getPackage());
//            if(test.getPackage().equals(test2.getPackage())){
//                System.out.println(test.getPackage()+ "=" + test2.getPackage());
//            }
//           
//        }
//        else{
//            
//            JClassInterfaceBox test = (JClassInterfaceBox)three;
//            JClassInterfaceBox test2 = (JClassInterfaceBox)three_2;
//            assertEquals(test.getPackage(), test.getPackage(), test2.getPackage());
//            if(test.getName().equals(test2.getName())){
//                System.out.println(test.getPackage()+ "=" + test2.getPackage());
//            }
//           
//        }
//        Node four = threadExample.getBoxes().get(0);
//        Node four_2 = data.getBoxes().get(0);
//        if(four instanceof JClassClassBox){
//            JClassClassBox test = (JClassClassBox)four;
//            JClassClassBox test2 = (JClassClassBox)four_2;
//            assertEquals(test.getPackage(), test.getPackage(), test2.getPackage());
//            if(test.getPackage().equals(test2.getPackage())){
//                System.out.println(test.getPackage()+ "=" + test2.getPackage());
//            }
//           
//        }
//        else{
//            
//            JClassInterfaceBox test = (JClassInterfaceBox)four;
//            JClassInterfaceBox test2 = (JClassInterfaceBox)four_2;
//            assertEquals(test.getPackage(), test.getPackage(), test2.getPackage());
//            if(test.getPackage().equals(test2.getPackage())){
//                System.out.println(test.getPackage()+ "=" + test2.getPackage());
//            }
//           
//        }
//        
//        System.out.println("Abstract");
//                
//      
//      
//        //test2 x location
//        Node five = threadExample.getBoxes().get(threadExample.getBoxes().size() -1 );
//        Node five_2 = data.getBoxes().get(data.getBoxes().size() -1 );
//        if(five instanceof JClassClassBox){
//            JClassClassBox test = (JClassClassBox)five;
//            JClassClassBox test2 = (JClassClassBox)five_2;
//            String ab1 = String.valueOf(test.isAbstract());
//            String ab2 = String.valueOf(test2.isAbstract());
//            assertEquals(ab1, ab1, ab2);
//            if(ab1 == ab2){
//                System.out.println(ab1+ "=" + ab2);
//            }
//           
//        }
//        Node six = threadExample.getBoxes().get(1);
//        Node six_2 = data.getBoxes().get(1);
//        if(six instanceof JClassClassBox){
//            JClassClassBox test = (JClassClassBox)six;
//            JClassClassBox test2 = (JClassClassBox)six_2;
//            assertEquals(test.getX(), test.getX(), test2.getX());
//            if(test.getX() == test2.getX()){
//                System.out.println(test.getX()+ "=" + test2.getX());
//            }
//           
//        }
//        else{
//            
//            JClassInterfaceBox test = (JClassInterfaceBox)six;
//            JClassInterfaceBox test2 = (JClassInterfaceBox)six_2;
//            assertEquals(test.getX(), test.getX(), test2.getX());
//            if(test.getX() == test2.getX()){
//                System.out.println(test.getX()+ "=" + test2.getX());
//            }
//           
//        }
//      
//        System.out.println("Test attribute and method");
//                
//        //test2 arguemnt / parent
//        Node seven = threadExample.getBoxes().get(0);
//        Node seven_2 = data.getBoxes().get(0);
//        if(seven instanceof JClassClassBox){
//            JClassClassBox test = (JClassClassBox)seven;
//            JClassClassBox test2 = (JClassClassBox)seven_2;
//            ObservableList<ClassMethods> ats = test.getClassMethods();
//            ObservableList<ClassMethods> ats2 = test2.getClassMethods();
//            ArrayList<String> arg1 = ats.get(ats.size() - 1).getArguments();
//            ArrayList<String> arg2  = ats2.get(ats.size() - 1).getArguments();
//            String argg1 = arg1.get(0);
//            String argg2 = arg1.get(0);
//
//            assertEquals(argg1, argg1, argg2);
//            if(argg1.equals(argg2)){
//                System.out.println(argg1+ "=" + argg2);
//            }
//           
//        }
//        else{
//            
//            JClassInterfaceBox test = (JClassInterfaceBox)seven;
//            JClassInterfaceBox test2 = (JClassInterfaceBox)seven_2;
//            ObservableList<InterfaceMethods> ats = test.getInterfaceMethods();
//            ObservableList<InterfaceMethods> ats2 = test2.getInterfaceMethods();
//           // String arg1 = ats.get(0).getArg();
//           // String arg2 = ats2.get(0).getArg();            
//            
////            assertEquals(arg1, arg1, arg2);
//        //    if(arg1.equals(arg2)){
//         //       System.out.println(arg1+ "=" + arg2);
//         //   }
//           
//        }
//        Node eight = threadExample.getBoxes().get(0);
//        Node eight_2 = data.getBoxes().get(0);
//        if(eight instanceof JClassClassBox){
//            JClassClassBox test = (JClassClassBox)eight;
//            JClassClassBox test2 = (JClassClassBox)eight_2;
//            ObservableList<ClassMethods> ats = test.getClassMethods();
//            ObservableList<ClassMethods> ats2 = test2.getClassMethods();
//        //    String arg1 = ats.get(0).getArg();
//        //    String arg2 = ats2.get(0).getArg();
//            ArrayList<String> parents = test.getBoxParent();
//            ArrayList<String> parents2 = test2.getBoxParent();
//            String yup = parents.get(0);
//            String yup2 = parents2.get(0);
//            
//            
//            assertEquals(yup, yup, yup2);
//            if(yup.equals(yup2)){
//                System.out.println(yup+ "=" + yup2);
//            }
//           
//        }
//        else{
//            
//            JClassInterfaceBox test = (JClassInterfaceBox)eight;
//            JClassInterfaceBox test2 = (JClassInterfaceBox)eight_2;
//            ObservableList<InterfaceMethods> ats = test.getInterfaceMethods();
//            ObservableList<InterfaceMethods> ats2 = test2.getInterfaceMethods();
////            String arg1 = ats.get(0).getArg();
//         //   String arg2 = ats2.get(0).getArg();
//         //   String int1 = test.getBoxParent();
//            String int2 = test2.getBoxParent();
//            
//           // assertEquals(int1, int1, int2);
//           // if(int1.equals(int2)){
//           //     System.out.println(int1+ "=" + int2);
//           // }
//           
//        }
//        System.out.println("Test 2 args");
//        //test 2 attributes
//        
//        Node nine = threadExample.getBoxes().get(3);
//        Node nine_2 = data.getBoxes().get(3);
//        if(nine instanceof JClassClassBox){
//            JClassClassBox test = (JClassClassBox)nine;
//            JClassClassBox test2 = (JClassClassBox)nine_2;
//            ObservableList<ClassAttributes> ats = test.getClassAttributes();
//            ObservableList<ClassAttributes> ats2 = test2.getClassAttributes();
//            String yes10 = ats.get(0).getAttributeName();
//            String yes11 = ats2.get(0).getAttributeName();
//            
//            assertEquals(yes10, yes10, yes11);
//            if(yes10.equals(yes11)){
//                System.out.println(yes10+ "=" + yes11);
//            }
//           
//        }
//        else{
//            
//            JClassInterfaceBox test = (JClassInterfaceBox)nine;
//            JClassInterfaceBox test2 = (JClassInterfaceBox)nine_2;
//            ObservableList<InterfaceAttributes> ats = test.getInterfaceAttributes();
//            ObservableList<InterfaceAttributes> ats2 = test2.getInterfaceAttributes();
//            String yes10 = ats.get(0).getAttributeName();
//            String yes11 = ats2.get(0).getAttributeName();
//            
//            assertEquals(yes10, yes10, yes11);
//            if(yes10.equals(yes11)){
//                System.out.println(yes10+ "=" + yes11);
//            }
//           
//        }
//                
//        Node ten = threadExample.getBoxes().get(0);
//        Node ten_2 = data.getBoxes().get(0);
//        if(ten instanceof JClassClassBox){
//            JClassClassBox test = (JClassClassBox)ten;
//            JClassClassBox test2 = (JClassClassBox)ten_2;
//            ObservableList<ClassAttributes> ats = test.getClassAttributes();
//            ObservableList<ClassAttributes> ats2 = test2.getClassAttributes();
//            String yes10 = ats.get(0).getAttributeType();
//            String yes11 = ats2.get(0).getAttributeType();
//            
//            assertEquals(yes10, yes10, yes11);
//            if(yes10.equals(yes11)){
//                System.out.println(yes10+ "=" + yes11);
//            }
//           
//        }
//        else{
//            
//            JClassInterfaceBox test = (JClassInterfaceBox)ten;
//            JClassInterfaceBox test2 = (JClassInterfaceBox)ten_2;
//            ObservableList<InterfaceAttributes> ats = test.getInterfaceAttributes();
//            ObservableList<InterfaceAttributes> ats2 = test2.getInterfaceAttributes();
//            String yes10 = ats.get(0).getAttributeType();
//            String yes11 = ats2.get(0).getAttributeType();
//            
//            assertEquals(yes10, yes10, yes11);
//            if(yes10.equals(yes11)){
//                System.out.println(yes10+ "=" + yes11);
//            }
//           
//        }
//
//
//    }
//    @Test
//    public void Test3() throws Exception {
//        DataManager threadExample = new DataManager(null); 
//        threadExample = new DataManager(null);
//        threadExample.setBoxes(FXCollections.observableArrayList());
//        JClassClassBox ThreadExample = new JClassClassBox();
//        ThreadExample.setLocationAndSize(0, 0, 10, 10);
//        ThreadExample.getClassName().setText("ThreadExample");
//        ThreadExample.setPackageName("Dummy");
//        //Thread example Attributes
//        ThreadExample.addParent("Application");
//        ClassAttributes thread = new ClassAttributes();
//        thread.setAttributeAccess("public");
//        thread.setAttributeName("START_TEXT");
//        thread.setIsStatic(true);
//        thread.setAttributeType("String");
//        ClassAttributes thread2 = new ClassAttributes();
//        thread2.setAttributeAccess("public");
//        thread2.setAttributeName("PAUSE_TEXT");
//        thread2.setIsStatic(true);
//        thread2.setAttributeType("String");
//        ClassAttributes thread3 = new ClassAttributes();
//        thread3.setAttributeAccess("private");
//        thread3.setAttributeName("window");
//        thread3.setAttributeType("String");
//        ClassAttributes thread4 = new ClassAttributes();
//        thread4.setAttributeAccess("private");
//        thread4.setAttributeName("appPane");
//        thread4.setAttributeType("FlowPane");
//        ClassAttributes thread5 = new ClassAttributes();
//        thread5.setAttributeAccess("private");
//        thread5.setAttributeName("startButton");
//        thread5.setAttributeType("Button");
//        ClassAttributes thread6 = new ClassAttributes();
//        thread6.setAttributeAccess("private");
//        thread6.setAttributeName("startButton");
//        thread6.setAttributeType("Button");
//        ClassAttributes thread7 = new ClassAttributes();
//        thread7.setAttributeAccess("private");
//        thread7.setAttributeName("scrollPane");
//        thread7.setAttributeType("ScrollPane");
//        ClassAttributes thread8 = new ClassAttributes();
//        thread8.setAttributeAccess("private");
//        thread8.setAttributeName("dateThread");
//        thread8.setAttributeType("Thread");
//        ClassAttributes thread9 = new ClassAttributes();
//        thread9.setAttributeAccess("private");
//        thread9.setAttributeName("dateTask");
//        thread9.setAttributeType("Task");
//        ClassAttributes thread10 = new ClassAttributes();
//        thread10.setAttributeAccess("private");
//        thread10.setAttributeName("counterThread");
//        thread10.setAttributeType("Thread");
//        ClassAttributes thread11 = new ClassAttributes();
//        thread11.setAttributeAccess("private");
//        thread11.setAttributeName("counterTask");
//        thread11.setAttributeType("Task");
//        ClassAttributes thread12 = new ClassAttributes();
//        thread12.setAttributeAccess("private");
//        thread12.setAttributeName("work");
//        thread12.setAttributeType("boolean");
//        ThreadExample.addAttribute(thread);
//        ThreadExample.addAttribute(thread2);
//        ThreadExample.addAttribute(thread3);
//        ThreadExample.addAttribute(thread4);
//        ThreadExample.addAttribute(thread5);
//        ThreadExample.addAttribute(thread6);
//        ThreadExample.addAttribute(thread7);
//        ThreadExample.addAttribute(thread8);
//        ThreadExample.addAttribute(thread9);
//        ThreadExample.addAttribute(thread10);
//        ThreadExample.addAttribute(thread11);
//        ThreadExample.addAttribute(thread12);
//        
//        // Methods
//        
//        ClassMethods m1 = new ClassMethods();
//        m1.setMethodAccess("public");
//        m1.setMethodReturn("void");
//      //  m1.setArg("Stage");
//        m1.setMethodName("start");
//        m1.setIsException(true);
//        ClassMethods m2 = new ClassMethods();
//        m2.setMethodAccess("public");
//        m2.setMethodReturn("void");
//        m2.setMethodName("startWork");
//        ClassMethods m3 = new ClassMethods();
//        m3.setMethodAccess("public");
//        m3.setMethodReturn("void");
//        m3.setMethodName("pauseWork");
//        ClassMethods m4 = new ClassMethods();
//        m4.setMethodAccess("public");
//        m4.setMethodReturn("boolean");
//        m4.setMethodName("doWork");
//        ClassMethods m5 = new ClassMethods();
//        m5.setMethodAccess("public");
//        m5.setMethodReturn("void");
//        m5.setMethodName("appendText");
//      //  m5.setArg("String");
//        ClassMethods m6 = new ClassMethods();
//        m6.setMethodAccess("public");
//        m6.setMethodReturn("void");
//        m6.setMethodName("sleep");
//      //  m6.setArg("int");
//        ClassMethods m7 = new ClassMethods();
//        m7.setMethodAccess("public");
//        m7.setMethodReturn("void");
//        m7.setMethodName("initLayout");
//     
//        ClassMethods m8 = new ClassMethods();
//        m8.setMethodAccess("public");
//        m8.setMethodReturn("void");
//        m8.setMethodName("initHandlers");
//        ClassMethods m9 = new ClassMethods();
//        m9.setMethodAccess("public");
//        m9.setMethodReturn("void");
//        m9.setMethodName("initWindow");
//        ClassMethods m10 = new ClassMethods();
//        m10.setMethodAccess("public");
//        m10.setMethodReturn("void");
//        m10.setMethodName("initWindow");
//      //  m10.setArg("Stage");
//        ClassMethods m11 = new ClassMethods();
//        m11.setMethodAccess("public");
//        m11.setMethodReturn("void");
//        m11.setMethodName("startWork");
//        ClassMethods m12 = new ClassMethods();
//        m12.setMethodAccess("public");
//        m12.setMethodReturn("void");
//        m12.setMethodName("initThreads");
//        ClassMethods m13 = new ClassMethods();
//        m13.setMethodAccess("public");
//        m13.setMethodReturn("void");
//        m13.setMethodName("startWork");
//        m13.setIsStatic(true);
//      //  m13.setArg("String[]");
//        
//        ThreadExample.addMethods(m1);
//        ThreadExample.addMethods(m2);
//        ThreadExample.addMethods(m3);
//        ThreadExample.addMethods(m4);
//        ThreadExample.addMethods(m5);
//        ThreadExample.addMethods(m6);
//        ThreadExample.addMethods(m7);
//        ThreadExample.addMethods(m8);
//        ThreadExample.addMethods(m9);
//        ThreadExample.addMethods(m10);
//        ThreadExample.addMethods(m11);
//        ThreadExample.addMethods(m12);
//        ThreadExample.addMethods(m13);
//        
//        //Start Handler
//        
//        JClassClassBox StartHandler = new JClassClassBox();
//        StartHandler.setLocationAndSize(200, 200, 30, 30);
//
//        StartHandler.getClassName().setText("StartHandler");
//        StartHandler.setPackageName("Dummy");
//        StartHandler.addParent("EventHandler");
//        ClassAttributes a1 = new ClassAttributes();
//        a1.setAttributeAccess("private");
//        a1.setAttributeType("ThreadExample");
//        a1.setAttributeName("app");
//        ClassMethods s1 = new ClassMethods();
//        s1.setMethodName("StartHandler");
//       // s1.setArg("ThreadExample");
//        ClassMethods s2 = new ClassMethods();
//        s2.setMethodName("handle");
//       // s2.setArg("Event");
//        
//        StartHandler.addAttribute(a1);
//        StartHandler.addMethods(s1);
//        StartHandler.addMethods(s2);
//        
//        // Pause Handler
//        
//        JClassClassBox PauseHandler = new JClassClassBox();
//        PauseHandler.setLocationAndSize(300, 0, 40, 40);
//        PauseHandler.getClassName().setText("PauseHandler");
//        PauseHandler.setPackageName("Dummy");
//        StartHandler.addParent("PauseHandler");
//        ClassAttributes p1 = new ClassAttributes();
//        p1.setAttributeAccess("private");
//        p1.setAttributeType("ThreadExample");
//        p1.setAttributeName("app");
//        ClassMethods q1 = new ClassMethods();
//        q1.setMethodName("PauseHandler");
//       // q1.setArg("ThreadExample");
//        ClassMethods y2 = new ClassMethods();
//        y2.setMethodName("handle");
//       // y2.setArg("Event");
//        
//        PauseHandler.addAttribute(p1);
//        PauseHandler.addMethods(q1);
//        PauseHandler.addMethods(y2);
//                //EventHandler
//        JClassInterfaceBox EventHandler = new JClassInterfaceBox();
//        EventHandler.setLocationAndSize(700, 200, 250, 250);
//        EventHandler.getClassName().setText("EventHandler");
//        EventHandler.setPackageName("javafx.event");
//               // Task<Void>
//        
//        JClassInterfaceBox Task = new JClassInterfaceBox();
//        Task.setLocationAndSize(0, 100, 40, 40);
//        Task.getClassName().setText("Task<Void>");
//        EventHandler.setPackageName("javafx.concurrent");
//             
//        // Application
//        
//        JClassClassBox abs = new JClassClassBox();
//        abs.setLocationAndSize(300, 0, 40, 40);
//        abs.setPackageName("javafx.application");
//        abs.getClassName().setText("Application");
//        abs.putAbstractLabel();
//        
//        threadExample.getBoxes().addAll(ThreadExample,StartHandler, PauseHandler, EventHandler, Task 
//                                        );
//        //Lines
//        threadExample.makeLine(EventHandler, StartHandler.getName(), "IMPLEMENTS");
//        threadExample.setGrid(new CheckBox());
//        threadExample.getGrid().setSelected(false);
//        threadExample.setSnap(new CheckBox());
//        threadExample.getSnap().setSelected(false);
//        FileManager fileManager = new FileManager();
//	String currentDirectory;
//	File file = new File("");
//	currentDirectory = file.getAbsolutePath() + "/test/jc/file/Test3.";
//        //save 
//        DataManager data = new DataManager(null);
//        data.setBoxes(FXCollections.observableArrayList());
//        
//        fileManager.saveData(threadExample, currentDirectory);
//        currentDirectory = file.getAbsolutePath() + "/test/jc/file/Test3.json";
//        fileManager.loadData(data, currentDirectory);
//        //load
//        System.out.println();
//        System.out.println();
//        System.out.println();
//        System.out.println("TEST3");
//        System.out.println("lines");
//        
//        //test 2 lines
//        ObservableList<Node> line1 = threadExample.getBoxes();
//        ObservableList<Node> line2 = threadExample.getBoxes();
//        Node one = line1.get(0);
//        Node one_2 = line2.get(0);
//        if(one instanceof JLine){
//            JLine test = (JLine)one;
//            JLine test2 = (JLine)one_2;
//            assertEquals(test.getStartX(), test.getStartX(), test2.getStartX());
//            if(test.getStartX() == test2.getStartX()){
//                System.out.println(test.getStartX() + "=" + test2.getStartX());
//            }
//            assertEquals(test.getStartY(), test.getStartY(), test2.getStartY());
//            if(test.getStartY() == test2.getStartY()){
//                System.out.println(test.getStartY() + "=" + test2.getStartY());
//            }
//            assertEquals(test.getJParent().getName(), test.getJParent().getName(), test2.getJParent().getName());
//            if(test.getJParent().getName().equals(test2.getJParent().getName())){
//                System.out.println(test.getJParent().getName() + "=" + test2.getJParent().getName());
//                
//            }
//            assertEquals(test.getJChildren().getName(), test.getJChildren().getName(), test2.getJChildren().getName());
//            if(test.getJChildren().getName().equals(test2.getJChildren().getName())){
//                System.out.println(test.getJChildren().getName() + "=" + test2.getJChildren().getName());
//                
//            }
//            assertEquals(test.getType(), test.getType(), test2.getType());
//            if(test.getType().equals(test2.getType())){
//                System.out.println(test.getType() + "=" + test2.getType());
//                
//            }
//            
//           
//        }
//       
//        
//        System.out.println("package");
//        
//        //test 2 packages
//        Node three = threadExample.getBoxes().get(4);
//        Node three_2 = data.getBoxes().get(4);
//        if(three instanceof JClassClassBox){
//            JClassClassBox test = (JClassClassBox)three;
//            JClassClassBox test2 = (JClassClassBox)three_2;
//            assertEquals(test.getPackage(), test.getPackage(), test2.getPackage());
//            if(test.getPackage().equals(test2.getPackage())){
//                System.out.println(test.getPackage()+ "=" + test2.getPackage());
//            }
//           
//        }
//        else{
//            
//            JClassInterfaceBox test = (JClassInterfaceBox)three;
//            JClassInterfaceBox test2 = (JClassInterfaceBox)three_2;
//            assertEquals(test.getPackage(), test.getPackage(), test2.getPackage());
//            if(test.getName().equals(test2.getName())){
//                System.out.println(test.getPackage()+ "=" + test2.getPackage());
//            }
//           
//        }
//        Node four = threadExample.getBoxes().get(2);
//        Node four_2 = data.getBoxes().get(2);
//        if(four instanceof JClassClassBox){
//            JClassClassBox test = (JClassClassBox)four;
//            JClassClassBox test2 = (JClassClassBox)four_2;
//            assertEquals(test.getPackage(), test.getPackage(), test2.getPackage());
//            if(test.getPackage().equals(test2.getPackage())){
//                System.out.println(test.getPackage()+ "=" + test2.getPackage());
//            }
//           
//        }
//        else{
//            
//            JClassInterfaceBox test = (JClassInterfaceBox)four;
//            JClassInterfaceBox test2 = (JClassInterfaceBox)four_2;
//            assertEquals(test.getPackage(), test.getPackage(), test2.getPackage());
//            if(test.getPackage().equals(test2.getPackage())){
//                System.out.println(test.getPackage()+ "=" + test2.getPackage());
//            }
//           
//        }
//        Node two = threadExample.getBoxes().get(3);
//        Node two_2 = data.getBoxes().get(3);
//        if(four instanceof JClassClassBox){
//            JClassClassBox test = (JClassClassBox)four;
//            JClassClassBox test2 = (JClassClassBox)four_2;
//            assertEquals(test.getPackage(), test.getPackage(), test2.getPackage());
//            if(test.getPackage().equals(test2.getPackage())){
//                System.out.println(test.getPackage()+ "=" + test2.getPackage());
//            }
//           
//        }
//        else{
//            
//            JClassInterfaceBox test = (JClassInterfaceBox)four;
//            JClassInterfaceBox test2 = (JClassInterfaceBox)four_2;
//            assertEquals(test.getPackage(), test.getPackage(), test2.getPackage());
//            if(test.getPackage().equals(test2.getPackage())){
//                System.out.println(test.getPackage()+ "=" + test2.getPackage());
//            }
//           
//        }
//
//
//
//        
//        System.out.println("location Y");
//                
//      
//      
//        //test2 x location
//        Node five = threadExample.getBoxes().get(3 );
//        Node five_2 = data.getBoxes().get(3 );
//        if(five instanceof JClassClassBox){
//            JClassClassBox test = (JClassClassBox)five;
//            JClassClassBox test2 = (JClassClassBox)five_2;
//            double y1 = test.getY();
//            double y3 = test2.getY();
//            assertEquals(y1, y1, y3);
//            if(y1 == y3){
//                System.out.println(y1+ "=" + y3);
//            }
//           
//        }
//        System.out.println("Y LOCATION");
//        Node six = threadExample.getBoxes().get(1);
//        Node six_2 = data.getBoxes().get(1);
//        if(six instanceof JClassClassBox){
//            JClassClassBox test = (JClassClassBox)six;
//            JClassClassBox test2 = (JClassClassBox)six_2;
//            assertEquals(test.getY(), test.getY(), test2.getY());
//            if(test.getX() == test2.getX()){
//                System.out.println(test.getX()+ "=" + test2.getX());
//            }
//           
//        }
//        else{
//            
//            JClassInterfaceBox test = (JClassInterfaceBox)six;
//            JClassInterfaceBox test2 = (JClassInterfaceBox)six_2;
//            assertEquals(test.getX(), test.getX(), test2.getX());
//            if(test.getX() == test2.getX()){
//                System.out.println(test.getX()+ "=" + test2.getX());
//            }
//           
//        }
//      
//        System.out.println("Test attribute and method and types");
//                
//        //test2 arguemnt / parent
//        Node seven = threadExample.getBoxes().get(1);
//        Node seven_2 = data.getBoxes().get(1);
//        if(seven instanceof JClassClassBox){
//            JClassClassBox test = (JClassClassBox)seven;
//            JClassClassBox test2 = (JClassClassBox)seven_2;
//            ObservableList<ClassMethods> ats = test.getClassMethods();
//            ObservableList<ClassMethods> ats2 = test2.getClassMethods();
//            ArrayList<String> arg1 = ats.get(ats.size() - 1).getArguments();
//            ArrayList<String> arg2  = ats2.get(ats.size() - 1).getArguments();
//            String argg1 = arg1.get(0);
//            String argg2 = arg1.get(0);
//
//            assertEquals(argg1, argg1, argg2);
//            if(argg1.equals(argg2)){
//                System.out.println(argg1+ "=" + argg2);
//            }
//           
//        }
//        else{
//            
//            JClassInterfaceBox test = (JClassInterfaceBox)seven;
//            JClassInterfaceBox test2 = (JClassInterfaceBox)seven_2;
//            ObservableList<InterfaceMethods> ats = test.getInterfaceMethods();
//            ObservableList<InterfaceMethods> ats2 = test2.getInterfaceMethods();
//    //        String arg1 = ats.get(0).getArg();
//     //       String arg2 = ats2.get(0).getArg();            
//            
//          //  assertEquals(arg1, arg1, arg2);
//           // if(arg1.equals(arg2)){
//           //     System.out.println(arg1+ "=" + arg2);
//          //  }
//           
//        }
//        Node eight = threadExample.getBoxes().get(3);
//        Node eight_2 = data.getBoxes().get(3);
//        if(eight instanceof JClassClassBox){
//            JClassClassBox test = (JClassClassBox)eight;
//            JClassClassBox test2 = (JClassClassBox)eight_2;
//            String yup = test.getBoxType();
//            String yup2 = test2.getBoxType();
//            
//            
//            assertEquals(yup, yup, yup2);
//            if(yup.equals(yup2)){
//                System.out.println(yup+ "=" + yup2);
//            }
//           
//        }
//        else{
//            
//            JClassInterfaceBox test = (JClassInterfaceBox)eight;
//            JClassInterfaceBox test2 = (JClassInterfaceBox)eight_2;
//            String yup = test.getBoxType();
//            String yup2 = test2.getBoxType();
//            
//            
//            assertEquals(yup, yup, yup2);
//            if(yup.equals(yup2)){
//                System.out.println(yup+ "=" + yup2);
//            }
//           
//        }
//        System.out.println("Test 2 args");
//        //test 2 attributes
//        
//        Node nine = threadExample.getBoxes().get(2);
//        Node nine_2 = data.getBoxes().get(2);
//        if(nine instanceof JClassClassBox){
//            JClassClassBox test = (JClassClassBox)nine;
//            JClassClassBox test2 = (JClassClassBox)nine_2;
//            ObservableList<ClassAttributes> ats = test.getClassAttributes();
//            ObservableList<ClassAttributes> ats2 = test2.getClassAttributes();
//            String yes10 = ats.get(0).getAttributeName();
//            String yes11 = ats2.get(0).getAttributeName();
//            
//            assertEquals(yes10, yes10, yes11);
//            if(yes10.equals(yes11)){
//                System.out.println(yes10+ "=" + yes11);
//            }
//           
//        }
//        else{
//            
//            JClassInterfaceBox test = (JClassInterfaceBox)nine;
//            JClassInterfaceBox test2 = (JClassInterfaceBox)nine_2;
//            ObservableList<InterfaceAttributes> ats = test.getInterfaceAttributes();
//            ObservableList<InterfaceAttributes> ats2 = test2.getInterfaceAttributes();
//            String yes10 = ats.get(0).getAttributeName();
//            String yes11 = ats2.get(0).getAttributeName();
//            
//            assertEquals(yes10, yes10, yes11);
//            if(yes10.equals(yes11)){
//                System.out.println(yes10+ "=" + yes11);
//            }
//           
//        }
//                
//        Node ten = threadExample.getBoxes().get(1);
//        Node ten_2 = data.getBoxes().get(1);
//        if(ten instanceof JClassClassBox){
//            JClassClassBox test = (JClassClassBox)ten;
//            JClassClassBox test2 = (JClassClassBox)ten_2;
//            ObservableList<ClassAttributes> ats = test.getClassAttributes();
//            ObservableList<ClassAttributes> ats2 = test2.getClassAttributes();
//            String yes10 = ats.get(0).getAttributeType();
//            String yes11 = ats2.get(0).getAttributeType();
//            
//            assertEquals(yes10, yes10, yes11);
//            if(yes10.equals(yes11)){
//                System.out.println(yes10+ "=" + yes11);
//            }
//           
//        }
//        else{
//            
//            JClassInterfaceBox test = (JClassInterfaceBox)ten;
//            JClassInterfaceBox test2 = (JClassInterfaceBox)ten_2;
//            ObservableList<InterfaceAttributes> ats = test.getInterfaceAttributes();
//            ObservableList<InterfaceAttributes> ats2 = test2.getInterfaceAttributes();
//            String yes10 = ats.get(0).getAttributeType();
//            String yes11 = ats2.get(0).getAttributeType();
//            
//            assertEquals(yes10, yes10, yes11);
//            if(yes10.equals(yes11)){
//                System.out.println(yes10+ "=" + yes11);
//            }
//           
//        }
//        }
//
//
//
//}
